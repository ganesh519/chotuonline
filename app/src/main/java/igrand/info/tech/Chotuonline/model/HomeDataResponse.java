package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class HomeDataResponse {


    @SerializedName("up_slider")
    @Expose
    public List<UpSliderResponse> upSlider = null;
    @SerializedName("today_deals")
    @Expose
    public List<TodayDealResponse> todayDeals = null;
    @SerializedName("all_categories")
    @Expose
    public List<AllCategoryResponse> allCategories = null;
    @SerializedName("down_slider")
    @Expose
    public List<DownSliderResponse> downSlider = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("upSlider", upSlider).append("todayDeals", todayDeals).append("allCategories", allCategories).append("downSlider", downSlider).toString();
    }
}
