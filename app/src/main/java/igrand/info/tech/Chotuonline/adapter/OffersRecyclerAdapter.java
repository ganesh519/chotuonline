package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.OffersDataResponse;

public class OffersRecyclerAdapter extends RecyclerView.Adapter<OffersRecyclerAdapter.Holder> {
    List<OffersDataResponse> offersDataResponse;
    Context context;
    private int lastPosition = -1;

    public OffersRecyclerAdapter(List<OffersDataResponse> offersDataResponse, Context context) {
        this.offersDataResponse =offersDataResponse;
        this.context=context;
    }

    @NonNull
    @Override
    public OffersRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_offers,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OffersRecyclerAdapter.Holder holder, int position) {
        setAnimation(holder.itemView,position);
        holder.off_name.setText(offersDataResponse.get(position).name);
        holder.off_dec.setText(offersDataResponse.get(position).description);

    }
    @Override
    public int getItemCount() {
        return offersDataResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView off_name,off_dec;

        public Holder(View itemView) {
            super(itemView);
            off_dec=itemView.findViewById(R.id.off_dec);
            off_name=itemView.findViewById(R.id.off_name);
        }
    }

    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
