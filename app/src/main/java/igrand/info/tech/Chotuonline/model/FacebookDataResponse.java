package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class FacebookDataResponse {

    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("user_email")
    @Expose
    public String userEmail;
    @SerializedName("user_first_name")
    @Expose
    public String userFirstName;
    @SerializedName("user_last_name")
    @Expose
    public String userLastName;
    @SerializedName("user_phone_number")
    @Expose
    public String userPhoneNumber;
    @SerializedName("user_type")
    @Expose
    public String userType;
    @SerializedName("user_photo")
    @Expose
    public String userPhoto;
    @SerializedName("user_alt_number")
    @Expose
    public String userAltNumber;
    @SerializedName("user_date_of_birth")
    @Expose
    public String userDateOfBirth;
    @SerializedName("user_facebook_type")
    @Expose
    public Integer userFacebookType;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("userEmail", userEmail).append("userFirstName", userFirstName).append("userLastName", userLastName).append("userPhoneNumber", userPhoneNumber).append("userType", userType).append("userPhoto", userPhoto).append("userAltNumber", userAltNumber).append("userDateOfBirth", userDateOfBirth).append("userFacebookType", userFacebookType).toString();
    }
}
