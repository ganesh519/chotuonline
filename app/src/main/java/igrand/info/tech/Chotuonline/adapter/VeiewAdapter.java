package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.Zoom_Image;
import igrand.info.tech.Chotuonline.model.HomeProductimageResponse;


public class VeiewAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<HomeProductimageResponse> images;
    private String name;

    public VeiewAdapter(List<HomeProductimageResponse> images,String name,Context context) {
        this.context = context;
        this.images = images;
        this.name=name;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.custom_layout, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

        String Image=images.get(position).image;

        if (Image.equals("")){
            Picasso.with(context).load(R.drawable.no_image).into(imageView);
        }
        else {
            Picasso.with(context).load(images.get(position).image).into(imageView);
        }


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int posit=position;
                Intent intent=new Intent(context, Zoom_Image.class);
                intent.putExtra("Sliderimage",(Serializable) images);
                Bundle bundle=new Bundle();
                bundle.putString("pname",name);
                bundle.putString("pos", String.valueOf(posit));
                intent.putExtras(bundle);
                context.startActivity(intent);

            }
        });

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }

}
