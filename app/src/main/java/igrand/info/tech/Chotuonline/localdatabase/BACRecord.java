package igrand.info.tech.Chotuonline.localdatabase;

import java.io.Serializable;

/**
 * Created by deesunda on 10/4/2014.
 */

public class BACRecord implements Serializable {
    String indexID;
    String variationId;
    String quantity;

    public BACRecord(String variationId, String quantity) {
        this.variationId = variationId;
        this.quantity = quantity;

    }

    public BACRecord(String indexID, String variationId,String quantity){
        this.indexID = indexID;
        this.variationId=variationId;
        this.quantity=quantity;

    }

/*
    public BACRecord(String date, String time, double bac, String status){
        this.date=date;
        this.time=time;
        this.bac = bac;
        this.status = status;
    }

    public BACRecord(String date, String time, double bac, double latitudeValue, double longitudeValue,
                     String videoFile, String status, boolean faceMatchFailBAC){
        this.date=date;
        this.time=time;
        this.bac = bac;
        this.latitudeValue = latitudeValue;
        this.longitudeValue = longitudeValue;
        this.videoFile = videoFile;
        this.status = status;
        this.faceMatchFailBAC = faceMatchFailBAC;
    }

    public BACRecord(String indexID, String date, String time, double bac, double latitudeValue, double longitudeValue,
                     String videoFile, String status, boolean faceMatchFailBAC){
        this.indexID = indexID;
        this.date=date;
        this.time=time;
        this.bac = bac;
        this.latitudeValue = latitudeValue;
        this.longitudeValue = longitudeValue;
        this.videoFile = videoFile;
        this.status = status;
        this.faceMatchFailBAC = faceMatchFailBAC;
    }
*/


    public String getIndexID() {
        return indexID;
    }

    public void setIndexID(String indexID) { this.indexID = indexID;
    }


    public String getVariationId() {
        return variationId;
    }

    public void setVariationId(String variationId) {
        this.variationId = variationId;
    }


    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    public BACRecord() {

    }


}
