package igrand.info.tech.Chotuonline.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.NotificationRecyclerAdapter;
import igrand.info.tech.Chotuonline.model.NotificationDataResponse;
import igrand.info.tech.Chotuonline.model.NotificationResponse;
import retrofit2.Call;
import retrofit2.Callback;


public class FragmentAlerts extends Fragment {

    RecyclerView recycler_notification;
    RecyclerView.Adapter adapter;
    ApiInterface apiInterface;
    Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_alerts, container, false);
        recycler_notification=v.findViewById(R.id.recycler_notification);
        final ProgressDialog progressDialog1 = new ProgressDialog(getActivity());
        progressDialog1.setMessage("wait...");
        progressDialog1.show();
        apiInterface=ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationResponse> call=apiInterface.Notifications();
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, retrofit2.Response<NotificationResponse> response) {
                if (response.isSuccessful()) ;
                NotificationResponse notificationResponse = response.body();
                if (notificationResponse.status.equals("1")) {
                    progressDialog1.dismiss();
                    List<NotificationDataResponse> notificationDataResponse = notificationResponse.data;
                    for (int i = 0; i < notificationDataResponse.size(); i++) {
                        recycler_notification.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        recycler_notification.setAdapter(new NotificationRecyclerAdapter(notificationDataResponse, context));
                        recycler_notification.setHasFixedSize(true);
                        recycler_notification.setNestedScrollingEnabled(false);
                    }

                }
                else if (notificationResponse.status.equals("0")) {
                    progressDialog1.dismiss();
                    Toast.makeText(context, notificationResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                progressDialog1.dismiss();
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        return v;
    }

}
