package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class OrderItemsDataResponse {


    @SerializedName("no_of_items")
    @Expose
    public Integer noOfItems;
    @SerializedName("grand_total")
    @Expose
    public String grandTotal;
    @SerializedName("items")
    @Expose
    public List<OrderItemsReonse> items = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("noOfItems", noOfItems).append("grandTotal", grandTotal).append("items", items).toString();
    }
}
