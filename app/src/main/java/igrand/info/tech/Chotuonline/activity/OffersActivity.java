package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.OffersRecyclerAdapter;
import igrand.info.tech.Chotuonline.model.OffersDataResponse;
import igrand.info.tech.Chotuonline.model.OffersResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersActivity extends AppCompatActivity {
    RecyclerView recycler_offers;
    ApiInterface apiInterface;
    Context context;
    ImageView back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offers);
        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recycler_offers=findViewById(R.id.recycler_offers);
        final ProgressDialog progressDialog1 = new ProgressDialog(OffersActivity.this);
        progressDialog1.setMessage("wait...");
        progressDialog1.show();
        apiInterface=ApiClient.getClient().create(ApiInterface.class);
        Call<OffersResponse> call=apiInterface.Offers();
        call.enqueue(new Callback<OffersResponse>() {
            @Override public void onResponse(Call<OffersResponse> call, Response<OffersResponse> response) {
                if (response.isSuccessful());
                OffersResponse offersResponse=response.body();
                if (offersResponse.status.equals("1")){
                    progressDialog1.dismiss();
                    List<OffersDataResponse> offersDataResponse = offersResponse.data;
                    for (int i=0;i<offersDataResponse.size();i++){
                        recycler_offers.setLayoutManager(new LinearLayoutManager(OffersActivity.this, LinearLayoutManager.VERTICAL, false));
                        recycler_offers.setAdapter(new OffersRecyclerAdapter(offersDataResponse, OffersActivity.this));
                        recycler_offers.setHasFixedSize(true);
                        recycler_offers.setNestedScrollingEnabled(false);
                    }

                }
                else if (offersResponse.status.equals("0")){
                    progressDialog1.dismiss();
                    Toast.makeText(OffersActivity.this, offersResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OffersResponse> call, Throwable t) {
                progressDialog1.dismiss();
                Toast.makeText(OffersActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }
}
