package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.OrderDetailsActivity;
import igrand.info.tech.Chotuonline.model.MyOrdersDataResponse;

public class MyOrderRecyclerAdapter extends RecyclerView.Adapter<MyOrderRecyclerAdapter.Holder> {
    List<MyOrdersDataResponse> myOrdersDataResponse;
    Context context;
    private int lastPosition = -1;

    public MyOrderRecyclerAdapter(List<MyOrdersDataResponse> myOrdersDataResponse, Context context) {
        this.myOrdersDataResponse = myOrdersDataResponse;
        this.context = context;
    }

    @NonNull
    @Override
    public MyOrderRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_order, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyOrderRecyclerAdapter.Holder holder, final int position) {

        setAnimation(holder.itemView,position);
        holder.date.setText(myOrdersDataResponse.get(position).orderDatetime);
        holder.id.setText(myOrdersDataResponse.get(position).orderNumber);
        holder.p_price.setText("Rs " + myOrdersDataResponse.get(position).totalAmount);
        holder.status.setText(myOrdersDataResponse.get(position).orderStatus);
        holder.items.setText(String.valueOf(myOrdersDataResponse.get(position).noOfItems + "items"));
        if (myOrdersDataResponse.get(position).orderStatus.equals("Pending")) {
            holder.status.setTextColor(Color.parseColor("#FFDC2424"));
        }
        if (myOrdersDataResponse.get(position).orderStatus.equals("Cancelled")) {
            holder.status.setTextColor(Color.parseColor("#f56b1111"));
        }
        if (myOrdersDataResponse.get(position).orderStatus.equals("Delivered")) {
            holder.status.setTextColor(Color.parseColor("#f53a8313"));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String orderid = myOrdersDataResponse.get(position).orderId;
                SharedPreferences sharedPreferences = context.getSharedPreferences("OrderId", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("order_id", orderid);
                editor.commit();
                Intent intent = new Intent(context, OrderDetailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return myOrdersDataResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView date, id, p_price, items, status;

        public Holder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            id = itemView.findViewById(R.id.id);
            p_price = itemView.findViewById(R.id.p_price);
            status = itemView.findViewById(R.id.status);
            items = itemView.findViewById(R.id.items);
        }
    }
    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
