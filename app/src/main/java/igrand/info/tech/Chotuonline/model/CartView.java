package igrand.info.tech.Chotuonline.model;

public class CartView {

    String variation;
    String quantity;

    public CartView(String variation, String quantity) {
        this.variation = variation;
        this.quantity = quantity;
    }

    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
