package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.Diaper;

public class CategoryDaiperAdapter extends RecyclerView.Adapter<CategoryDaiperAdapter.Holder> {

    ArrayList<Diaper> arrayList;
    Context context;
    int counter=0;
    String[] catagory={"46 piece - ₹ 649","50 piece - ₹ 800","55 piece - ₹ 1000"};


    public CategoryDaiperAdapter(ArrayList<Diaper> arrayList,Context context) {
        this.arrayList = arrayList;
        this.context=context;
    }

    @NonNull
    @Override
    public CategoryDaiperAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_diaper,parent,false);

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryDaiperAdapter.Holder holder, int position) {


        holder.image.setImageResource(arrayList.get(position).getProduct_image());
        holder.title.setText(arrayList.get(position).getProduct_title());
        holder.name.setText(arrayList.get(position).getProduct_name());
        holder.price.setText("₹ "+arrayList.get(position).getProduct_price());
        holder.off.setText(arrayList.get(position).getOff());

        holder.linear_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.linear_add.setVisibility(View.GONE);
                holder.linear_cart.setVisibility(View.VISIBLE);
            }
        });

        holder.decreas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter--;
                if (counter < 1){
                    counter = 1;
                    holder.linear_add.setVisibility(View.VISIBLE);
                    holder.linear_cart.setVisibility(View.GONE);
                }
                holder.show_value.setText(Integer.toString(counter));

            }
        });

        holder.increas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter++;
                holder.show_value.setText(Integer.toString(counter));
            }
        });

        holder.dprice.setText("₹ "+arrayList.get(position).getProduct_dprice());
        holder.dprice.setPaintFlags(holder.price.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);

        holder.s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> aa = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, catagory);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.s.setAdapter(aa);


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title,name,price,dprice,off;
        Spinner s;
        LinearLayout linear_add,linear_cart;
        TextView show_value;
        ImageView increas,decreas;
        public Holder(View itemView) {
            super(itemView);

            image=itemView.findViewById(R.id.image);
            title=itemView.findViewById(R.id.title);
            name=itemView.findViewById(R.id.name);
            price=itemView.findViewById(R.id.price);
            dprice=itemView.findViewById(R.id.dprice);
            off=itemView.findViewById(R.id.off);
            s=itemView.findViewById(R.id.spinner);

            show_value=itemView.findViewById(R.id.show_value);
            decreas=itemView.findViewById(R.id.decreas);
            increas=itemView.findViewById(R.id.increas);
            linear_add=itemView.findViewById(R.id.linear_add);
            linear_cart=itemView.findViewById(R.id.linear_cart);


        }
    }
}
