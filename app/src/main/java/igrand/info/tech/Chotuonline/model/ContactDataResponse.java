package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ContactDataResponse {


    @SerializedName("company_name")
    @Expose
    public String companyName;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("phone_number")
    @Expose
    public String phoneNumber;
    @SerializedName("email_id")
    @Expose
    public String emailId;
    @SerializedName("working_hours")
    @Expose
    public String workingHours;
    @SerializedName("latitude")
    @Expose
    public String latitude;
    @SerializedName("longitude")
    @Expose
    public String longitude;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("companyName", companyName).append("address", address).append("phoneNumber", phoneNumber).append("emailId", emailId).append("workingHours", workingHours).append("latitude", latitude).append("longitude", longitude).toString();
    }
}
