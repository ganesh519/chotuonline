package igrand.info.tech.Chotuonline.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.ItemRecyclerAdapter;
import igrand.info.tech.Chotuonline.model.Item;
import igrand.info.tech.Chotuonline.model.OrderItemsDataResponse;
import igrand.info.tech.Chotuonline.model.OrderItemsReonse;
import igrand.info.tech.Chotuonline.model.OrderItemsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentItems extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    ArrayList<Item> arrayList = new ArrayList<>();
    ApiInterface apiInterface;
    TextView Total, items;
    String total_amount;
    String Items;
    LinearLayout linear_item;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragments_items, container, false);

        items = v.findViewById(R.id.items);
        Total = v.findViewById(R.id.Total);
        linear_item = v.findViewById(R.id.linear_item);

        recyclerView = (RecyclerView) v.findViewById(R.id.orderitem_recycler);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences.getString("user_id", "");

        SharedPreferences sharedPreferences1 = getActivity().getSharedPreferences("OrderId", Context.MODE_PRIVATE);
        String Order_id = sharedPreferences1.getString("order_id", "");

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("wait...");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderItemsResponse> call = apiInterface.OrderItems(user_id, Order_id);
        call.enqueue(new Callback<OrderItemsResponse>() {
            @Override
            public void onResponse(Call<OrderItemsResponse> call, Response<OrderItemsResponse> response) {
                if (response.isSuccessful()) ;
                OrderItemsResponse orderItemsResponse = response.body();
                if (orderItemsResponse.status.equals("1")) {
                    linear_item.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    OrderItemsDataResponse orderItemsDataResponse = orderItemsResponse.data;
                    total_amount = "₹ " + orderItemsDataResponse.grandTotal;
                    Items = orderItemsDataResponse.noOfItems + " items";
                    items.setText(Items);
                    Total.setText(total_amount);

                    List<OrderItemsReonse> orderItemsReonse = orderItemsDataResponse.items;
                    recyclerView.setAdapter(new ItemRecyclerAdapter(orderItemsReonse,getActivity()));
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setNestedScrollingEnabled(false);

                }
                else if (orderItemsResponse.status.equals("0")) {
                    linear_item.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), orderItemsResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OrderItemsResponse> call, Throwable t) {
                linear_item.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }
}
