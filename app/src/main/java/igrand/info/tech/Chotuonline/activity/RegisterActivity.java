package igrand.info.tech.Chotuonline.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.ViewPagerAdapter1;
import igrand.info.tech.Chotuonline.fragments.FragmentLogin;
import igrand.info.tech.Chotuonline.fragments.FragmentSignup;

public class RegisterActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter1 viewPagerAdapter;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        viewPager = (ViewPager) findViewById(R.id.viewPager_signup);
        viewPagerAdapter = new ViewPagerAdapter1(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new FragmentLogin(), "LOGIN");
        viewPagerAdapter.addFragment(new FragmentSignup(), "SIGNUP");
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(1);
        tabLayout = (TabLayout) findViewById(R.id.tabs_signup);
        tabLayout.setupWithViewPager(viewPager);
        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

}
