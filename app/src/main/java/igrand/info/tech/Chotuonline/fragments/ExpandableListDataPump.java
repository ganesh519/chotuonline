package igrand.info.tech.Chotuonline.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by admin1 on 1/8/2018.
 */
public class ExpandableListDataPump {
    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> Fruits = new ArrayList<String>();
        Fruits.add("Fresh Vegetables");
        Fruits.add("Fresh Herbs & Seasonings");
        Fruits.add("Fresh Fruits");
        Fruits.add("Cuts & Sprouts");

        List<String> Foodgrains = new ArrayList<String>();
        Foodgrains.add("Atta,Flours & Sooji");
        Foodgrains.add("Dals & pulses");
        Foodgrains.add("Salt,Sugar & Jaggery");

        List<String> Bakery = new ArrayList<String>();
        Bakery.add("Dairy");
        Bakery.add("Breads & Buns");
        Bakery.add("Cakes & Pastries");
        Bakery.add("Cookies,Rusk & Khari");

        List<String> BrandedFood = new ArrayList<String>();
        BrandedFood.add("Breakfast Cereals");
        BrandedFood.add("Noodles");
        BrandedFood.add("Jams & Spreads");
        BrandedFood.add("pasta &Vermicelli ");
        BrandedFood.add("Veg Snacks");
        BrandedFood.add("Sauces & Ketchup");
        BrandedFood.add("Chocolates & Sweets");
        BrandedFood.add("Baking & Dessert Items");
        BrandedFood.add("Indian Sweets");
        BrandedFood.add("Pickels");

        List<String> Beauty = new ArrayList<String>();
        Beauty.add("Feminine Hygiene");
        Beauty.add("Oral Care");
        Beauty.add("Bath,Face & Hand Wash");
        Beauty.add("Hair Care");
        Beauty.add("Health & Medicine");
        Beauty.add("Skin Care");
        Beauty.add("Mens Grooming");
        Beauty.add("Makeup");
        Beauty.add("Deodorant & Fragrance");

        List<String> Household = new ArrayList<String>();
        Household.add("Kitchen & Dining");
        Household.add("Detergents");
        Household.add("Utensil");
        Household.add("Toilet,Floor & Other Cleaners");
        Household.add("Kitchenware");
        Household.add("paper & Disposable");
        Household.add("Serveware");
        Household.add("Crackers");
        Household.add("Pet Care");
        Household.add("Pooja Needs");
        Household.add("Electricals");
        Household.add("Car & Shoe Care");
        Household.add("Stationery");
        Household.add("Gardening Needs");
        Household.add("Utilities");
        Household.add("Party & Festive Needs");

        List<String> pooja = new ArrayList<String>();
        pooja.add("Tumeric (Pasupu)");
        pooja.add("KumKum");
        pooja.add("Incense Sticks (Agarbattis)");
        pooja.add("Camphor (Karpuram)");
        pooja.add("Special pooja Oil");
        pooja.add("Chandan/Pooja powder");
        pooja.add("Gingely Oil");
        pooja.add("Coconut");
        pooja.add("Candles");
        pooja.add("Cotton Vastra (Vastramu)");
        pooja.add("Navadhanyallu");

        List<String> Eggs = new ArrayList<String>();
        Eggs.add("Eggs");
        Eggs.add("Marinates");
        Eggs.add("Poultry");
        Eggs.add("Fish & Seafood");
        Eggs.add("Mutton & Lamb");
        Eggs.add("Sausages,Bacon & Salami");
        Eggs.add("Pork & other Meats");

        List<String> Baby = new ArrayList<String>();
        Baby.add("Baby Food & Formula");
        Baby.add("Baby Diapers & Wipes");
        Baby.add("Baby Toiletries");
        Baby.add("Baby Accessories");
        Baby.add("Mothers & Maternity");

        List<String> Beverages = new ArrayList<String>();
        Beverages.add("Mineral Water");
        Beverages.add("Energy & Health drinks");
        Beverages.add("Soft Drinks");
        Beverages.add("Tea & Coffee");
        Beverages.add("Ayurvedic");
        Beverages.add("Organic Beverages");

        List<String> Bath = new ArrayList<String>();
        Bath.add("Liquid Soaps & Bars");
        Bath.add("Shampoo & Conditioners");
        Bath.add("Hand Wash");
        Bath.add("Deos & Perfumes");
        Bath.add("Tooth Powder");
        Bath.add("Tooth Paste & Mouthwash");
        Bath.add("Body Lotions");

        List<String> Rice = new ArrayList<String>();
        Rice.add("Basmathi Rice");
        Rice.add("Sonamasoori Rice");

        List<String> Dals = new ArrayList<String>();
        Dals.add("Dals");
        Dals.add("Pulses");
        Dals.add("Ravva");
        Dals.add("Flour");

        List<String> Masala = new ArrayList<String>();
        Masala.add("Masalas");
        Masala.add("Spices");
        Masala.add("Dry Fruits");

        List<String> plastic = new ArrayList<String>();
        plastic.add("Tissue Paper & Napkins");
        plastic.add("Disposable Plates & Utensils");
        plastic.add("Wet Wipers");

        List<String> book = new ArrayList<String>();
        book.add("A4 Paper");
        book.add("Glue Sticks & Adhesive");
        book.add("Staplers");
        book.add("Markers");
        book.add("Stamp Pad & Ink");
        book.add("Ruberbands & Tapes");
        book.add("Exam Pad");
        book.add("Carbon Paper");
        book.add("ID card Holders & Pulley");

        List<String> animal = new ArrayList<String>();
        animal.add("Dog Food");

        expandableListDetail.put("Fruits & Vegetables", Fruits);
        expandableListDetail.put("Foodgrains & Oil", Foodgrains);
        expandableListDetail.put("Bakery,Cakes & Dairy", Bakery);
        expandableListDetail.put("Beverages",Beverages);
        expandableListDetail.put("Branded Foods",BrandedFood);
        expandableListDetail.put("Beauty & Hygiene",Beauty);
        expandableListDetail.put("Household",Household);
        expandableListDetail.put("pooja Needs",pooja);
        expandableListDetail.put("Eggs,Meat & Fish",Eggs);
        expandableListDetail.put("Baby & Care",Baby);
        expandableListDetail.put("Bath & Body",Bath);
        expandableListDetail.put("Rice",Rice);
        expandableListDetail.put("Dals,Pulses,Flour & Ravva",Dals);
        expandableListDetail.put("Masalas,Spices & Dry Fruits",Masala);
        expandableListDetail.put("Plastic & Disposables",plastic);
        expandableListDetail.put("Books & stationery",book);
        expandableListDetail.put("Animal Food",animal);

        return expandableListDetail;
    }
}