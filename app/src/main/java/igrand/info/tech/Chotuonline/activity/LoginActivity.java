package igrand.info.tech.Chotuonline.activity;

import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.ViewPagerAdapter1;
import igrand.info.tech.Chotuonline.fragments.FragmentLogin;
import igrand.info.tech.Chotuonline.fragments.FragmentSignup;

public class LoginActivity extends AppCompatActivity {

    TabLayout tabLayout;
    private static ViewPager viewPager;
    ViewPagerAdapter1 viewPagerAdapter;
    ImageView back, image;
    int currentpage;
    TextView login_txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login_txt = findViewById(R.id.login_txt);
        image = findViewById(R.id.image);


        viewPager = (ViewPager) findViewById(R.id.viewPager_login);
        viewPagerAdapter = new ViewPagerAdapter1(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new FragmentLogin(), "LOGIN");
        viewPagerAdapter.addFragment(new FragmentSignup(), "SIGNUP");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs_login);
        tabLayout.setupWithViewPager(viewPager);

        String Signup=getIntent().getStringExtra("signup");
        if (!Signup.equals("")){
            if (Signup.equals("Sign")){
                login_txt.setText("SIGNUP");
                viewPager.setCurrentItem(1);
            }
            else {
                login_txt.setText("LOGIN");
                viewPager.setCurrentItem(0);
            }
        }
        else {
            login_txt.setText("LOGIN");
            viewPager.setCurrentItem(0);
        }

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
                alertDialog.setTitle("Exit");
                alertDialog.setMessage("Are you sure you want close this application?");


                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
//                startActivity(new Intent(ctx, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                sessionManager.logoutUser();
                        finishAffinity();
                    }
                });
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                alertDialog.show();
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {

                currentpage = position;

                if (position == 1) {
                    login_txt.setText("SIGNUP");
                    image.setImageResource(R.drawable.sign);
                }
                else {
                    login_txt.setText("LOGIN");
                    image.setImageResource(R.drawable.vegetablejuice);

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public static void setPage(int signup) {

        viewPager.setCurrentItem(signup);

    }
    @Override
    public void onBackPressed()
    {
        showAlert();
//        super.onBackPressed();
    }
    private void showAlert() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
        alertDialog.setTitle("Exit");
        alertDialog.setMessage("Are you sure you want close this application?");


        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

}
