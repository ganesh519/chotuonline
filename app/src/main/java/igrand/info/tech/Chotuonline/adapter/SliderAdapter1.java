package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.fragments.CategoryMainListFragment;
import igrand.info.tech.Chotuonline.model.DownSliderResponse;



public class SliderAdapter1 extends PagerAdapter {
    Context context;
    List<DownSliderResponse> upSliderResponse;


    public SliderAdapter1(List<DownSliderResponse> upSliderResponse, Context context) {
        this.context = context;
        this.upSliderResponse = upSliderResponse;

    }

    @Override
    public int getCount() {
        return upSliderResponse.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slider, container, false);
        ImageView myImage = view.findViewById(R.id.image1);
        Picasso.with(context).load(upSliderResponse.get(position).image).into(myImage);
        myImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String category_id=upSliderResponse.get(position).catId;
                String type=upSliderResponse.get(position).linkType;
                String type_id=upSliderResponse.get(position).linkId;
                String category_name=upSliderResponse.get(position).linkName;

                if (type.equals("Product")){

                    if (type_id.equals("0")){
                        Toast.makeText(context, "Product", Toast.LENGTH_SHORT).show();
                    }
                    else {


                    }

                }else if (type.equals("Sub Category")){
                    if (type_id.equals("0")){
                        Toast.makeText(context, "Sub Category", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Bundle bundle=new Bundle();
                        bundle.putString("category_id", category_id);
                        bundle.putString("subcategory_id", type_id);
                        bundle.putString("category_name", category_name);
                        bundle.putString("menuList","data");
                        bundle.putString("all_category","Allcategory");

                        AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
                        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
                        appCompatActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fl_main_framelayout,categoryMainListFragment,"").addToBackStack("").commit();
                        categoryMainListFragment.setArguments(bundle);

                    }
                }
                else if (type.equals("Sub Sub Category")){
                    if (type_id.equals("0")){
                        Toast.makeText(context, "Sub Sub Category", Toast.LENGTH_SHORT).show();
                    }
                    else {
//                        Bundle bundle = new Bundle();
//                        bundle.putString("category_id", category_id);
//                        bundle.putString("subcategory_id", type_id);
//                        bundle.putString("sub_sub_id", sub_sub_id);
//                        bundle.putString("category_name", category_name);
//                        bundle.putString("all_category", "");
//                        bundle.putString("menuList", "data");
//                        bundle.putString("SubSubCategory", "subsubcategory");
//
//                        AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
//                        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
//                        appCompatActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fl_main_framelayout, categoryMainListFragment, "").addToBackStack("").commit();
//                        categoryMainListFragment.setArguments(bundle);
                    }
                }
                else if (type.equals("Category")){
                    if (type_id.equals("0")){
                        Toast.makeText(context, "Category", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Bundle bundle=new Bundle();
                        bundle.putString("category_id", type_id);
                        bundle.putString("subcategory_id", "");
                        bundle.putString("category_name", category_name);
                        bundle.putString("all_category","gfgfg");
                        bundle.putString("SubSubCategory","");

                        AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
                        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransaction = appCompatActivity.getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fl_main_framelayout, categoryMainListFragment,"");
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        categoryMainListFragment.setArguments(bundle);
                    }
                }

            }
        });
        container.addView(view);
        return view;

    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
