package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class All_CategoryDataResponse {


    @SerializedName("category_id")
    @Expose
    public String categoryId;
    @SerializedName("category_name")
    @Expose
    public String categoryName;
    @SerializedName("category_icon")
    @Expose
    public String categoryIcon;
    @SerializedName("subcategoryinfo")
    @Expose
    public List<AllSubCategoryResponse> subcategoryinfo = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("categoryId", categoryId).append("categoryName", categoryName).append("categoryIcon", categoryIcon).append("subcategoryinfo", subcategoryinfo).toString();
    }
}
