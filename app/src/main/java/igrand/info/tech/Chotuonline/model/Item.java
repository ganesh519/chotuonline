package igrand.info.tech.Chotuonline.model;

public class Item {
    private int product_image;
    private String product_title;
    private String product_name;
    private String pack;
    private String price;
    private String quantity;

    public Item(int product_image, String product_title, String product_name, String pack, String price, String quantity) {
        this.product_image = product_image;
        this.product_title = product_title;
        this.product_name = product_name;
        this.pack = pack;
        this.price = price;
        this.quantity = quantity;
    }

    public int getProduct_image() {
        return product_image;
    }

    public void setProduct_image(int product_image) {
        this.product_image = product_image;
    }

    public String getProduct_title() {
        return product_title;
    }

    public void setProduct_title(String product_title) {
        this.product_title = product_title;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPack() {
        return pack;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
