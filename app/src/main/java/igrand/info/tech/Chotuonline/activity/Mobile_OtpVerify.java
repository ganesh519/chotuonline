package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.MobileOtpVerifyDataResponse;
import igrand.info.tech.Chotuonline.model.MobileOtpVerifyResponse;
import igrand.info.tech.Chotuonline.model.OtpResendResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Mobile_OtpVerify extends AppCompatActivity{
    EditText signup_otp;
    String otp, mobile, code;
    ApiInterface apiInterface, apiInterface1;
    Button proceed;
    TextView resend;
    SmsVerifyCatcher smsVerifyCatcher;
    MobileOtpVerifyDataResponse mobileOtpVerifyDataResponse;
    String u_id,image,name,Phone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_mobile);
        signup_otp = findViewById(R.id.signup_otp);
        proceed = findViewById(R.id.proceed);
        resend = findViewById(R.id.resend);
        mobile = getIntent().getStringExtra("phone");

        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                code = parseCode(message);
                signup_otp.setText(code);
            }
        });



        final ProgressDialog progressDialog1 = new ProgressDialog(Mobile_OtpVerify.this);
        progressDialog1.setMessage("wait...");
        progressDialog1.show();

        apiInterface1 = ApiClient.getClient().create(ApiInterface.class);
        Call<OtpResendResponse> call1 = apiInterface1.ReSend(mobile);
        call1.enqueue(new Callback<OtpResendResponse>() {
            @Override
            public void onResponse(Call<OtpResendResponse> call, Response<OtpResendResponse> response) {
                if (response.isSuccessful()) ;
                OtpResendResponse otpResendResponse = response.body();
                if (otpResendResponse.status.equals("1")) {
                    progressDialog1.dismiss();
                    Toast.makeText(Mobile_OtpVerify.this, otpResendResponse.message, Toast.LENGTH_SHORT).show();
                } else if (otpResendResponse.status.equals("0")) {
                    progressDialog1.dismiss();
                    Toast.makeText(Mobile_OtpVerify.this, otpResendResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OtpResendResponse> call, Throwable t) {
                progressDialog1.dismiss();
                Toast.makeText(Mobile_OtpVerify.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });




        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp = signup_otp.getText().toString();
                final ProgressDialog progressDialog = new ProgressDialog(Mobile_OtpVerify.this);
                progressDialog.setMessage("wait...");
                progressDialog.show();
                SharedPreferences sharedPreference =getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
                String device = sharedPreference.getString("mobile_id","");

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<MobileOtpVerifyResponse> call=apiInterface.MobileOtpVerify(otp,device);
                call.enqueue(new Callback<MobileOtpVerifyResponse>() {
                    @Override
                    public void onResponse(Call<MobileOtpVerifyResponse> call, Response<MobileOtpVerifyResponse> response) {
                        if (response.isSuccessful());
                        MobileOtpVerifyResponse mobileOtpVerifyResponse=response.body();
                        if (mobileOtpVerifyResponse.status.equals("1")){

                            mobileOtpVerifyDataResponse=mobileOtpVerifyResponse.data;
                           u_id=mobileOtpVerifyDataResponse.userId;
                           image=mobileOtpVerifyDataResponse.userPhoto;
                           name=mobileOtpVerifyDataResponse.userFirstName;
                           Phone=mobileOtpVerifyDataResponse.userPhoneNumber;
                           Toast.makeText(Mobile_OtpVerify.this, mobileOtpVerifyResponse.message, Toast.LENGTH_SHORT).show();

                           SharedPreferences sharedPreferences = Mobile_OtpVerify.this.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("user_id", u_id);
                            editor.putString("profileimage", image);
                            editor.putString("Name", name);
                            editor.putString("Mobile", Phone);
                            editor.commit();
                            Intent intent = new Intent(Mobile_OtpVerify.this, MainActivity.class);
                            startActivity(intent);
                            progressDialog.dismiss();
                        }
                        else if (mobileOtpVerifyResponse.status.equals("0")){
                            progressDialog.dismiss();
                            Toast.makeText(Mobile_OtpVerify.this, mobileOtpVerifyResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<MobileOtpVerifyResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(Mobile_OtpVerify.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog progressDialog1 = new ProgressDialog(Mobile_OtpVerify.this);
                progressDialog1.setMessage("wait...");
                progressDialog1.show();

                apiInterface1 = ApiClient.getClient().create(ApiInterface.class);
                Call<OtpResendResponse> call1 = apiInterface1.ReSend(mobile);
                call1.enqueue(new Callback<OtpResendResponse>() {
                    @Override
                    public void onResponse(Call<OtpResendResponse> call, Response<OtpResendResponse> response) {
                        if (response.isSuccessful()) ;
                        OtpResendResponse otpResendResponse = response.body();
                        if (otpResendResponse.status.equals("1")) {
                            progressDialog1.dismiss();
                            Toast.makeText(Mobile_OtpVerify.this, otpResendResponse.message, Toast.LENGTH_SHORT).show();
                        } else if (otpResendResponse.status.equals("0")) {
                            progressDialog1.dismiss();
                            Toast.makeText(Mobile_OtpVerify.this, otpResendResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<OtpResendResponse> call, Throwable t) {
                        progressDialog1.dismiss();
                        Toast.makeText(Mobile_OtpVerify.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
