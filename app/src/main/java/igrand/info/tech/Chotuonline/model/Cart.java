package igrand.info.tech.Chotuonline.model;

public class Cart {

    private int product_image;
    private String product_title;
    private String name;
    private String pack;
    private String mrp;
    private String discount;
    private String save;

    public Cart(int product_image, String product_title, String name, String pack, String mrp, String discount, String save) {
        this.product_image = product_image;
        this.product_title = product_title;
        this.name = name;
        this.pack = pack;
        this.mrp = mrp;
        this.discount = discount;
        this.save = save;
    }

    public int getProduct_image() {
        return product_image;
    }

    public void setProduct_image(int product_image) {
        this.product_image = product_image;
    }

    public String getProduct_title() {
        return product_title;
    }

    public void setProduct_title(String product_title) {
        this.product_title = product_title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPack() {
        return pack;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getSave() {
        return save;
    }

    public void setSave(String save) {
        this.save = save;
    }
}
