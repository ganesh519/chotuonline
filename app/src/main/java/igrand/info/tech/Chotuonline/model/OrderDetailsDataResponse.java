package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class OrderDetailsDataResponse {

    @SerializedName("order_id")
    @Expose
    public String orderId;
    @SerializedName("order_no")
    @Expose
    public String orderNo;
    @SerializedName("invoice_no")
    @Expose
    public String invoiceNo;
    @SerializedName("payment_option")
    @Expose
    public String paymentOption;
    @SerializedName("phone_number")
    @Expose
    public String phoneNumber;
    @SerializedName("order_items")
    @Expose
    public Integer orderItems;
    @SerializedName("sub_total")
    @Expose
    public String subTotal;
    @SerializedName("delivery_charges")
    @Expose
    public String deliveryCharges;
    @SerializedName("total")
    @Expose
    public String total;
    @SerializedName("delivery_date")
    @Expose
    public String deliveryDate;
    @SerializedName("delivery_time")
    @Expose
    public String deliveryTime;
    @SerializedName("delivery_address")
    @Expose
    public String deliveryAddress;
    @SerializedName("order_status")
    @Expose
    public String orderStatus;
    @SerializedName("order_datetime")
    @Expose
    public String orderDatetime;
    @SerializedName("customer_name")
    @Expose
    public String customerName;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("orderId", orderId).append("orderNo", orderNo).append("invoiceNo", invoiceNo).append("paymentOption", paymentOption).append("phoneNumber", phoneNumber).append("orderItems", orderItems).append("subTotal", subTotal).append("deliveryCharges", deliveryCharges).append("total", total).append("deliveryDate", deliveryDate).append("deliveryTime", deliveryTime).append("deliveryAddress", deliveryAddress).append("orderStatus", orderStatus).append("orderDatetime", orderDatetime).append("customerName", customerName).toString();
    }

}
