package igrand.info.tech.Chotuonline.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.ContactDataResponse;
import igrand.info.tech.Chotuonline.model.ContactFormResponse;
import igrand.info.tech.Chotuonline.model.ContactResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactUsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    double lat;
    double lon;
    ImageView back;
    TextView customernumber,companyname,address,mobilenumber,page;
    ApiInterface apiInterface,apiInterface1;
    String cmail,ccname,caddress,cmobile,name;
    LatLngBounds bounds;
    LatLngBounds.Builder boundsBuilder;
    Marker marker;
    float zoomLevel;
    Button bt_changepass_update;
    EditText edt_name,edt_email,edt_mobilenumber,edt_subject,edt_message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        customernumber=findViewById(R.id.customernumber);
        companyname=findViewById(R.id.companyname);
        address=findViewById(R.id.address);
        mobilenumber=findViewById(R.id.mobilenumber);
        bt_changepass_update=findViewById(R.id.bt_changepass_update);
        edt_name=findViewById(R.id.edt_name);
        edt_email=findViewById(R.id.edt_email);
        edt_mobilenumber=findViewById(R.id.edt_mobilenumber);
        edt_subject=findViewById(R.id.edt_subject);
        edt_message=findViewById(R.id.edt_message);

        bt_changepass_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String c_name=edt_name.getText().toString().trim();
                String c_email=edt_email.getText().toString().trim();
                String c_mobile=edt_mobilenumber.getText().toString().trim();
                String c_subject=edt_subject.getText().toString().trim();
                String c_message=edt_message.getText().toString().trim();

                final ProgressDialog progressDialog = new ProgressDialog(ContactUsActivity.this);
                progressDialog.setMessage("wait...");
                progressDialog.show();
                apiInterface1=ApiClient.getClient().create(ApiInterface.class);
                Call<ContactFormResponse> contactFormResponseCall=apiInterface1.ContactuFom(c_name,c_email,c_mobile,c_subject,c_message);
                contactFormResponseCall.enqueue(new Callback<ContactFormResponse>() {
                    @Override
                    public void onResponse(Call<ContactFormResponse> call, Response<ContactFormResponse> response) {
                        if (response.isSuccessful());
                        ContactFormResponse contactFormResponse=response.body();
                        if (contactFormResponse.status.equals("1")){
                            progressDialog.dismiss();
                            Toast.makeText(ContactUsActivity.this, contactFormResponse.message, Toast.LENGTH_SHORT).show();
                            edt_name.setText("");
                            edt_email.setText("");
                            edt_mobilenumber.setText("");
                            edt_subject.setText("");
                            edt_message.setText("");
                        }
                        else if (contactFormResponse.status.equals("0")){
                            progressDialog.dismiss();
                            Toast.makeText(ContactUsActivity.this, contactFormResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<ContactFormResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(ContactUsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        boundsBuilder=new LatLngBounds.Builder();
        final ProgressDialog progressDialog = new ProgressDialog(ContactUsActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<ContactResponse> call=apiInterface.Contactus();
        call.enqueue(new Callback<ContactResponse>() {
            @Override
            public void onResponse(Call<ContactResponse> call, Response<ContactResponse> response) {
                if (response.isSuccessful());
                ContactResponse contactResponse=response.body();
                ContactDataResponse contactDataResponse=contactResponse.data;
                lat =  Double.parseDouble(contactDataResponse.latitude);
                lon =  Double.parseDouble(contactDataResponse.longitude);

                final LatLng sydney = new LatLng(lat, lon);
                boundsBuilder.include(sydney);
                bounds=boundsBuilder.build();
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                marker=mMap.addMarker(new MarkerOptions().position(sydney));

                mMap.getUiSettings().setZoomControlsEnabled(false);
                mMap.getUiSettings().setCompassEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.getUiSettings().setMapToolbarEnabled(true);
                mMap.getUiSettings().setZoomGesturesEnabled(true);
                mMap.getUiSettings().setScrollGesturesEnabled(true);
                mMap.getUiSettings().setTiltGesturesEnabled(true);
                mMap.getUiSettings().setRotateGesturesEnabled(true);
//                mMap.setMyLocationEnabled(true);

                try {
                     zoomLevel = (float) 18.0;
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomLevel));
                } catch (IllegalStateException e) {
// layout not yet initialized
                    final View mapView = getFragmentManager()
                            .findFragmentById(R.id.map).getView();
                    if (mapView.getViewTreeObserver().isAlive()) {
                        mapView.getViewTreeObserver().addOnGlobalLayoutListener(
                                new ViewTreeObserver.OnGlobalLayoutListener() {
                                    @SuppressWarnings("deprecation")
                                    @SuppressLint("NewApi")
// We check which build version we are using.
                                    @Override
                                    public void onGlobalLayout() {
                                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                                            mapView.getViewTreeObserver()
                                                    .removeGlobalOnLayoutListener(this);
                                        } else {
                                            mapView.getViewTreeObserver()
                                                    .removeOnGlobalLayoutListener(this);
                                        }
                                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomLevel));
                                    }
                                });
                    }
                }

                cmail=contactDataResponse.emailId;
                ccname=contactDataResponse.companyName+",";
                caddress=contactDataResponse.address;
                cmobile=contactDataResponse.phoneNumber;

                customernumber.setText(cmail);
                companyname.setText(ccname);
                address.setText(caddress);
                mobilenumber.setText(cmobile);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ContactResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ContactUsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        // Add a marker in Sydney and move the camera

    }
}
