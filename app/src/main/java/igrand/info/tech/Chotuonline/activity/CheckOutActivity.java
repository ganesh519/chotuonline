package igrand.info.tech.Chotuonline.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.REcyclerSotAdapter;
import igrand.info.tech.Chotuonline.model.AddressBookDataResoponse;
import igrand.info.tech.Chotuonline.model.Category_spinner;
import igrand.info.tech.Chotuonline.model.DefaultAdressResponse;
import igrand.info.tech.Chotuonline.model.SlotDataDetailsResponse;
import igrand.info.tech.Chotuonline.model.SlotDataResponse;
import igrand.info.tech.Chotuonline.model.SlotResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class
CheckOutActivity extends AppCompatActivity {
    ImageView back;
    Button Pay;
    RelativeLayout add_address;
    public static TextView slot_text;
    static final int DATE_DIALOG_ID = 0;
    private int mYear, mMonth, mDay;
    public static ApiInterface apiInterface;
    Spinner Spinner_slot;
    String Slot_id;
    RelativeLayout slot_relative;
    CustomAdapterSlot customSpinnerAdapter;
    String date;
    public static AlertDialog.Builder builder;
    List<Category_spinner> slot_spinner;
    public static AlertDialog b;
    List<SlotDataResponse> slotDataResponse;
    String add;
    List<AddressBookDataResoponse> addressBookDataResoponse;
    View view;
    public static String Identy;
    public static TextView slot_date;
    public static TextView delivery_address,addresstype;
    String Address_id;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout_activity);
        slot_text = findViewById(R.id.slot_text);
        slot_relative = findViewById(R.id.slot_relative);
        slot_date = findViewById(R.id.slot_date);
        delivery_address = findViewById(R.id.delivery_address);
        addresstype=findViewById(R.id.addresstype);
        Data();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SlotResponse> call = apiInterface.DeliverySlot();
        call.enqueue(new Callback<SlotResponse>() {
            @Override
            public void onResponse(Call<SlotResponse> call, Response<SlotResponse> response) {
                if (response.isSuccessful()) ;
                SlotResponse slotResponse = response.body();
                if (slotResponse.status.equals("1")) {
                    slotDataResponse = slotResponse.data;

                    for (int i = 0; i < slotDataResponse.size(); i++) {
                        date = slotDataResponse.get(0).day;
                        slot_date.setText(date);
                        SharedPreferences sharedPreferences1 = getSharedPreferences("date", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences1.edit();
                        editor.putString("DateSlot", date);
                        editor.commit();
                        List<SlotDataDetailsResponse> slotDataDetailsResponse = slotDataResponse.get(i).slots;
//                        slotDataDetailsResponse.get(i).first=true;
                        for (int j = 0; j < slotDataDetailsResponse.size(); j++) {
                            slot_text.setText(slotDataDetailsResponse.get(0).name);
                            Identy = "1";
                        }
                    }
                } else if (slotResponse.status.equals("0")) {
                    Toast.makeText(CheckOutActivity.this, slotResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SlotResponse> call, Throwable t) {
                Toast.makeText(CheckOutActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        slot_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder = new AlertDialog.Builder(CheckOutActivity.this);
                final View view = getLayoutInflater().inflate(R.layout.slot_alert, null);
                RecyclerView recycler_slot = view.findViewById(R.id.recycler_slot);
                if (slotDataResponse != null) {
                    recycler_slot.setAdapter(new REcyclerSotAdapter(getApplicationContext(), slotDataResponse, Identy, slot_date));
                    recycler_slot.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                    recycler_slot.setOverScrollMode(View.OVER_SCROLL_NEVER);
                    recycler_slot.setHasFixedSize(true);
                    recycler_slot.setNestedScrollingEnabled(false);
                }
                builder.setView(view);
                b = builder.create();
                b.show();

            }
        });

//      final SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
//            String user_id = sharedPreferences.getString("user_id", "");
//            final ProgressDialog progressDialog = new ProgressDialog(CheckOutActivity.this);
//            progressDialog.setMessage("wait...");
//            progressDialog.show();
//
//            apiInterface = ApiClient.getClient().create(ApiInterface.class);
//            Call<DefaultAdressResponse> call1 = apiInterface.DefaultAddress(user_id);
//            call1.enqueue(new Callback<DefaultAdressResponse>() {
//                @Override
//                public void onResponse(Call<DefaultAdressResponse> call, Response<DefaultAdressResponse> response) {
//                    if (response.isSuccessful()) ;
//                    DefaultAdressResponse defaultAdressResponse = response.body();
//                    if (defaultAdressResponse.status.equals("1")) {
//                        progressDialog.dismiss();
////                        List<DefaultAddressDataResponse> defaultAddressDataResponse = defaultAdressResponse.data;
////                        if (defaultAddressDataResponse != null){
////                            for (int i = 0; i < defaultAddressDataResponse.size(); i++) {
////                                delivery_address.setText(defaultAddressDataResponse.get(i).house +","+ defaultAddressDataResponse.get(i).residential+","
////                                        + defaultAddressDataResponse.get(i).landmark +","+ defaultAddressDataResponse.get(i).area +","+ defaultAddressDataResponse.get(i).city+
////                                        ","+ defaultAddressDataResponse.get(i).state +","+ defaultAddressDataResponse.get(i).country+"," + defaultAddressDataResponse.get(i).pincode+".");
////
////                                addresstype.setText(defaultAddressDataResponse.get(i).addressType);
////                                Address_id=defaultAddressDataResponse.get(i).addressbookId;
////
////                            }
////                        }
//
//                    }
//                    else if (defaultAdressResponse.status.equals("0")){
//                        progressDialog.dismiss();
////                        SharedPreferences sharedPreferences =getApplicationContext().getSharedPreferences("AddressData", Context.MODE_PRIVATE);
////                        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = sharedPreferences.edit();
////                        editor.clear();
////                        editor.apply();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<DefaultAdressResponse> call, Throwable t) {
//                    progressDialog.dismiss();
//                    Toast.makeText(CheckOutActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//
//                }
//            });


        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        add_address = findViewById(R.id.add_address);
        add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CheckOutActivity.this, Addressbook.class).putExtra("Addressbook","checkout"));
                finish();
            }
        });

//        deliveraddress_recycler = findViewById(R.id.deliveraddress_recycler);
//        final ProgressDialog progressDialog = new ProgressDialog(CheckOutActivity.this);
//        progressDialog.setMessage("wait...");
//        progressDialog.show();
//
//        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
//        final String user_id = sharedPreferences.getString("user_id", "");
//
//        apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        Call<AddressBookResponse> calladd = apiInterface.AddressBook(user_id);
//        calladd.enqueue(new Callback<AddressBookResponse>() {
//            @Override
//            public void onResponse(Call<AddressBookResponse> call, Response<AddressBookResponse> response) {
//                if (response.isSuccessful()) ;
//                AddressBookResponse addressBookResponse = response.body();
//                if (addressBookResponse.status.equals("1")) {
//                    progressDialog.dismiss();
//                    addressBookDataResoponse = addressBookResponse.data;
//                    if (addressBookDataResoponse != null) {
//                        AddAddressRecyclerAdapter1 addAddressRecyclerAdapter = new AddAddressRecyclerAdapter1(getApplicationContext(), addressBookDataResoponse, "checkout");
//                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
//                        deliveraddress_recycler.setLayoutManager(layoutManager);
//                        deliveraddress_recycler.setNestedScrollingEnabled(false);
//                        deliveraddress_recycler.setHasFixedSize(true);
//                        deliveraddress_recycler.setAdapter(addAddressRecyclerAdapter);
//                    }
//                } else if (addressBookResponse.status.equals("0")) {
//                    progressDialog.dismiss();
//                    Toast.makeText(CheckOutActivity.this, addressBookResponse.message, Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<AddressBookResponse> call, Throwable t) {
//                progressDialog.dismiss();
//                Toast.makeText(CheckOutActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });


        Pay = findViewById(R.id.Pay);
        Pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Address_id.equals("")){
                    Toast.makeText(CheckOutActivity.this, "Please select Address", Toast.LENGTH_SHORT).show();
                }
                else {

                    String slot = slot_text.getText().toString().trim();
                    Intent intent = new Intent(CheckOutActivity.this, PaymentActivity.class);
                    intent.putExtra("address",Address_id );
                    intent.putExtra("Timeslot", slot);
                    startActivity(intent);
                }

            }
        });
    }

    public void Data() {


        SharedPreferences sharedPreferences =getApplicationContext().getSharedPreferences("AddressData", Context.MODE_PRIVATE);
        String address = sharedPreferences.getString("ADDRESS", "");
        Address_id=sharedPreferences.getString("ADDRESS_ID","");
        String address_type=sharedPreferences.getString("ADDRESS_TYPE","");

        delivery_address.setText(address);
        addresstype.setText(address_type);




//        String TYPE=getIntent().getStringExtra("Type");
//         Address_id=getIntent().getStringExtra("Address_id");
//        String address_type=getIntent().getStringExtra("Address_Type");
//        String house=getIntent().getStringExtra("House");
//        String resedent=getIntent().getStringExtra("Resedent");
//        String landemark=getIntent().getStringExtra("Landmark");
//        String area=getIntent().getStringExtra("Area");
//        String city=getIntent().getStringExtra("City");
//        String state=getIntent().getStringExtra("State");
//        String pindcode=getIntent().getStringExtra("Pincode");
//
//
//        if (TYPE == null){
//
//
//        }
//        else {
//
//            delivery_address.setText(house+","+resedent+","+landemark+","+area+","+city+","+state+","+pindcode+".");
//            addresstype.setText(address_type);
//        }
    }

    class CustomAdapterSlot extends ArrayAdapter<String> {

        private Context activity;
        private List<Category_spinner> data;
        public Resources res;
        Category_spinner tempValues = null;
        LayoutInflater inflater;

        /*************  CustomSpinnerAdapter Constructor *****************/
        public CustomAdapterSlot(
                Context activity,
                int textViewResourceId,
                List data
        ) {
            super(activity, textViewResourceId, data);

            /********** Take passed values **********/
            this.activity = activity;
            this.data = data;

            /***********  Layout inflator to call external xml layout () **********************/
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        // This funtion called for each row ( Called data.size() times )
        public View getCustomView(int position, View convertView, ViewGroup parent) {

            /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
            View row = inflater.inflate(R.layout.spinner_slot, parent, false);

            /***** Get each Model object from Arraylist ********/
            tempValues = null;
            tempValues = (Category_spinner) data.get(position);

            TextView label = (TextView) row.findViewById(R.id.qantity);
            TextView label1 = row.findViewById(R.id.price);
            String paint = slot_spinner.get(position).getStartlimit();
            if (paint.equals("0")) {
                label.setPaintFlags(label.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                label.setText(tempValues.getQuantiti());
            } else {
                label.setText(tempValues.getQuantiti());
            }
            label.setTag(tempValues.getId());

            label1.setText(tempValues.getPricee());
            return row;
        }

    }
}
