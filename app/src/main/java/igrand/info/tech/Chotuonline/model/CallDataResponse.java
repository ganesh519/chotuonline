package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CallDataResponse {


    @SerializedName("number")
    @Expose
    public String number;
    @SerializedName("id")
    @Expose
    public String id;
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("number", number).toString();
    }

}
