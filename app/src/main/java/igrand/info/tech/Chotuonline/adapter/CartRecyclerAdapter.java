package igrand.info.tech.Chotuonline.adapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.CartActivity;
import igrand.info.tech.Chotuonline.model.AddCartResponse;
import igrand.info.tech.Chotuonline.model.CartDelete;
import igrand.info.tech.Chotuonline.model.CartViewItemsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;




public class CartRecyclerAdapter extends RecyclerView.Adapter<CartRecyclerAdapter.Holder> {

    List<CartViewItemsResponse> cartViewItemsResponses;
    Context context;
    ApiInterface apiInterface;
    ProgressDialog progressDialog;
    int counter;
    private int lastPosition = -1;

    public CartRecyclerAdapter(Context applicationContext, List<CartViewItemsResponse> cartViewItemsResponse) {
        this.context = applicationContext;
        this.cartViewItemsResponses = cartViewItemsResponse;

    }

    @NonNull
    @Override
    public CartRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_cart, parent, false);

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CartRecyclerAdapter.Holder holder, final int position) {

        setAnimation(holder.itemView,position);
        Picasso.with(context).load(cartViewItemsResponses.get(position).variationImage).into(holder.image);
        holder.title.setText(cartViewItemsResponses.get(position).brand);
        holder.name.setText(cartViewItemsResponses.get(position).productName);
        holder.pack.setText(cartViewItemsResponses.get(position).quantity);
        holder.mrp.setText("₹ " + cartViewItemsResponses.get(position).actualPrice);
        holder.mrp.setPaintFlags(holder.mrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        holder.save.setText("₹ " + cartViewItemsResponses.get(position).discount);
        holder.discount.setText("₹ " + cartViewItemsResponses.get(position).unitPrice);
        holder.noofitems.setText(cartViewItemsResponses.get(position).noOfItems);
        holder.item_price.setText("* "+cartViewItemsResponses.get(position).unitPrice);
        holder.show_value.setText(cartViewItemsResponses.get(position).noOfItems);
        holder.Total_amount.setText(" = "+cartViewItemsResponses.get(position).totalUnitPrice);


        holder.decreas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                final String user_id = sharedPreferences.getString("user_id", "");

//                holder.progressbar_add_increase.setVisibility(View.VISIBLE);
                final String  Variationid =cartViewItemsResponses.get(position).variationId;
                String Startlimit = cartViewItemsResponses.get(position).startLimit;
                String  Endlimit =cartViewItemsResponses.get(position).endLimit;
                String value="1";
                String operator = "minus";
                SharedPreferences sharedPreference =context.getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
                String device = sharedPreference.getString("mobile_id","");

                int equal=Integer.valueOf(holder.show_value.getText().toString().trim());

                if (equal == 1) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle("Remove Item");
                    alertDialog.setMessage("Are you sure you want to remove this item?");
                    alertDialog.setPositiveButton("Remove Item", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            CartDelete(user_id, Variationid);

                        }
                    });
                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            holder.progressbar_add_increase.setVisibility(View.GONE);
                            dialog.cancel();
                        }
                    });

                    alertDialog.show();
                }
                else {

                    holder.progressbar_add_increase.setVisibility(View.VISIBLE);
                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<AddCartResponse> cartResponseCall = apiInterface.AddCart(user_id,Variationid,value, Startlimit, Endlimit,operator,device);
                    cartResponseCall.enqueue(new Callback<AddCartResponse>() {
                        @Override
                        public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                            if (response.isSuccessful()) ;
                            AddCartResponse addCartResponse = response.body();
                            if (addCartResponse.status.equals("1")) {
                                Integer count=addCartResponse.noOfItems;
                                holder.show_value.setText(Integer.toString(count));

                                ((CartActivity)context).init();


                                holder.progressbar_add_increase.setVisibility(View.GONE);
//                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                            } else if (addCartResponse.status.equals("0")) {
                                holder.progressbar_add_increase.setVisibility(View.GONE);
                                Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<AddCartResponse> call, Throwable t) {
                            holder. progressbar_add_increase.setVisibility(View.GONE);
                            Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

                }
            }
        });

        holder.increas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");

                String v_id=cartViewItemsResponses.get(position).variationId;
                String item="1";
                String operator = "plus";
                String start_limit= (String) cartViewItemsResponses.get(position).startLimit;
                String end_limit= (String) cartViewItemsResponses.get(position).endLimit;
                SharedPreferences sharedPreference =context.getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
                String device = sharedPreference.getString("mobile_id","");

                holder.progressbar_add_increase.setVisibility(View.VISIBLE);
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddCartResponse> cartResponseCall = apiInterface.AddCart(user_id,v_id,item , start_limit, end_limit,operator,device);
                cartResponseCall.enqueue(new Callback<AddCartResponse>() {
                    @Override
                    public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                        if (response.isSuccessful()) ;
                        AddCartResponse addCartResponse = response.body();
                        if (addCartResponse.status.equals("1")) {
                            holder.progressbar_add_increase.setVisibility(View.GONE);
                            Integer i_cart=addCartResponse.noOfItems;
                            holder.show_value.setText(Integer.toString(i_cart));
//                            holder.discount.setText("24");
//                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                            ((CartActivity)context).init();

                        } else if (addCartResponse.status.equals("0")) {
                            holder.progressbar_add_increase.setVisibility(View.GONE);
                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddCartResponse> call, Throwable t) {
                        holder.progressbar_add_increase.setVisibility(View.GONE);
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        holder.cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");

                String v_id=cartViewItemsResponses.get(position).variationId;
                CartDelete(user_id,v_id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartViewItemsResponses.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title, name, pack, mrp, save, discount,noofitems,item_price,Total_amount;
        TextView show_value;
        ImageView decreas, increas,cancel1;
        LinearLayout linear_add, linear_cart;
        RelativeLayout Relative_increase;
        ProgressBar progressbar_add_increase;


        public Holder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            name = itemView.findViewById(R.id.name);
            pack = itemView.findViewById(R.id.pack);
            mrp = itemView.findViewById(R.id.mrp);
            save = itemView.findViewById(R.id.save);
            discount = itemView.findViewById(R.id.discount);
            noofitems=itemView.findViewById(R.id.noofitems);
            item_price=itemView.findViewById(R.id.item_price);
            Total_amount=itemView.findViewById(R.id.Total_amount);

            show_value = itemView.findViewById(R.id.show_value);
            decreas = itemView.findViewById(R.id.decreas);
            increas = itemView.findViewById(R.id.increas);

            linear_add = itemView.findViewById(R.id.linear_add);
            linear_cart = itemView.findViewById(R.id.linear_cart);
            cancel1=itemView.findViewById(R.id.cancel1);

            Relative_increase = itemView.findViewById(R.id.Relative_increase);

            progressbar_add_increase = itemView.findViewById(R.id.progressbar_add_increase);

        }
    }
    private void CartDelete(String user_id,String v_id){
        SharedPreferences sharedPreference =context.getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
        String device = sharedPreference.getString("mobile_id","");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CartDelete> cartDeleteCall=apiInterface.CartDelete(user_id,v_id,device);
        cartDeleteCall.enqueue(new Callback<CartDelete>() {
            @Override
            public void onResponse(Call<CartDelete> call, Response<CartDelete> response) {
                if (response.isSuccessful());
                CartDelete cartDelete=response.body();
                if (cartDelete.status.equals("1")){
                    Toast.makeText(context, cartDelete.message, Toast.LENGTH_SHORT).show();
                    ((CartActivity)context).init();

                }
                else if (cartDelete.status.equals("0")){
                    Toast.makeText(context, cartDelete.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartDelete> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();


            }
        });
    }

    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
