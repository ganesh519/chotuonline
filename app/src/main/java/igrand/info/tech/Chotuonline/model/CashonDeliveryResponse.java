package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CashonDeliveryResponse {


    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("cash")
    @Expose
    public String cash;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("message", message).append("type", type).append("cash", cash).toString();
    }
}
