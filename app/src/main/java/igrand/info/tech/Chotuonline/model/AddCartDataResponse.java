package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AddCartDataResponse {

    @SerializedName("variation_id")
    @Expose
    public String variationId;
    @SerializedName("no_of_items")
    @Expose
    public String no_of_items;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("variationId", variationId).append("no_of_items", no_of_items).toString();
    }
}
