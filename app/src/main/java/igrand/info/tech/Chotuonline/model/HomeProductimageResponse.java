package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class HomeProductimageResponse implements Serializable {


    @SerializedName("image")
    @Expose
    public String image;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("image", image).toString();
    }

}
