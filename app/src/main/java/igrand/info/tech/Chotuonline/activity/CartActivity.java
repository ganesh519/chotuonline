package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.CartRecyclerAdapter;
import igrand.info.tech.Chotuonline.model.CartViewDataResponse;
import igrand.info.tech.Chotuonline.model.CartViewItemsResponse;
import igrand.info.tech.Chotuonline.model.CartViewResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ImageView back;
    TextView checkout, shopping, empty_shopping;
    ApiInterface apiInterface;
    TextView price, items, subTotal, deliveryCharges, Totalamount, saveamount;
    String Items, Subtotal, Deliverycharges, totalamount, Saveamount;
    String Price;
    RelativeLayout Cart_empty, Cart_main,cart_relative,relative_hide;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_activity);

       init();
    }

    public void init(){
        cart_relative=findViewById(R.id.cart_relative);
        relative_hide=findViewById(R.id.relative_hide);

        Cart_empty=findViewById(R.id.Cart_empty);
        Cart_main=findViewById(R.id.Cart_main);
        empty_shopping=findViewById(R.id.empty_shopping);
        empty_shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CartActivity.this,MainActivity.class));
            }
        });
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CartActivity.this,MainActivity.class));
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            }
        });
        checkout = findViewById(R.id.checkout);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");
                if (TextUtils.isEmpty(user_id)){
                    startActivity(new Intent(CartActivity.this, LoginActivity.class).putExtra("signup","login"));

                }else {
                    startActivity(new Intent(CartActivity.this, CheckOutActivity.class));
                }
            }
        });

        shopping = findViewById(R.id.shopping);
        shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CartActivity.this, MainActivity.class));
            }
        });
        price = findViewById(R.id.price);
        items = findViewById(R.id.items);
        subTotal = findViewById(R.id.subTotal);
        deliveryCharges = findViewById(R.id.deliveryCharges);
        Totalamount = findViewById(R.id.Totalamount);
        saveamount = findViewById(R.id.saveamount);

        recyclerView = (RecyclerView) findViewById(R.id.cart_recycler);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences.getString("user_id", "");
        progressDialog = new ProgressDialog(CartActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        SharedPreferences sharedPreference =getApplicationContext().getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
        String device = sharedPreference.getString("mobile_id","");
        if (user_id.equals("")){
            Listing(user_id,device);
        }
        else {
            Listing(user_id,device);
        }

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        startActivity(new Intent(CartActivity.this,MainActivity.class));
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }

    private void Listing(String userid,String deviceid){

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CartViewResponse> cartViewResponseCall = apiInterface.CartView(userid,deviceid);
        cartViewResponseCall.enqueue(new Callback<CartViewResponse>() {
            @Override
            public void onResponse(Call<CartViewResponse> call, Response<CartViewResponse> response) {
                if (response.isSuccessful()) ;
                CartViewResponse cartViewResponse = response.body();
                if (cartViewResponse.status.equals("1")) {
                    relative_hide.setVisibility(View.VISIBLE);
                    Cart_empty.setVisibility(View.GONE);
                    Cart_main.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    CartViewDataResponse cartViewDataResponse = cartViewResponse.data;
                    Price = "₹ " +cartViewDataResponse.totalAmount;
                    Items = String.valueOf(cartViewDataResponse.totalItems + "items");
                    Subtotal = "₹ " +cartViewDataResponse.subtotal;
                    Deliverycharges = "₹ " + cartViewDataResponse.deliveryCharges;
                    totalamount = "₹ " +cartViewDataResponse.totalAmount;
                    Saveamount = "₹ " + cartViewDataResponse.totalSaveAmount;

                    price.setText(Price);
                    items.setText(Items);
                    subTotal.setText(Subtotal);
                    deliveryCharges.setText(Deliverycharges);
                    Totalamount.setText(totalamount);
                    saveamount.setText(Saveamount);

                    List<CartViewItemsResponse> cartViewItemsResponse = cartViewDataResponse.itemsInfo;

                    CartRecyclerAdapter cartRecyclerAdapter = new CartRecyclerAdapter(CartActivity.this, cartViewItemsResponse);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setNestedScrollingEnabled(false);
                    recyclerView.setAdapter(cartRecyclerAdapter);

                }
                else if (cartViewResponse.status.equals("0")) {
                    progressDialog.dismiss();
                    relative_hide.setVisibility(View.GONE);
//                    Toast.makeText(CartActivity.this, cartViewResponse.message, Toast.LENGTH_SHORT).show();
                    Cart_empty.setVisibility(View.VISIBLE);
                    Cart_main.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<CartViewResponse> call, Throwable t) {
                progressDialog.dismiss();
                relative_hide.setVisibility(View.GONE);
                Cart_empty.setVisibility(View.VISIBLE);
                Cart_main.setVisibility(View.GONE);
                Toast.makeText(CartActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
