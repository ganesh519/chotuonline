package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AllCategoryResponse {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("sort_order")
    @Expose
    public String sortOrder;
    @SerializedName("status")
    @Expose
    public String status;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("code", code).append("image", image).append("sortOrder", sortOrder).append("status", status).toString();
    }

}
