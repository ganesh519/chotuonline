package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.MyWalletResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyWalletActivity extends AppCompatActivity {
    ImageView back;
    ApiInterface apiInterface;
    TextView balance;
    String balancee;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mywallet_activity);

        back = findViewById(R.id.back);
        balance = findViewById(R.id.balance);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences.getString("user_id", "");

        final ProgressDialog progressDialog = new ProgressDialog(MyWalletActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MyWalletResponse> call = apiInterface.MyWallet(user_id);
        call.enqueue(new Callback<MyWalletResponse>() {
            @Override
            public void onResponse(Call<MyWalletResponse> call, Response<MyWalletResponse> response) {
                if (response.isSuccessful()) ;
                MyWalletResponse myWalletResponse = response.body();
                if (myWalletResponse.status.equals("1")) {
                    progressDialog.dismiss();
                    balancee = (String) myWalletResponse.data;
                    balance.setText(balancee);
                    Toast.makeText(MyWalletActivity.this, myWalletResponse.message, Toast.LENGTH_SHORT).show();

                }
                else if (myWalletResponse.status.equals("0")) {
                    progressDialog.dismiss();
                    Toast.makeText(MyWalletActivity.this, myWalletResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MyWalletResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MyWalletActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
