package igrand.info.tech.Chotuonline.model;

public class CategoryMain {

    private String name;

    public CategoryMain(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
