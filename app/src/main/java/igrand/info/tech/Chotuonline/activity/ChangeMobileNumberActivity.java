package igrand.info.tech.Chotuonline.activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;

import igrand.info.tech.Chotuonline.model.ChangeMobilenumberResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangeMobileNumberActivity extends AppCompatActivity {
    ImageView back;
    ApiInterface apiInterface;
    EditText changemobile_number;
    Button update;
    String MobilePattern = "[0-9]{10}";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_mobile_number);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        changemobile_number = findViewById(R.id.changemobile_number);
        update = findViewById(R.id.update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMobile();

            }
        });
    }

    public void getMobile() {
        String mobile = changemobile_number.getText().toString().trim();

        if (mobile.equals("")) {
            Toast.makeText(this, "Enter Mobile Number", Toast.LENGTH_SHORT).show();
        }
        else if(!mobile.matches(MobilePattern)) {
            Toast.makeText(getApplicationContext(), "Please enter valid 10 digit mobile number", Toast.LENGTH_SHORT).show();
        }
        else {

            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
            String user_id = sharedPreferences.getString("user_id", "");

            final ProgressDialog progressDialog = new ProgressDialog(ChangeMobileNumberActivity.this);
            progressDialog.setMessage("wait...");
            progressDialog.show();

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ChangeMobilenumberResponse> changeMobilenumberResponseCall = apiInterface.ChangeMobilenumber(user_id, mobile);
            changeMobilenumberResponseCall.enqueue(new Callback<ChangeMobilenumberResponse>() {
                @Override
                public void onResponse(Call<ChangeMobilenumberResponse> call, Response<ChangeMobilenumberResponse> response) {
                    if (response.isSuccessful());
                    ChangeMobilenumberResponse changeMobilenumberResponse = response.body();

                    if (changeMobilenumberResponse.status.equals("1")) {
                        progressDialog.dismiss();
                        String phone = changeMobilenumberResponse.data;
//                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("mobile", Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = sharedPreferences.edit();
//                        editor.putString("MobileNumber", phone);
//                        editor.commit();
//                        Toast.makeText(ChangeMobileNumberActivity.this, changeMobilenumberResponse.message, Toast.LENGTH_SHORT).show();
//                        startActivity(new Intent(ChangeMobileNumberActivity.this, MobilenumberOtp.class));
                        Intent intent=new Intent(ChangeMobileNumberActivity.this,MobilenumberOtp.class);
                        intent.putExtra("MobileNumber",phone);
                        startActivity(intent);
                        finish();
                    }
                    else if (changeMobilenumberResponse.status.equals("0")) {
                        progressDialog.dismiss();
                        Toast.makeText(ChangeMobileNumberActivity.this, changeMobilenumberResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ChangeMobilenumberResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(ChangeMobileNumberActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
