package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ProfileDataResponse {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("email_id")
    @Expose
    public String emailId;
    @SerializedName("phone_number")
    @Expose
    public String phoneNumber;
    @SerializedName("date_of_birth")
    @Expose
    public String dateOfBirth;
    @SerializedName("alternate_number")
    @Expose
    public String alternateNumber;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("firstName", firstName).append("lastName", lastName).append("image", image).append("emailId", emailId).append("phoneNumber", phoneNumber).append("dateOfBirth", dateOfBirth).append("alternateNumber", alternateNumber).toString();
    }

}
