package igrand.info.tech.Chotuonline.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.fragments.FragmentAlerts;
import igrand.info.tech.Chotuonline.fragments.Fragmentoffers;

public class NotificationsActivity extends AppCompatActivity {

    ImageView back;
    RecyclerView recycler_notification;
    ApiInterface apiInterface;
    Context context;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapterNotifications viewPagerAdapter;
    TextView noti_name;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_activity);
        viewPager = (ViewPager) findViewById(R.id.viewPager_notifications);
        viewPagerAdapter = new ViewPagerAdapterNotifications(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new FragmentAlerts(), "Notifications");
        viewPagerAdapter.addFragment(new Fragmentoffers(), "Offers");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs_notification);
        tabLayout.setupWithViewPager(viewPager);
        noti_name=findViewById(R.id.noti_name);

        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        String notifi=getIntent().getStringExtra("Notifications");
        if (! notifi.equals("")){
            if (notifi.equals("offers")){
                noti_name.setText("OFFERS");
                viewPager.setCurrentItem(1);
            }
            else {
                noti_name.setText("NOTIFICATIONS");
                viewPager.setCurrentItem(0);
            }
        }
        else {
            noti_name.setText("NOTIFICATIONs");
            viewPager.setCurrentItem(0);
        }

    }
    public class ViewPagerAdapterNotifications extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapterNotifications(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

    }

}
