package igrand.info.tech.Chotuonline.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.util.List;
import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.MainActivity;
import igrand.info.tech.Chotuonline.adapter.CategoryMainListAdapter;
import igrand.info.tech.Chotuonline.adapter.CategorymainRecyclerAdapter;
import igrand.info.tech.Chotuonline.adapter.SubCategoryMainListAdapter;
import igrand.info.tech.Chotuonline.model.AllCategorySubCategoryDataREsponse;
import igrand.info.tech.Chotuonline.model.AllCategorySubCategoryResponse;
import igrand.info.tech.Chotuonline.model.CategoryMainListDataREsponse;
import igrand.info.tech.Chotuonline.model.CategoryMainListResponse;
import igrand.info.tech.Chotuonline.model.SubSubCategoryDataResponse;
import igrand.info.tech.Chotuonline.model.SubSubCategoryResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class CategoryMainListFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView mainlist_recycler;
    public RecyclerView.Adapter adapter;
    ApiInterface apiInterface;
    Context context;
    ProgressDialog progressDialog;
    private static String cat_id, u_id, cat_name, subcat_id,device;
    private static String subsubcategory_id, subcategory_name;
    List<AllCategorySubCategoryDataREsponse> allCategorySubCategoryDataREsponse;
    RelativeLayout horizontal;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categorymainlist, null);

        MainActivity.user_iv.setVisibility(View.GONE);
        MainActivity.logo.setVisibility(View.GONE);
        MainActivity.main_search.setVisibility(View.VISIBLE);
        MainActivity.category_text.setVisibility(View.VISIBLE);

        horizontal = view.findViewById(R.id.horizontal);
        recyclerView = (RecyclerView) view.findViewById(R.id.category_recycler);
        mainlist_recycler = (RecyclerView) view.findViewById(R.id.mainlist_recycler);

        SharedPreferences sharedPreferences1 = getActivity().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        u_id = sharedPreferences1.getString("user_id", "");

        SharedPreferences sharedPreference =getActivity().getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
         device = sharedPreference.getString("mobile_id","");

        cat_id = getArguments().getString("category_id");
        cat_name = getArguments().getString("category_name");
        subcat_id = getArguments().getString("subcategory_id");
        String AllCategory = getArguments().getString("all_category");
        subsubcategory_id = getArguments().getString("sub_sub_id");
        subcategory_name = getArguments().getString("category_name");
        String Subsubcategory = getArguments().getString("SubSubCategory");

        MainActivity.category_text.setText(cat_name);

        if (AllCategory.equals("Allcategory")) {
            horizontal.setVisibility(View.GONE);
            catr(u_id, cat_id, subcat_id, getActivity(),device);

        }
        else if (Subsubcategory.equals("subsubcategory")) {
            horizontal.setVisibility(View.GONE);
            subsubcategory(u_id, subsubcategory_id);
            MainActivity.category_text.setText(subcategory_name);
        }
        else {
            horizontal.setVisibility(View.VISIBLE);
            catr(u_id, cat_id, "", getActivity(),device);
        }


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AllCategorySubCategoryResponse> call1 = apiInterface.AllCategorySubCategory(cat_id);
        call1.enqueue(new Callback<AllCategorySubCategoryResponse>() {
            @Override
            public void onResponse(Call<AllCategorySubCategoryResponse> call, Response<AllCategorySubCategoryResponse> response) {
                if (response.isSuccessful()) ;
                AllCategorySubCategoryResponse allCategorySubCategoryResponse = response.body();
                if (allCategorySubCategoryResponse.status.equals("1")) {
                    allCategorySubCategoryDataREsponse = allCategorySubCategoryResponse.data;
                    recyclerView.setAdapter(new CategorymainRecyclerAdapter(allCategorySubCategoryDataREsponse, getActivity(), CategoryMainListFragment.this));
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setNestedScrollingEnabled(false);
                }
                else if (allCategorySubCategoryResponse.status.equals("0")) {
                    Toast.makeText(getActivity(), allCategorySubCategoryResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AllCategorySubCategoryResponse> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        return view;
    }

    public void cat(String subcat_id, final Context context) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        Call<CategoryMainListResponse> call = apiInterface.ProductListing(u_id, cat_id, subcat_id,device);
        call.enqueue(new Callback<CategoryMainListResponse>() {
            @Override
            public void onResponse(Call<CategoryMainListResponse> call, Response<CategoryMainListResponse> response) {

                try {
                    if (response.isSuccessful()) ;
                    CategoryMainListResponse categoryMainListResponse = response.body();
                    if (categoryMainListResponse.status.equals("1")) {
                        mainlist_recycler.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        List<CategoryMainListDataREsponse> categoryMainListDataREsponse = categoryMainListResponse.data;
                        if (categoryMainListDataREsponse != null) {
                            mainlist_recycler.setAdapter(new CategoryMainListAdapter(categoryMainListDataREsponse, context, u_id));
                            mainlist_recycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                            mainlist_recycler.setHasFixedSize(true);
                            mainlist_recycler.setNestedScrollingEnabled(false);
                        }
                        else {
                            mainlist_recycler.setVisibility(View.GONE);
                        }
                    }
                    else if (categoryMainListResponse.status.equals("0")) {
                        progressDialog.dismiss();
                        mainlist_recycler.setVisibility(View.GONE);
                        Toast.makeText(context, categoryMainListResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }
                catch (NumberFormatException n) {
                    n.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<CategoryMainListResponse> call, Throwable t) {
                progressDialog.dismiss();
                mainlist_recycler.setVisibility(View.GONE);
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void catr(final String u_id, String cat_id, String subcat_id, final Context context,String device) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        Call<CategoryMainListResponse> call = apiInterface.ProductListing(u_id, cat_id, subcat_id,device);
        call.enqueue(new Callback<CategoryMainListResponse>() {
            @Override
            public void onResponse(Call<CategoryMainListResponse> call, Response<CategoryMainListResponse> response) {

                try {
                    if (response.isSuccessful()) ;
                    CategoryMainListResponse categoryMainListResponse = response.body();
                    if (categoryMainListResponse.status.equals("1")) {
                        mainlist_recycler.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        List<CategoryMainListDataREsponse> categoryMainListDataREsponse = categoryMainListResponse.data;
                        if (categoryMainListDataREsponse != null) {

                            mainlist_recycler.setAdapter(new CategoryMainListAdapter(categoryMainListDataREsponse, context, u_id));
                            mainlist_recycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                            mainlist_recycler.setHasFixedSize(true);
                            mainlist_recycler.setNestedScrollingEnabled(false);
                        } else {
                            mainlist_recycler.setVisibility(View.GONE);
                        }
                    } else if (categoryMainListResponse.status.equals("0")) {
                        progressDialog.dismiss();
                        mainlist_recycler.setVisibility(View.GONE);
                        Toast.makeText(context, categoryMainListResponse.message, Toast.LENGTH_SHORT).show();
                    }
                } catch (NumberFormatException n) {
                    n.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<CategoryMainListResponse> call, Throwable t) {
                progressDialog.dismiss();
                mainlist_recycler.setVisibility(View.GONE);
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void subsubcategory(String user_id, String SubSub_category_id) {
        ApiInterface apiInterface1 = ApiClient.getClient().create(ApiInterface.class);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("wait...");
        progressDialog.show();

        Call<SubSubCategoryResponse> categoryResponseCall = apiInterface1.SubSubCategoryId(SubSub_category_id, user_id,device);
        categoryResponseCall.enqueue(new Callback<SubSubCategoryResponse>() {
            @Override
            public void onResponse(Call<SubSubCategoryResponse> call, Response<SubSubCategoryResponse> response) {
                if (response.isSuccessful()) ;
                SubSubCategoryResponse subSubCategoryResponse = response.body();

                if (subSubCategoryResponse.status.equals("1")) {
                    progressDialog.dismiss();
                    mainlist_recycler.setVisibility(View.VISIBLE);

                    List<SubSubCategoryDataResponse> subSubCategoryDataResponse = subSubCategoryResponse.data;
                    if (subSubCategoryDataResponse != null) {
                        mainlist_recycler.setAdapter(new SubCategoryMainListAdapter(subSubCategoryDataResponse, getActivity(), u_id));
                        mainlist_recycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        mainlist_recycler.setHasFixedSize(true);
                        mainlist_recycler.setNestedScrollingEnabled(false);

                    } else {
                        mainlist_recycler.setVisibility(View.GONE);
                    }

                } else if (subSubCategoryResponse.status.equals("0")) {
                    progressDialog.dismiss();
                    Toast.makeText(context, subSubCategoryResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SubSubCategoryResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }


}

