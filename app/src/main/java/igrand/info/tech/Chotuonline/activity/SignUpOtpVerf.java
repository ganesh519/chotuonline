package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.MobilenumberOtpResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUpOtpVerf extends AppCompatActivity implements View.OnClickListener{
    Button bt_proceed;
    SmsVerifyCatcher smsVerifyCatcher;
    String otp,code;

    ProgressDialog progressDialog;
    ApiInterface apiInterface;
    TextView countdownText;
    EditText pin_first_edittext,pin_second_edittext,pin_third_edittext,pin_forth_edittext,pin_fifth_edittext,pin_sixth_edittext;
    LinearLayout otpprocess,ll_resend;
    private static CountDownTimer countDownTimer;
    String Otp;
    String e1,e2,e3,e4,e5,e6;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_verification);
//        bt_proceed = findViewById(R.id.bt_proceed);
        otpprocess = findViewById(R.id.otpprocess);
        countdownText = findViewById(R.id.countdownText);
        ll_resend = findViewById(R.id.ll_resend);
        pin_first_edittext = findViewById(R.id.pin_first_edittext);
        pin_second_edittext = findViewById(R.id.pin_second_edittext);
        pin_third_edittext = findViewById(R.id.pin_third_edittext);
        pin_forth_edittext = findViewById(R.id.pin_forth_edittext);
        pin_fifth_edittext = findViewById(R.id.pin_fifth_edittext);
        pin_sixth_edittext = findViewById(R.id.pin_sixth_edittext);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        progressDialog = new ProgressDialog(SignUpOtpVerf.this);
        progressDialog.setMessage("Loading....");

        TimeCount();


        pin_first_edittext.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (pin_first_edittext.getText().length() != 0)
                    pin_second_edittext.requestFocus();

                return false;
            }
        });

        pin_second_edittext.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (pin_second_edittext.getText().length() != 0){
                    pin_third_edittext.requestFocus();
                }else if (pin_second_edittext.getText().length() == 0){
                    pin_first_edittext.requestFocus();
                }
                return false;
            }
        });
        pin_third_edittext.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (pin_third_edittext.getText().length() != 0){
                    pin_forth_edittext.requestFocus();
                }else if (pin_third_edittext.getText().length() == 0){
                    pin_second_edittext.requestFocus();
                }
                return false;

            }
        });
        pin_forth_edittext.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (pin_forth_edittext.getText().length() != 0){
                    pin_fifth_edittext.requestFocus();
                }else if (pin_forth_edittext.getText().length() == 0){
                    pin_third_edittext.requestFocus();
                }
                return false;

            }
        });
        pin_fifth_edittext.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (pin_fifth_edittext.getText().length() != 0){
                    pin_sixth_edittext.requestFocus();

                }else if (pin_fifth_edittext.getText().length() == 0){
                    pin_forth_edittext.requestFocus();
                }

                return false;

            }
        });
        pin_sixth_edittext.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (pin_sixth_edittext.getText().length() == 0)
                    pin_fifth_edittext.requestFocus();
                return false;

            }
        });





        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                code = parseCode(message);
                String  one = String.valueOf(code.charAt(0));
                String  two = String.valueOf(code.charAt(1));
                String  three = String.valueOf(code.charAt(2));
                String  four = String.valueOf(code.charAt(3));
                String  five = String.valueOf(code.charAt(4));
                String  six = String.valueOf(code.charAt(5));
                pin_first_edittext.setText(one);
                pin_second_edittext.setText(two);
                pin_third_edittext.setText(three);
                pin_forth_edittext.setText(four);
                pin_fifth_edittext.setText(five);
                pin_sixth_edittext.setText(six);

                e1 = pin_first_edittext.getText().toString().trim();
                e2 = pin_second_edittext.getText().toString().trim();
                e3 = pin_third_edittext.getText().toString().trim();
                e4 = pin_forth_edittext.getText().toString().trim();
                e5 = pin_fifth_edittext.getText().toString().trim();
                e6 = pin_sixth_edittext.getText().toString().trim();


                if (!TextUtils.isEmpty(e1) && !TextUtils.isEmpty(e2) && !TextUtils.isEmpty(e3) && !TextUtils.isEmpty(e4)
                        && !TextUtils.isEmpty(e5) && !TextUtils.isEmpty(e6) ) {
                    countDownTimer.cancel();
                    otpprocess.setVisibility(View.GONE);
                    ll_resend.setVisibility(View.GONE);

                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.bt_proceed:

                e1 = pin_first_edittext.getText().toString().trim();
                e2 = pin_second_edittext.getText().toString().trim();
                e3 = pin_third_edittext.getText().toString().trim();
                e4 = pin_forth_edittext.getText().toString().trim();
                e5 = pin_fifth_edittext.getText().toString().trim();
                e6 = pin_sixth_edittext.getText().toString().trim();
                Otp = e1+e2+e3+e4+e5+e6;
                progressDialog.show();


                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("mobile", Context.MODE_PRIVATE);
                String mobile_no = sharedPreferences.getString("MobileNumber", "");

                SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                final String user_id = sharedPreferences1.getString("user_id", "");
                final ProgressDialog progressDialog = new ProgressDialog(SignUpOtpVerf.this);
                progressDialog.setMessage("wait...");
                progressDialog.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<MobilenumberOtpResponse> call = apiInterface.MobilenumberOtp(user_id, mobile_no, Otp);
                call.enqueue(new Callback<MobilenumberOtpResponse>() {
                    @Override
                    public void onResponse(Call<MobilenumberOtpResponse> call, Response<MobilenumberOtpResponse> response) {
                        if (response.isSuccessful()) ;
                        MobilenumberOtpResponse mobilenumberOtpResponse = response.body();
                        if (mobilenumberOtpResponse.status.equals("1")) {
                            progressDialog.dismiss();
                            Toast.makeText(SignUpOtpVerf.this, mobilenumberOtpResponse.message, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(SignUpOtpVerf.this, MyAccountActivity.class));
                            finish();
                        } else if (mobilenumberOtpResponse.status.equals("0")) {
                            progressDialog.dismiss();
                            Toast.makeText(SignUpOtpVerf.this, mobilenumberOtpResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<MobilenumberOtpResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(SignUpOtpVerf.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


                break;


        }


    }
    private void TimeCount(){

        ll_resend.setVisibility(View.GONE);
        otpprocess.setVisibility(View.VISIBLE);


        countDownTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                String hms = String.format("%02d", TimeUnit.MILLISECONDS.toSeconds(millis));
                countdownText.setText(hms);//set text
            }

            public void onFinish() {
                otpprocess.setVisibility(View.GONE);
                ll_resend.setVisibility(View.VISIBLE);
            }
        }.start();

    }




    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
