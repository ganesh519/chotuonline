package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.OrderItemsReonse;



public class ItemRecyclerAdapter extends RecyclerView.Adapter<ItemRecyclerAdapter.Holder> {

    List<OrderItemsReonse> orderItemsReonses;
    Context context;
    private int lastPosition = -1;

    public ItemRecyclerAdapter(List<OrderItemsReonse> orderItemsReonses, Context context) {
        this.orderItemsReonses = orderItemsReonses;
        this.context = context;
    }

    @NonNull
    @Override
    public ItemRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemRecyclerAdapter.Holder holder, int position) {

        setAnimation(holder.itemView,position);
        Picasso.with(context).load(orderItemsReonses.get(position).image1).into(holder.image);
        holder.title.setText(orderItemsReonses.get(position).brand);
        holder.name.setText(orderItemsReonses.get(position).productName);
        holder.pack.setText(orderItemsReonses.get(position).quantity);
        holder.gm.setText("₹ " + orderItemsReonses.get(position).price);
        holder.price.setText("₹ " + orderItemsReonses.get(position).price);
        holder.quantity.setText("Quantity : " + orderItemsReonses.get(position).productQuantity);

    }

    @Override
    public int getItemCount() {
        return orderItemsReonses.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title, name, pack, price, quantity, gm;

        public Holder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            name = itemView.findViewById(R.id.name);
            pack = itemView.findViewById(R.id.pack);
            price = itemView.findViewById(R.id.price);
            quantity = itemView.findViewById(R.id.quantity);
            gm = itemView.findViewById(R.id.gm);
        }
    }

    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
