package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.TermsRecyclerAdapter;
import igrand.info.tech.Chotuonline.model.TermsDataResponse;
import igrand.info.tech.Chotuonline.model.TermsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsConditionActivity extends AppCompatActivity {
    ImageView back;
    TextView terms,page,discount_text,like_text,send_text,time_text,lock_text,money_text,right_text,user_text;
    ApiInterface apiInterface;
    String text,name;
    RecyclerView terms_recycler;
    Context ctx;
    LinearLayout linear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);
        back=findViewById(R.id.back);
        terms_recycler=findViewById(R.id.terms_recycler);

        discount_text=findViewById(R.id.discount_text);
        discount_text.setTypeface(discount_text.getTypeface(), Typeface.BOLD);

        like_text=findViewById(R.id.like_text);
        like_text.setTypeface(like_text.getTypeface(), Typeface.BOLD);

        send_text=findViewById(R.id.send_text);
        send_text.setTypeface(send_text.getTypeface(), Typeface.BOLD);

        time_text=findViewById(R.id.time_text);
        time_text.setTypeface(time_text.getTypeface(), Typeface.BOLD);

        lock_text=findViewById(R.id.lock_text);
        lock_text.setTypeface(lock_text.getTypeface(), Typeface.BOLD);

        money_text=findViewById(R.id.money_text);
        money_text.setTypeface(money_text.getTypeface(), Typeface.BOLD);

        right_text=findViewById(R.id.right_text);
        right_text.setTypeface(right_text.getTypeface(), Typeface.BOLD);

        user_text=findViewById(R.id.user_text);
        user_text.setTypeface(user_text.getTypeface(), Typeface.BOLD);

        linear=findViewById(R.id.linear);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final ProgressDialog progressDialog = new ProgressDialog(TermsConditionActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<TermsResponse> call=apiInterface.Terms();
        call.enqueue(new Callback<TermsResponse>() {
            @Override
            public void onResponse(Call<TermsResponse> call, Response<TermsResponse> response) {
                if (response.isSuccessful());
                TermsResponse termsResponse=response.body();
                if (termsResponse.status.equals("1")){
                    linear.setVisibility(View.VISIBLE);
                    List<TermsDataResponse> termsDataResponse=termsResponse.data;
                    for (int i=0;i<termsDataResponse.size();i++){
                        terms_recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                        terms_recycler.setAdapter(new TermsRecyclerAdapter(termsDataResponse, ctx));
                        terms_recycler.setHasFixedSize(true);
                        terms_recycler.setNestedScrollingEnabled(false);
                        progressDialog.dismiss();
                    }

                }
                else if (termsResponse.status.equals("0")){
                    linear.setVisibility(View.GONE);
                    Toast.makeText(TermsConditionActivity.this, termsResponse.message, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<TermsResponse> call, Throwable t) {
                linear.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(TermsConditionActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
