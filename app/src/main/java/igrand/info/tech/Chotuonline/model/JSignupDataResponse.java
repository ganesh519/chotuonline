package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class JSignupDataResponse {

    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("user_name")
    @Expose
    public String userName;
    @SerializedName("otp_code")
    @Expose
    public Integer otpCode;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;
    @SerializedName("email_address")
    @Expose
    public String emailAddress;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("userName", userName).append("otpCode", otpCode).append("mobileNumber", mobileNumber).append("emailAddress", emailAddress).toString();
    }
}
