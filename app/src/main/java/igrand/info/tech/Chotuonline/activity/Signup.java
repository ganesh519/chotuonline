package igrand.info.tech.Chotuonline.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import igrand.info.tech.Chotuonline.ApiClient1;
import igrand.info.tech.Chotuonline.ApiInterface1;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.JSignupResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Signup extends AppCompatActivity {
    EditText name,mobile;
    Button button;
    ApiInterface1 apiInterface;
    String username,phone;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        name=findViewById(R.id.name);
        mobile=findViewById(R.id.mobile);
        button=findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username=name.getText().toString().trim();
                phone=mobile.getText().toString().trim();

                apiInterface= ApiClient1.getClient().create(ApiInterface1.class);
                Call<JSignupResponse> call=apiInterface.JSignup(username,phone);
                call.enqueue(new Callback<JSignupResponse>() {
                    @Override
                    public void onResponse(Call<JSignupResponse> call, Response<JSignupResponse> response) {
                        if (response.isSuccessful());
                        JSignupResponse signupResponse=response.body();
                        if (signupResponse.status == 1){
                            Toast.makeText(Signup.this, signupResponse.message, Toast.LENGTH_SHORT).show();
                        }
                        else if (signupResponse.status == 0){
                            Toast.makeText(Signup.this, signupResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<JSignupResponse> call, Throwable t) {
                        Toast.makeText(Signup.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

    }
}
