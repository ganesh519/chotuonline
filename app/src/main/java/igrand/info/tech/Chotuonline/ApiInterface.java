package igrand.info.tech.Chotuonline;

import igrand.info.tech.Chotuonline.model.AboutResponse;
import igrand.info.tech.Chotuonline.model.AddAddressResponse;
import igrand.info.tech.Chotuonline.model.AddCartResponse;
import igrand.info.tech.Chotuonline.model.AddressBookResponse;
import igrand.info.tech.Chotuonline.model.AllCategorySubCategoryResponse;
import igrand.info.tech.Chotuonline.model.All_CategoryResponse;
import igrand.info.tech.Chotuonline.model.AreaResponse;
import igrand.info.tech.Chotuonline.model.CallResponse;
import igrand.info.tech.Chotuonline.model.CancelOrderResponse;
import igrand.info.tech.Chotuonline.model.CartCountResponse;
import igrand.info.tech.Chotuonline.model.CartDelete;
import igrand.info.tech.Chotuonline.model.CartViewResponse;
import igrand.info.tech.Chotuonline.model.CashonDeliveryResponse;
import igrand.info.tech.Chotuonline.model.CategoryMainListResponse;
import igrand.info.tech.Chotuonline.model.CategoryVariationResponse;
import igrand.info.tech.Chotuonline.model.ChangeMobilenumberResponse;
import igrand.info.tech.Chotuonline.model.ChangePasswordResponse;
import igrand.info.tech.Chotuonline.model.CityResponse;
import igrand.info.tech.Chotuonline.model.ContactFormResponse;
import igrand.info.tech.Chotuonline.model.ContactResponse;
import igrand.info.tech.Chotuonline.model.DateWiseResponse;
import igrand.info.tech.Chotuonline.model.DefaultAdressResponse;
import igrand.info.tech.Chotuonline.model.DeleteAddressResponse;
import igrand.info.tech.Chotuonline.model.EditAddressResponse;
import igrand.info.tech.Chotuonline.model.EditprofileResponse;
import igrand.info.tech.Chotuonline.model.FacbookResponse;
import igrand.info.tech.Chotuonline.model.FaqResponse;
import igrand.info.tech.Chotuonline.model.FirebaseResponse;
import igrand.info.tech.Chotuonline.model.ForgotOtpResponse;
import igrand.info.tech.Chotuonline.model.ForgotpassResendResponse;
import igrand.info.tech.Chotuonline.model.ForgotpassResetResponse;
import igrand.info.tech.Chotuonline.model.ForgotpasswordResponse;
import igrand.info.tech.Chotuonline.model.GoogleLoginResponse;
import igrand.info.tech.Chotuonline.model.HomeResponse;
import igrand.info.tech.Chotuonline.model.LoginMobileResponse;
import igrand.info.tech.Chotuonline.model.LoginResponse;
import igrand.info.tech.Chotuonline.model.MobileOtpVerifyResponse;
import igrand.info.tech.Chotuonline.model.MobilenumberOtpResponse;
import igrand.info.tech.Chotuonline.model.MyOrdersResponse;
import igrand.info.tech.Chotuonline.model.MyWalletResponse;
import igrand.info.tech.Chotuonline.model.NewDeliveryAddressResponse;
import igrand.info.tech.Chotuonline.model.NotificationResponse;
import igrand.info.tech.Chotuonline.model.OffersResponse;
import igrand.info.tech.Chotuonline.model.OrderDetailsResponse;
import igrand.info.tech.Chotuonline.model.OrderItemsResponse;
import igrand.info.tech.Chotuonline.model.OtpResendResponse;
import igrand.info.tech.Chotuonline.model.PaymentMethodsResponse;
import igrand.info.tech.Chotuonline.model.PaymentResponse;
import igrand.info.tech.Chotuonline.model.PrivacyResponse;
import igrand.info.tech.Chotuonline.model.ProductVariationDetailsResponse;
import igrand.info.tech.Chotuonline.model.Product_DetailsResponse;
import igrand.info.tech.Chotuonline.model.ProfileResponse;
import igrand.info.tech.Chotuonline.model.ReorderResponse;
import igrand.info.tech.Chotuonline.model.SearchResponse;
import igrand.info.tech.Chotuonline.model.SignupResponse;
import igrand.info.tech.Chotuonline.model.Signup_otpResponse;
import igrand.info.tech.Chotuonline.model.SingleCancelOrderResponse;
import igrand.info.tech.Chotuonline.model.SlotResponse;
import igrand.info.tech.Chotuonline.model.SubSubCategoryResponse;
import igrand.info.tech.Chotuonline.model.TermsResponse;
import igrand.info.tech.Chotuonline.model.UserWalletResponse;
import igrand.info.tech.Chotuonline.model.ValuesResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("app/login.php")
    Call<LoginResponse> Login(@Field("email") String email,
                              @Field("password") String password,
                              @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("app/signup.php")
    Call<SignupResponse> Signup(@Field("first_name") String first_name,@Field("last_name") String last_name,@Field("email_id") String email_id,@Field("phone_number") String phone_number,
                                @Field("password") String password);
    @FormUrlEncoded
    @POST("app/signup-otp.php")
    Call<Signup_otpResponse> SignupOtp(@Field("otp") String otp);

    @FormUrlEncoded
    @POST("app/my-profile.php")
    Call<ProfileResponse> Profile(@Field("user_id") String user_id);

    @GET("app/privacy-policy.php")
    Call<PrivacyResponse> Privacy();

    @GET("app/terms-condition.php")
    Call<TermsResponse> Terms();

    @GET("app/home.php")
    Call<HomeResponse> Home();

    @FormUrlEncoded
    @POST("app/home.php")
    Call<HomeResponse> PostHome(@Field("user_id") String user_id,@Field("device_id") String device_id);

    @GET("app/contact-us.php")
    Call<ContactResponse> Contactus();

    @FormUrlEncoded
    @POST("app/forgot-password.php")
    Call<ForgotpasswordResponse> ForgotPassword(@Field("username") String username);

    @GET("app/about-us.php")
    Call<AboutResponse> Aboutus();

    @Multipart
    @POST("app/edit-profile.php")
    Call<EditprofileResponse> EditProfile(@Part MultipartBody.Part file,
                                          @Part("user_id") RequestBody user_id,
                                          @Part("first_name") RequestBody first_name,
                                          @Part("last_name") RequestBody last_name,
                                          @Part("email_id") RequestBody email,
                                          @Part("phone_number") RequestBody phone,
                                          @Part("date_of_birth") RequestBody date_of_birth,
                                          @Part("alt_number") RequestBody alt_number);
    @GET("app/faqs.php")
    Call<FaqResponse> Faq();


    @GET("app/notifications.php")
    Call<NotificationResponse> Notifications();

    @GET("app/offers.php")
    Call<OffersResponse> Offers();

    @FormUrlEncoded
    @POST("app/change-password.php")
    Call<ChangePasswordResponse> ChangePassword(@Field("user_id") String user_id, @Field("old_password") String old_password,
                                                @Field("new_password") String new_password, @Field("confirm_password") String confirm_password);

    @FormUrlEncoded
    @POST("app/product-details.php")
    Call<Product_DetailsResponse> ProductDetails(@Field("product_id") String product_id,@Field("user_id") String user_id,@Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("app/variation-details.php")
    Call<ProductVariationDetailsResponse> ProductVariationDetails(@Field("variation_id") String variation_id, @Field("user_id") String user_id,@Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("app/resend-otp.php")
    Call<OtpResendResponse> ReSend(@Field("phone_number") String phone_number);

    @FormUrlEncoded
    @POST("app/category-list.php")
    Call<CategoryMainListResponse> CategoryMainList(@Field("category_id") String category_id, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("app/variation-info.php")
    Call<CategoryVariationResponse> CategoryVariaton(@Field("product_id") String product_id, @Field("user_id") String user_id);

    @GET("app/values.php")
    Call<ValuesResponse> Address();

    @FormUrlEncoded
    @POST("app/addressbook-add.php")
    Call<AddAddressResponse> AddAddress(@Field("user_id") String user_id, @Field("adress_type") String adress_type,
                                        @Field("state") String state, @Field("city") String city,@Field("house") String house,@Field("residential") String residential,
                                        @Field("area") String area,@Field("landmark") String landmark,@Field("default_address") int default_address);

    @FormUrlEncoded
    @POST("app/city.php")
    Call<CityResponse> City(@Field("state_id") String state_id);

    @FormUrlEncoded
    @POST("app/areas.php")
    Call<AreaResponse> Area(@Field("city_id") String city_id);

    @FormUrlEncoded
    @POST("app/address-book.php")
    Call<AddressBookResponse> AddressBook(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("app/addressbook-edit.php")
    Call<EditAddressResponse> EditAddress(@Field("user_id") String user_id, @Field("addressbook_id") String addressbook_id,@Field("adress_type") String adress_type,
                                          @Field("state") String state, @Field("city") String city, @Field("house") String house, @Field("residential") String residential,
                                          @Field("area") String area, @Field("landmark") String landmark, @Field("default_address") int default_address);

    @FormUrlEncoded
    @POST("app/addressbook-delete.php")
    Call<DeleteAddressResponse> AddressDelete(@Field("user_id") String user_id, @Field("addressbook_id") String addressbook_id);

    @FormUrlEncoded
    @POST("app/change-mobile-number.php")
    Call<ChangeMobilenumberResponse> ChangeMobilenumber(@Field("user_id") String user_id, @Field("phone_number") String phone_number);

    @FormUrlEncoded
    @POST("app/change-mobile-number-otp.php")
    Call<MobilenumberOtpResponse> MobilenumberOtp(@Field("user_id") String user_id, @Field("phone_number") String phone_number,
                                                  @Field("otp") String otp);


//    @FormUrlEncoded
//    @POST("app/default-address.php")
//    Call<DefaultAddressResponse> Defaultaddress(@Field("user_id") String user_id, @Field("addressbook_id") String addressbook_id,
//                                                @Field("default_address") int default_address);

    @FormUrlEncoded
    @POST("app/forgot-password-otp.php")
    Call<ForgotOtpResponse> ForgotOtp(@Field("otp") String otp);

    @FormUrlEncoded
    @POST("app/reset-password.php")
    Call<ForgotpassResetResponse> ForgotpassReset(@Field("new_password") String new_password, @Field("confirm_password") String confirm_password,
                                                  @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("app/change-number-resendotp.php")
    Call<ForgotpassResendResponse> ForgotpassResend(@Field("user_id") String user_id, @Field("phone_number") String phone_number);

    @FormUrlEncoded
    @POST("app/login-google-plus.php")
    Call<GoogleLoginResponse> GoogleLogin(@Field("token") String token,
                                          @Field("first_name") String first_name,
                                          @Field("email") String email,
                                          @Field("device_id") String device_id);
    @FormUrlEncoded
    @POST("app/login-facebook.php")
    Call<FacbookResponse> FacebookLogin(@Field("token") String token,
                                        @Field("first_name") String first_name,
                                        @Field("email") String email,
                                         @Field("device_id") String device_id);
    @FormUrlEncoded
    @POST("app/order-details.php")
    Call<OrderDetailsResponse> OrderDetails(@Field("user_id") String user_id, @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("app/my-orders.php")
    Call<MyOrdersResponse> MyOrders(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("app/my-wallet.php")
    Call<MyWalletResponse> MyWallet(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("app/search.php")
    Call<SearchResponse> Search(@Field("search") String search, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("app/order-items.php")
    Call<OrderItemsResponse> OrderItems(@Field("user_id") String user_id, @Field("order_id") String order_id);

    @GET("app/delivery-slot.php")
    Call<SlotResponse> DeliverySlot();

    @FormUrlEncoded
    @POST("app/all-sub-category.php")
    Call<AllCategorySubCategoryResponse> AllCategorySubCategory(@Field("category_id") String category_id);

    @FormUrlEncoded
    @POST("app/cart-add.php")
    Call<AddCartResponse> AddCart(@Field("user_id") String user_id,@Field("variation_id") String variation_id, @Field("no_of_items") String no_of_items,
                                  @Field("start_limit") String start_limit, @Field("end_limit") String end_limit,
                                  @Field("operator") String operator, @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("app/cart-view.php")
    Call<CartViewResponse>

    CartView(@Field("user_id") String user_id,
                                    @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("app/cart-remove.php")
    Call<CartDelete> CartDelete(@Field("user_id") String user_id,
                                @Field("variation_id") String variation_id,
                                @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("app/product-listing.php")
    Call<CategoryMainListResponse> ProductListing(@Field("user_id") String user_id, @Field("category_id") String category_id,
                                                  @Field("subcategory_id") String subcategory_id,@Field("device_id") String device_id);
    @GET("app/all-categories.php")
    Call<All_CategoryResponse> AllCategory();

    @FormUrlEncoded
    @POST("app/delivery-slot.php")
    Call<DateWiseResponse> DateWiseSlot(@Field("selected_date") String selected_date);

    @FormUrlEncoded
    @POST("app/cart-count.php")
    Call<CartCountResponse> CartCount(@Field("user_id") String user_id,
                                      @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("app/sub-sub-category-list.php")
    Call<SubSubCategoryResponse> SubSubCategoryId(@Field("subsubcategory_id") String subsubcategory_id, @Field("user_id") String user_id,@Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("app/re-order.php")
    Call<ReorderResponse> Reorder(@Field("user_id") String user_id, @Field("order_id") String order_id,@Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("app/wallet.php")
    Call<PaymentResponse> PaymentOptions(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("app/user-wallet.php")
    Call<UserWalletResponse> UserWallet(@Field("user_id") String user_id,@Field("use_wallet") int use_wallet );

    @GET("app/payment-methods.php")
    Call<PaymentMethodsResponse> PaymentMethods();

    @FormUrlEncoded
    @POST("app/cash-on-delivery.php")
    Call<CashonDeliveryResponse> CashOnDelivery(@Field("user_id") String user_id, @Field("address_id") String address_id,
                                                @Field("delivery_date") String delivery_date, @Field("delivery_time") String delivery_time,
                                                @Field("used_wallet") Integer used_wallet, @Field("payment_method") String payment_method);
    @FormUrlEncoded
    @POST("app/login-phone-number.php")
    Call<LoginMobileResponse> LoginMobile(@Field("phone_number") String phone_number);

    @FormUrlEncoded
    @POST("app/login-otp.php")
    Call<MobileOtpVerifyResponse> MobileOtpVerify(@Field("otp") String otp,@Field("device_id") String device_id );

    @FormUrlEncoded
    @POST("app/contact-us-form.php")
    Call<ContactFormResponse> ContactuFom(@Field("name") String name, @Field("email") String email, @Field("mobile") String mobile,@Field("subject") String subject,@Field("message") String message);

    @GET("app/customer-call.php")
    Call<CallResponse> Call();

    @FormUrlEncoded
    @POST("app/delivery-address.php")
    Call<DefaultAdressResponse> DefaultAddress(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("app/my-orders-pending.php")
    Call<CancelOrderResponse> CancelOrder(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("app/cancel-order.php")
    Call<SingleCancelOrderResponse> SingleOrderCancel(@Field("user_id") String user_id,@Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("app/new-delivery-address.php")
    Call<NewDeliveryAddressResponse> NewDeliverAddress(@Field("address_id") String address_id, @Field("user_id") String user_id );

    @FormUrlEncoded
    @POST("app/firebase.php")
    Call<FirebaseResponse> FirebaseResponse(@Field("type") String type, @Field("token") String token, @Field("device_id") String device_id);


}
