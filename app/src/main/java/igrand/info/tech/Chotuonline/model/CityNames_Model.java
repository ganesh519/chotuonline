package igrand.info.tech.Chotuonline.model;

public class CityNames_Model {
    private String id;
    private String dprice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDprice() {
        return dprice;
    }

    public void setDprice(String dprice) {
        this.dprice = dprice;
    }
}

