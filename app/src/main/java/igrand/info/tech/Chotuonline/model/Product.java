package igrand.info.tech.Chotuonline.model;

public class Product {

    private int product_image;
    private String product_title;
    private String price;
    private String dprice;
//    private String count;
    public Product(int product_image, String product_title, String price, String dprice) {
        this.product_image = product_image;
        this.product_title = product_title;
        this.price = price;
        this.dprice = dprice;
//        this.count = count;
    }

//    public String getCount() {
//        return count;
//    }
//
//    public void setCount(String count) {
//        this.count = count;
//    }

    public int getProduct_image() {
        return product_image;
    }

    public void setProduct_image(int product_image) {
        this.product_image = product_image;
    }

    public String getProduct_title() {
        return product_title;
    }

    public void setProduct_title(String product_title) {
        this.product_title = product_title;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDprice() {
        return dprice;
    }

    public void setDprice(String dprice) {
        this.dprice = dprice;
    }
}
