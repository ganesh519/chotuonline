package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.OtpResendResponse;
import igrand.info.tech.Chotuonline.model.Signup_otpResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Text extends AppCompatActivity {
    EditText signup_otp;
    String otp, mobile, code;
    ApiInterface apiInterface, apiInterface1;
    Button proceed;
    TextView resend;
    SmsVerifyCatcher smsVerifyCatcher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text);
        signup_otp = findViewById(R.id.signup_otp);
        proceed = findViewById(R.id.proceed);
        resend = findViewById(R.id.resend);
        mobile = getIntent().getStringExtra("phone");

        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                code = parseCode(message);
                signup_otp.setText(code);
            }
        });


        final ProgressDialog progressDialog1 = new ProgressDialog(Text.this);
        progressDialog1.setMessage("wait...");
        progressDialog1.show();
        smsVerifyCatcher.onStart();
        apiInterface1 = ApiClient.getClient().create(ApiInterface.class);
        Call<OtpResendResponse> call1 = apiInterface1.ReSend(mobile);
        call1.enqueue(new Callback<OtpResendResponse>() {
            @Override
            public void onResponse(Call<OtpResendResponse> call, Response<OtpResendResponse> response) {
                if (response.isSuccessful()) ;
                OtpResendResponse otpResendResponse = response.body();
                if (otpResendResponse.status.equals("1")) {
                    progressDialog1.dismiss();
                    Toast.makeText(Text.this, otpResendResponse.message, Toast.LENGTH_SHORT).show();
                } else if (otpResendResponse.status.equals("0")) {
                    progressDialog1.dismiss();
                    Toast.makeText(Text.this, otpResendResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OtpResendResponse> call, Throwable t) {
                progressDialog1.dismiss();
                Toast.makeText(Text.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp = signup_otp.getText().toString();
                final ProgressDialog progressDialog = new ProgressDialog(Text.this);
                progressDialog.setMessage("wait...");
                progressDialog.show();
                smsVerifyCatcher.onStart();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<Signup_otpResponse> call = apiInterface.SignupOtp(otp);
                call.enqueue(new Callback<Signup_otpResponse>() {
                    @Override
                    public void onResponse(Call<Signup_otpResponse> call, Response<Signup_otpResponse> response) {
                        if (response.isSuccessful()) ;
                        Signup_otpResponse signup_otpResponse = response.body();
                        if (signup_otpResponse.status.equals("1")) {
                            progressDialog.dismiss();
                            Toast.makeText(Text.this, signup_otpResponse.message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class).putExtra("signup","login");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                        else if (signup_otpResponse.status.equals("0")) {
                            progressDialog.dismiss();
                            Toast.makeText(Text.this, signup_otpResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Signup_otpResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(Text.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog progressDialog1 = new ProgressDialog(Text.this);
                progressDialog1.setMessage("wait...");
                progressDialog1.show();
                smsVerifyCatcher.onStart();
                apiInterface1 = ApiClient.getClient().create(ApiInterface.class);
                Call<OtpResendResponse> call1 = apiInterface1.ReSend(mobile);
                call1.enqueue(new Callback<OtpResendResponse>() {
                    @Override
                    public void onResponse(Call<OtpResendResponse> call, Response<OtpResendResponse> response) {
                        if (response.isSuccessful()) ;
                        OtpResendResponse otpResendResponse = response.body();
                        if (otpResendResponse.status.equals("1")) {
                            progressDialog1.dismiss();
                            Toast.makeText(Text.this, otpResendResponse.message, Toast.LENGTH_SHORT).show();
                        } else if (otpResendResponse.status.equals("0")) {
                            progressDialog1.dismiss();
                            Toast.makeText(Text.this, otpResendResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<OtpResendResponse> call, Throwable t) {
                        progressDialog1.dismiss();
                        Toast.makeText(Text.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });
    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
