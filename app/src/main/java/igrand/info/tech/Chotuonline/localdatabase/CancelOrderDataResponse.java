package igrand.info.tech.Chotuonline.localdatabase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CancelOrderDataResponse {

    @SerializedName("order_id")
    @Expose
    public String orderId;
    @SerializedName("order_number")
    @Expose
    public String orderNumber;
    @SerializedName("order_datetime")
    @Expose
    public String orderDatetime;
    @SerializedName("payment_method")
    @Expose
    public String paymentMethod;
    @SerializedName("order_status")
    @Expose
    public String orderStatus;
    @SerializedName("total_amount")
    @Expose
    public String totalAmount;
    @SerializedName("no_of_items")
    @Expose
    public Integer noOfItems;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("orderId", orderId).append("orderNumber", orderNumber).append("orderDatetime", orderDatetime).append("paymentMethod", paymentMethod).append("orderStatus", orderStatus).append("totalAmount", totalAmount).append("noOfItems", noOfItems).toString();
    }
}
