package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.AboutDataResponse;

public class AboutUsRecyclerAdapter extends RecyclerView.Adapter<AboutUsRecyclerAdapter.Holder> {
    List<AboutDataResponse> aboutDataResponse;
    Context context;
    private int lastPosition = -1;
    public AboutUsRecyclerAdapter(List<AboutDataResponse> aboutDataResponse, Context ctx) {
        this.aboutDataResponse=aboutDataResponse;
        this.context=ctx;

    }
    @NonNull
    @Override
    public AboutUsRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_about, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AboutUsRecyclerAdapter.Holder holder, int position) {
        setAnimation(holder.itemView,position);
        holder.text_about.setText(aboutDataResponse.get(position).description);
        holder.text_about_name.setText(aboutDataResponse.get(position).name);
    }

    @Override
    public int getItemCount() {
        return aboutDataResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView text_about,text_about_name;
        public Holder(View itemView) {
            super(itemView);

            text_about=itemView.findViewById(R.id.text_about);
            text_about_name=itemView.findViewById(R.id.text_about_name);

        }
    }
    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
