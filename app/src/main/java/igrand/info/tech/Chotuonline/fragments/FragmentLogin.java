package igrand.info.tech.Chotuonline.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Objects;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.CommonClass;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.EditProfileActivity;
import igrand.info.tech.Chotuonline.activity.FacebookEditProfileActivity;
import igrand.info.tech.Chotuonline.activity.ForgotPassword;
import igrand.info.tech.Chotuonline.activity.LoginActivity;
import igrand.info.tech.Chotuonline.activity.Login_Mobile;
import igrand.info.tech.Chotuonline.activity.MainActivity;
import igrand.info.tech.Chotuonline.model.FacbookResponse;
import igrand.info.tech.Chotuonline.model.FacebookDataResponse;
import igrand.info.tech.Chotuonline.model.GoogleLoginDataResponse;
import igrand.info.tech.Chotuonline.model.GoogleLoginResponse;
import igrand.info.tech.Chotuonline.model.LoginDataResponse;
import igrand.info.tech.Chotuonline.model.LoginResponse;
import igrand.info.tech.Chotuonline.util.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentLogin extends Fragment {

    TextView signup, forgot_password, bt_login_login, text_show;
    Context context;
    ApiInterface apiInterface, apiInterface1, apiInterface2;
    ProgressDialog pd;
    EditText edt_email, edt_password;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    String email_id, Pass_word;
    ProgressBar login_progress;
    CheckBox checkbox;
    String facebook_id, first_name, last_name, email;

    public static GoogleSignInClient mGoogleSignInClient;
    public static final int RC_SIGN_IN = 007;
    private String TAG = "error";
    ImageView login_gplus, login_facebook;
    ProgressDialog progressDialog, progressDialog1;
    AccessTokenTracker accessTokenTracker;
    ProfileTracker profileTracker;
    String accessToken;
    LoginButton login_button;
    CallbackManager callbackManager;
    private static final String EMAIL = "email";
    String femail = "";
    private boolean isConnected;
    RelativeLayout relative;
    TextView login_mobile;
    String USER_NAME, USER_ID, USER_MOBILE, USER_EMAIL, OTP, USER_IMAGE, USER_DOB;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        login_mobile = v.findViewById(R.id.login_mobile);
        login_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Login_Mobile.class));
            }
        });
        relative = v.findViewById(R.id.relative);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("please wait...");

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);

        login_gplus = v.findViewById(R.id.login_gplus);
        login_gplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                signIn();
            }
        });
        login_button = v.findViewById(R.id.login_button);
        login_facebook = v.findViewById(R.id.login_facebook);

//        AppEventsLogger.activateApp(getContext());
//        callbackManager = CallbackManager.Factory.create();

        login_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFacebookLogin();
            }
        });

        signup = v.findViewById(R.id.signup);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getContext(),RegisterActivity.class));
                ((LoginActivity) getActivity()).setPage(1);
            }
        });
        edt_email = v.findViewById(R.id.email);
        edt_password = v.findViewById(R.id.password);
        text_show = v.findViewById(R.id.text_show);
        text_show.setVisibility(View.GONE);
        edt_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        edt_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edt_password.getText().length() > 0) {
                    text_show.setVisibility(View.VISIBLE);
                } else {
                    text_show.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        text_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (text_show.getText() == "show") {
                    text_show.setText("hide");
                    edt_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    edt_password.setSelection(edt_password.length());
                } else {
                    text_show.setText("show");
                    edt_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_password.setSelection(edt_password.length());
                }
            }
        });

        forgot_password = v.findViewById(R.id.forgot_password);
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ForgotPassword.class));
            }
        });
        bt_login_login = v.findViewById(R.id.bt_login_login);
        bt_login_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isConnected = ConnectionDetector.isConnected();
                if (isConnected) {
                    getData();
                } else {
                    CommonClass.showSnack(isConnected, relative);
                }
            }
        });
        return v;
    }

    private void getFacebookLogin() {
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        login_button.setReadPermissions(Arrays.asList(EMAIL));
//        login_button.setReadPermissions("user_friends");
        login_button.setReadPermissions("public_profile");
        login_button.setReadPermissions("email");
//        login_button.setReadPermissions("user_birthday");
//
//        // If you are using in a fragment, call facebbokloginButton.setFragment(this);
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", EMAIL));

        // Callback registration
        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {

                AccessToken accessToken=loginResult.getAccessToken();

                GraphRequest request = GraphRequest.newMeRequest(accessToken,new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            facebook_id = object.getString("id");
                            first_name = object.getString("first_name");
                            last_name = object.getString("last_name");
//                                            String mobile_no=object.getString("mobile_number");
                            email = "";
                            if (object.has("email")) {
                                email = object.getString("email");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        final String fname = first_name + " " + last_name;
//
                        SharedPreferences sharedPreference = getContext().getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
                        String device = sharedPreference.getString("mobile_id", "");

                        apiInterface = ApiClient.getClient().create(ApiInterface.class);
                        Call<FacbookResponse> call = apiInterface.FacebookLogin(facebook_id, fname, email, device);
                        call.enqueue(new Callback<FacbookResponse>() {
                            @Override
                            public void onResponse(Call<FacbookResponse> call, Response<FacbookResponse> response) {
                                if (response.isSuccessful()) ;
                                FacbookResponse facbookResponse = response.body();
                                if (facbookResponse.status.equals("1")) {
                                    FacebookDataResponse facebookDataResponse = facbookResponse.data;
                                    String userid = facebookDataResponse.userId;
                                    String firstname = facebookDataResponse.userFirstName;
                                    String lastname = facebookDataResponse.userLastName;
                                    String Email = facebookDataResponse.userEmail;
                                    String mobile = facebookDataResponse.userPhoneNumber;
                                    String aMobile = facebookDataResponse.userAltNumber;
                                    String Date = facebookDataResponse.userDateOfBirth;
                                    String image = facebookDataResponse.userPhoto;

                                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("user_id", userid);
                                    editor.putString("firstname", firstname);
                                    editor.putString("lastname", lastname);
                                    editor.putString("e_mail", Email);
                                    editor.putString("mobile", mobile);
                                    editor.putString("image", image);
                                    editor.putString("date_of_birth", Date);
                                    editor.putString("altermobile", aMobile);
                                    editor.commit();

                                    SharedPreferences sharedPreferences1 = getActivity().getSharedPreferences("Profile", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor1 = sharedPreferences1.edit();
                                    editor1.putString("firstname", firstname);
                                    editor1.putString("lastname", lastname);
                                    editor1.putString("e_mail", Email);
                                    editor1.putString("mobile", mobile);
                                    editor1.putString("image", image);
                                    editor1.putString("date_of_birth", Date);
                                    editor1.putString("altermobile", aMobile);
                                    editor1.commit();

                                    Toast.makeText(getActivity(), facbookResponse.message, Toast.LENGTH_SHORT).show();

                                    int f_type = facebookDataResponse.userFacebookType;
                                    if (f_type == 0) {
                                        startActivity(new Intent(getActivity(), FacebookEditProfileActivity.class));
                                    } else if (f_type == 1) {
                                        startActivity(new Intent(getActivity(), MainActivity.class));
                                    }
                                } else if (facbookResponse.status.equals("0")) {

                                    Toast.makeText(getActivity(), facbookResponse.message, Toast.LENGTH_SHORT).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<FacbookResponse> call, Throwable t) {
                                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender,birthday"); // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(getActivity(), "cancel Login", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast.makeText(getActivity(), "error in Login", Toast.LENGTH_SHORT).show();
            }
        });

//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(final LoginResult loginResult) {
//                        // App code
//
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        // App code
//                        Toast.makeText(getActivity(), "facebook login cancel by user", Toast.LENGTH_LONG).show();
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//
//                        Toast.makeText(getActivity(), exception.toString() + "", Toast.LENGTH_LONG).show();
//                    }
//                });
    }

    private void getData() {
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading....");
        pd.show();

        email_id = edt_email.getText().toString().trim();
        Pass_word = edt_password.getText().toString().trim();

        if (email_id.equals("") || Pass_word.equals("")) {
            pd.dismiss();
            Toast.makeText(getActivity(), "Enter Username and password", Toast.LENGTH_SHORT).show();
        } else if (email_id.equals("")) {
            pd.dismiss();
            Toast.makeText(getActivity(), "Invalid Username", Toast.LENGTH_SHORT).show();
        } else {
            getLogin(email_id, Pass_word);
        }
    }

    private void getLogin(String email, String password) {
        SharedPreferences sharedPreference = getContext().getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
        String device = sharedPreference.getString("mobile_id", "");

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResponse> call = apiInterface.Login(email, password, device);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) ;
                LoginResponse loginResponse = response.body();
                if (loginResponse.status.equals("1")) {
                    LoginDataResponse loginDataResponse = loginResponse.data;
                    String u_id = loginDataResponse.userId;
                    String image = loginDataResponse.userPhoto;
                    String name = loginDataResponse.userFirstName;
                    String mobile = loginDataResponse.userPhoneNumber;
                    String email = loginDataResponse.userEmail;

                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("user_id", u_id);
                    editor.putString("profileimage", image);
                    editor.putString("Name", name);
                    editor.putString("Mobile", mobile);
                    editor.putString("Email", email);
                    editor.commit();

                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                    edt_email.setText("");
                    edt_password.setText("");
                    pd.dismiss();
                }
                else if (loginResponse.status.equals("0")) {
                    Toast.makeText(getActivity(), loginResponse.message, Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void signIn() {

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            callbackManager.onActivityResult(requestCode, resultCode, data);

        } catch (Exception e) {
            e.printStackTrace();

        }
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {

        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Register Activity", "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }

    private void updateUI(GoogleSignInAccount googleSignInAccount) {
        progressDialog.show();
        if (googleSignInAccount != null) {

            SharedPreferences sharedPreference = getActivity().getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
            String device = sharedPreference.getString("mobile_id", "");

            String g_id = googleSignInAccount.getId();
            final String email = googleSignInAccount.getEmail();
            final String fname = googleSignInAccount.getGivenName();

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<GoogleLoginResponse> call = apiInterface.GoogleLogin(g_id, fname, email, device);
            call.enqueue(new Callback<GoogleLoginResponse>() {
                @Override
                public void onResponse(Call<GoogleLoginResponse> call, Response<GoogleLoginResponse> response) {
                    if (response.isSuccessful()) ;

                    GoogleLoginResponse googleLoginResponse = response.body();
                    if (googleLoginResponse.status.equals("1")) {
                        progressDialog.dismiss();

                        GoogleLoginDataResponse googleLoginDataResponse = googleLoginResponse.data;

                        String userid = googleLoginDataResponse.userId;
                        String firstname = googleLoginDataResponse.userFirstName;
                        String lastname = googleLoginDataResponse.userLastName;
                        String Email = googleLoginDataResponse.userEmail;
                        String mobile = googleLoginDataResponse.userPhoneNumber;
                        String aMobile = googleLoginDataResponse.userAltNumber;
                        String Date = googleLoginDataResponse.userDateOfBirth;
                        String image = googleLoginDataResponse.userPhoto;

                        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("user_id", userid);
                        editor.putString("firstname", firstname);
                        editor.putString("lastname", lastname);
                        editor.putString("e_mail", Email);
                        editor.putString("mobile", mobile);
                        editor.putString("image", image);
                        editor.putString("date_of_birth", Date);
                        editor.putString("altermobile", aMobile);
                        editor.commit();

                        SharedPreferences sharedPreferences1 = getActivity().getSharedPreferences("Profile", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = sharedPreferences1.edit();
                        editor1.putString("firstname", firstname);
                        editor1.putString("lastname", lastname);
                        editor1.putString("e_mail", Email);
                        editor1.putString("mobile", mobile);
                        editor1.putString("image", image);
                        editor1.putString("date_of_birth", Date);
                        editor1.putString("altermobile", aMobile);
                        editor1.commit();
                        Toast.makeText(getActivity(), googleLoginResponse.message, Toast.LENGTH_SHORT).show();

                        int g_type = googleLoginDataResponse.userGoogleType;
                        if (g_type == 1) {
                            startActivity(new Intent(getActivity(), MainActivity.class));
                        } else if (g_type == 0) {
                            startActivity(new Intent(getActivity(), FacebookEditProfileActivity.class));
                        }
                    } else if (googleLoginResponse.status.equals("0")) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), googleLoginResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GoogleLoginResponse> call, Throwable t) {
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        } else {
            progressDialog.dismiss();
            Toast.makeText(getActivity(), "please login ", Toast.LENGTH_SHORT).show();
        }
    }
}

