package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class FaqDataResponse {

    @SerializedName("question")
    @Expose
    public String question;
    @SerializedName("answer")
    @Expose
    public String answer;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("question", question).append("answer", answer).toString();
    }


}
