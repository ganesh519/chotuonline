package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class NewDeliveryAddressDataResponse {
    @SerializedName("type")
    @Expose
    public Integer type;
    @SerializedName("addressbook_id")
    @Expose
    public String addressbookId;
    @SerializedName("address_type")
    @Expose
    public String addressType;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("state_id")
    @Expose
    public String stateId;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("city_id")
    @Expose
    public String cityId;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("house")
    @Expose
    public String house;
    @SerializedName("residential")
    @Expose
    public String residential;
    @SerializedName("area_id")
    @Expose
    public String areaId;
    @SerializedName("area")
    @Expose
    public String area;
    @SerializedName("pincode")
    @Expose
    public String pincode;
    @SerializedName("landmark")
    @Expose
    public String landmark;
    @SerializedName("default_address")
    @Expose
    public String defaultAddress;
    @SerializedName("customer_name")
    @Expose
    public String customerName;
    @SerializedName("customer_phone_number")
    @Expose
    public Object customerPhoneNumber;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addressbookId", addressbookId).append("addressType", addressType).append("country", country).append("stateId", stateId).append("state", state).append("cityId", cityId).append("city", city).append("house", house).append("residential", residential).append("areaId", areaId).append("area", area).append("pincode", pincode).append("landmark", landmark).append("defaultAddress", defaultAddress).append("customerName", customerName).append("customerPhoneNumber", customerPhoneNumber).toString();
    }

}
