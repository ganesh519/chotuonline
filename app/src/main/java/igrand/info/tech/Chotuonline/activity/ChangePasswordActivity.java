package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.ChangePasswordResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {
    ImageView back;
    EditText old_psd, new_psd, conform_psd;
    Button bt_changepass_update;
    ApiInterface apiInterface;
    String o_pass, n_pass, c_pass;
    TextView text_show, new_show, conform_show;
    String pattern ="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}";
    String user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        old_psd = findViewById(R.id.old_psd);
        text_show = findViewById(R.id.text_show);
        text_show.setVisibility(View.GONE);
        old_psd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        old_psd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (old_psd.getText().length() > 0) {
                    text_show.setVisibility(View.VISIBLE);
                }
                else {
                    text_show.setVisibility(View.GONE);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }

        });

        text_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (text_show.getText() == "show") {
                    text_show.setText("hide");
                    old_psd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    old_psd.setSelection(old_psd.length());
                }
                else {
                    text_show.setText("show");
                    old_psd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    old_psd.setSelection(old_psd.length());
                }
            }
        });
        new_psd = findViewById(R.id.new_psd);
        new_show = findViewById(R.id.new_show);
        new_show.setVisibility(View.GONE);
        new_psd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        new_psd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (new_psd.getText().length() > 0) {
                    new_show.setVisibility(View.VISIBLE);
                }
                else {
                    new_show.setVisibility(View.GONE);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        new_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new_show.getText() == "show") {
                    new_show.setText("hide");
                    new_psd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    new_psd.setSelection(old_psd.length());
                }
                else {
                    new_show.setText("show");
                    new_psd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    new_psd.setSelection(old_psd.length());
                }
            }
        });
        conform_psd = findViewById(R.id.conform_psd);
        conform_show = findViewById(R.id.conform_show);
        conform_show.setVisibility(View.GONE);
        conform_psd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        conform_psd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (conform_psd.getText().length() > 0) {
                    conform_show.setVisibility(View.VISIBLE);
                }
                else {
                    conform_show.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        conform_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (conform_show.getText() == "show") {
                    conform_show.setText("hide");
                    conform_psd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    conform_psd.setSelection(old_psd.length());
                }
                else {
                    conform_show.setText("show");
                    conform_psd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    conform_psd.setSelection(old_psd.length());
                }
            }
        });
        bt_changepass_update = findViewById(R.id.bt_changepass_update);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreferences.getString("user_id", "");

        bt_changepass_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPassword();
            }
        });

    }
    public void getPassword() {

        o_pass = old_psd.getText().toString();
        n_pass = new_psd.getText().toString();
        c_pass = conform_psd.getText().toString();

        if (o_pass.equals("")) {
            Toast.makeText(this, "Enter Old Password", Toast.LENGTH_SHORT).show();
        }

        else if (n_pass.equals("")) {
            Toast.makeText(this, "Enter New Password", Toast.LENGTH_SHORT).show();
        }
        else if (!n_pass.matches(pattern)){
            Toast.makeText(getApplicationContext(), "password length must be 6-12, One upper case, One lower case and one Special Character", Toast.LENGTH_LONG).show();
        }
        else if (c_pass.equals("")) {
            Toast.makeText(this, "Enter Conform Password", Toast.LENGTH_SHORT).show();
        }
        else if (!c_pass.matches(pattern)){
            Toast.makeText(getApplicationContext(), "password length must be 6-12, One upper letter, One lower letter and one Special Character", Toast.LENGTH_LONG).show();
        }
        else {
            final ProgressDialog progressDialog = new ProgressDialog(ChangePasswordActivity.this);
            progressDialog.setMessage("wait...");
            progressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ChangePasswordResponse> call = apiInterface.ChangePassword(user_id, o_pass, n_pass, c_pass);
            call.enqueue(new Callback<ChangePasswordResponse>() {
                @Override
                public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                    if (response.isSuccessful()) ;
                    ChangePasswordResponse changePasswordResponse = response.body();
                    if (changePasswordResponse.status.equals("1")) {
                        progressDialog.dismiss();
                        Toast.makeText(ChangePasswordActivity.this, changePasswordResponse.message, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ChangePasswordActivity.this, MyAccountActivity.class));
                        finish();
                    }
                    else if (changePasswordResponse.status.equals("0")) {
                        progressDialog.dismiss();
                        Toast.makeText(ChangePasswordActivity.this, changePasswordResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(ChangePasswordActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
