package igrand.info.tech.Chotuonline.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.CommonClass;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.Text;
import igrand.info.tech.Chotuonline.activity.LoginActivity;
import igrand.info.tech.Chotuonline.model.SignupResponse;
import igrand.info.tech.Chotuonline.util.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentSignup extends Fragment {

    TextView login, signup, password_show, cpass_show;
    Context context;
    EditText et_fname, et_lname, et_email, et_mobile, et_password, et_cpassword;
    String fname, lname, mobile, email, password, con_password;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String MobilePattern = "[0-9]{10}";
    String pattern ="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}";
    ApiInterface apiInterface, apiInterface_otp;
    LinearLayout linearLayout;
    String otp;
    BroadcastReceiver receiver;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private boolean isConnected;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_signup, container, false);
        linearLayout = v.findViewById(R.id.linear);
        login = v.findViewById(R.id.login);
        et_fname = v.findViewById(R.id.et_fname);
        et_lname = v.findViewById(R.id.et_lname);
        et_email = v.findViewById(R.id.et_email);
        et_mobile = v.findViewById(R.id.et_mobile);
        et_password = v.findViewById(R.id.et_password);
        password_show = v.findViewById(R.id.password_show);
        password_show.setVisibility(View.GONE);

        et_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        et_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (et_password.getText().length() > 0) {
                    password_show.setVisibility(View.VISIBLE);
                } else {
                    password_show.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        password_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password_show.getText() == "show") {
                    password_show.setText("hide");
                    et_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    et_password.setSelection(et_password.length());
                }
                else {
                    password_show.setText("show");
                    et_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    et_password.setSelection(et_password.length());

                }
            }
        });
        et_cpassword = v.findViewById(R.id.et_cpassword);
        cpass_show = v.findViewById(R.id.cpass_show);
        cpass_show.setVisibility(View.GONE);

        et_cpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        et_cpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (et_cpassword.getText().length() > 0) {
                    cpass_show.setVisibility(View.VISIBLE);
                }
                else {
                    cpass_show.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        cpass_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cpass_show.getText() == "show") {
                    cpass_show.setText("hide");
                    et_cpassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    et_cpassword.setSelection(et_cpassword.length());
                }
                else {
                    cpass_show.setText("show");
                    et_cpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    et_cpassword.setSelection(et_cpassword.length());

                }
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LoginActivity) getActivity()).setPage(0);
            }
        });

        signup = v.findViewById(R.id.signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isConnected = ConnectionDetector.isConnected();
                if (isConnected) {
                    getData();
                }
                else {
                    CommonClass.showSnack(isConnected, linearLayout);
                }
            }
        });
        return v;
    }

    private void getData() {

        fname = et_fname.getText().toString();
        lname = et_lname.getText().toString();
        mobile = et_mobile.getText().toString();
        email = et_email.getText().toString();
        password = et_password.getText().toString();
        con_password = et_cpassword.getText().toString();

        if (fname.equals("")|| mobile.equals("") || email.equals("") || password.equals("") || con_password.equals("")) {

            Toast.makeText(getActivity(), "Please enter required details", Toast.LENGTH_SHORT).show();
        }
        else if (!email.matches(emailPattern)) {

            Toast.makeText(getActivity(), "Invalid Username", Toast.LENGTH_SHORT).show();
        }
        else if (!password.matches(pattern)){
            Toast.makeText(context, "password length must be 6-12, One upper case, One lower case and one Special Character", Toast.LENGTH_LONG).show();
        }
        else if (!con_password.matches(pattern)){
            Toast.makeText(context, "password length must be 6-12, One upper case, One lower case and one Special Character", Toast.LENGTH_LONG).show();
        }
        else if (!password.equals(con_password)) {
            Toast.makeText(getActivity(), "confirm Password is not matched", Toast.LENGTH_SHORT).show();
        }
        else if(!mobile.matches(MobilePattern)) {
            Toast.makeText(context, "Please enter valid 10 digit mobile number", Toast.LENGTH_SHORT).show();
        }
        else {

            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("mobile", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("phone", mobile);
            editor.commit();
            login();

        }

    }
    private void login() {

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SignupResponse> call = apiInterface.Signup(fname, lname, email, mobile, password);
        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(final Call<SignupResponse> call, Response<SignupResponse> response) {
                if (response.isSuccessful()) ;
                final SignupResponse signupResponse = response.body();
                if (signupResponse.status.equals("1")) {
                    progressDialog.dismiss();
                    getActivity().startActivity(new Intent(getActivity(), Text.class).putExtra("phone", mobile));
                } else if (signupResponse.status.equals("0")) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), signupResponse.message, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}