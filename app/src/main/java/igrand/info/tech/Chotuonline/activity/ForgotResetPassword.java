package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.ForgotpassResetResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotResetPassword extends AppCompatActivity {
    ImageView back;
    EditText new_psd, conform_psd;
    TextView text_show, new_show, conform_show;
    Button submit;
    String newpass, conformpass;
    ApiInterface apiInterface;
    String pattern ="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resetpassword);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        new_psd = findViewById(R.id.new_psd);
        new_show = findViewById(R.id.new_show);
        new_show.setVisibility(View.GONE);
        new_psd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        new_psd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (new_psd.getText().length() > 0) {
                    new_show.setVisibility(View.VISIBLE);
                }
                else {
                    new_show.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        new_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new_show.getText() == "show") {
                    new_show.setText("hide");
                    new_psd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    new_psd.setSelection(new_psd.length());
                }
                else {
                    new_show.setText("show");
                    new_psd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    new_psd.setSelection(new_psd.length());
                }
            }
        });
        conform_psd = findViewById(R.id.conform_psd);
        conform_show = findViewById(R.id.conform_show);
        conform_show.setVisibility(View.GONE);
        conform_psd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        conform_psd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (conform_psd.getText().length() > 0) {
                    conform_show.setVisibility(View.VISIBLE);
                }
                else {
                    conform_show.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        conform_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (conform_show.getText() == "show") {
                    conform_show.setText("hide");
                    conform_psd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    conform_psd.setSelection(conform_psd.length());
                }
                else {
                    conform_show.setText("show");
                    conform_psd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    conform_psd.setSelection(conform_psd.length());
                }
            }
        });

        submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getResetPassword();
            }
        });

    }

    public void getResetPassword() {

        newpass = new_psd.getText().toString();
        conformpass = conform_psd.getText().toString();

        if (newpass.equals("")) {
            Toast.makeText(this, "Enter your new Password", Toast.LENGTH_SHORT).show();
        }
        else if (!newpass.matches(pattern)){
            Toast.makeText(getApplicationContext(), "password length must be 6-12, One upper case, One lower case and one Special Character", Toast.LENGTH_LONG).show();
        }
        else if (conformpass.equals("")) {
            Toast.makeText(this, "Conform Your Password", Toast.LENGTH_SHORT).show();
        }
        else if (!conformpass.matches(pattern)){
            Toast.makeText(getApplicationContext(), "password length must be 6-12, One upper case, One lower case and one Special Character", Toast.LENGTH_LONG).show();
        }
        else {

            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("Forgot", Context.MODE_PRIVATE);
            String user_id = sharedPreferences.getString("Fuser_id", "");

            final ProgressDialog progressDialog = new ProgressDialog(ForgotResetPassword.this);
            progressDialog.setMessage("wait...");
            progressDialog.show();

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ForgotpassResetResponse> call = apiInterface.ForgotpassReset(newpass, conformpass, user_id);
            call.enqueue(new Callback<ForgotpassResetResponse>() {
                @Override
                public void onResponse(Call<ForgotpassResetResponse> call, Response<ForgotpassResetResponse> response) {
                    if (response.isSuccessful()) ;
                    ForgotpassResetResponse forgotpassResetResponse = response.body();
                    if (forgotpassResetResponse.status.equals("1")) {
                        progressDialog.dismiss();
                        Toast.makeText(ForgotResetPassword.this, forgotpassResetResponse.message, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ForgotResetPassword.this, LoginActivity.class).putExtra("signup","login"));
                        finish();
                    }
                    else if (forgotpassResetResponse.status.equals("0")) {
                        progressDialog.dismiss();
                        Toast.makeText(ForgotResetPassword.this, forgotpassResetResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ForgotpassResetResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(ForgotResetPassword.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}
