package igrand.info.tech.Chotuonline.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.OffersRecyclerAdapter;
import igrand.info.tech.Chotuonline.model.OffersDataResponse;
import igrand.info.tech.Chotuonline.model.OffersResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Fragmentoffers extends Fragment{
    RecyclerView recycler_offers;
    ApiInterface apiInterface;
    Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_offers, container, false);
        recycler_offers=v.findViewById(R.id.recycler_offers);
        final ProgressDialog progressDialog1 = new ProgressDialog(getActivity());
        progressDialog1.setMessage("wait...");
        progressDialog1.show();
        apiInterface=ApiClient.getClient().create(ApiInterface.class);
        Call<OffersResponse> call=apiInterface.Offers();
        call.enqueue(new Callback<OffersResponse>() {
            @Override public void onResponse(Call<OffersResponse> call, Response<OffersResponse> response) {
                if (response.isSuccessful());
                OffersResponse offersResponse=response.body();
                if (offersResponse.status.equals("1")){
                    progressDialog1.dismiss();
                    List<OffersDataResponse> offersDataResponse = offersResponse.data;
                    for (int i=0;i<offersDataResponse.size();i++){
                        recycler_offers.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        recycler_offers.setAdapter(new OffersRecyclerAdapter(offersDataResponse, context));
                        recycler_offers.setHasFixedSize(true);
                        recycler_offers.setNestedScrollingEnabled(false);
                    }
                }
                else if (offersResponse.status.equals("0")){
                    progressDialog1.dismiss();
                    Toast.makeText(getActivity(), offersResponse.message, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<OffersResponse> call, Throwable t) {
                progressDialog1.dismiss();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }
}
