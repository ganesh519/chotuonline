package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.FaqDataResponse;

public class FaqRecyclerAdapter extends RecyclerView.Adapter<FaqRecyclerAdapter.Holder> {
    List<FaqDataResponse> faqDataResponse;
    Context context;
    private int lastPosition = -1;

    public FaqRecyclerAdapter(List<FaqDataResponse> faqDataResponse, Context context) {
        this.faqDataResponse = faqDataResponse;
        this.context = context;

    }

    @NonNull
    @Override
    public FaqRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_faq, parent, false);
            return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FaqRecyclerAdapter.Holder holder, int position) {
        setAnimation(holder.itemView,position);
        holder.textView3.setText(faqDataResponse.get(position).question);
        holder.textView.setText(faqDataResponse.get(position).answer);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.textView.setVisibility(holder.textView.isShown() ? View.GONE : View.VISIBLE);
                if (holder.textView.isShown()) {
                    holder.add.setVisibility(View.GONE);
                    holder.remove.setVisibility(View.VISIBLE);
                }
                else {
                    holder.add.setVisibility(View.VISIBLE);
                    holder.remove.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return faqDataResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout, linearLayout1;
        TextView textView, textView1, textView2, textView3;
        ImageView add, remove;

        public Holder(View view) {
            super(view);
            linearLayout = (LinearLayout) view.findViewById(R.id.linearfaq);
            textView3 = view.findViewById(R.id.textfaq);
            textView = (TextView) view.findViewById(R.id.textexpandfaq);
            textView.setVisibility(View.GONE);
            add = view.findViewById(R.id.add);
            remove = view.findViewById(R.id.remove);
        }
    }

    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
