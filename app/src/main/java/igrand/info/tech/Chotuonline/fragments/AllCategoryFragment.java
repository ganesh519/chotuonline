package igrand.info.tech.Chotuonline.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.OnClickExpand;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.MainActivity;
import igrand.info.tech.Chotuonline.activity.SearchActivity;
import igrand.info.tech.Chotuonline.model.AllSubCategoryResponse;
import igrand.info.tech.Chotuonline.model.AllSubSubCategoryDataResponse;
import igrand.info.tech.Chotuonline.model.All_CategoryDataResponse;
import igrand.info.tech.Chotuonline.model.All_CategoryResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AllCategoryFragment extends Fragment implements OnClickExpand {
    ExpandableListView expandableListView;
    ApiInterface apiInterface;
    TextView et_search_input;
    android.widget.ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    LinkedHashMap<String, List<AllSubCategoryResponse>> expandableListDetail;
    ProgressDialog progressDialog;
    Context context;
    FragmentTransaction fragmentTransaction;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.allcategoryfragment, null);
        expandableListView = view.findViewById(R.id.categorylist);
        et_search_input=view.findViewById(R.id.et_search_input);
        MainActivity.user_iv.setVisibility(View.GONE);
        MainActivity.logo.setVisibility(View.GONE);
        MainActivity.category_text.setVisibility(View.VISIBLE);
        MainActivity.category_text.setText("All Category");
        et_search_input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().startActivity(new Intent(getActivity(), SearchActivity.class));
            }
        });
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<All_CategoryResponse> categoryResponseCall=apiInterface.AllCategory();
        categoryResponseCall.enqueue(new Callback<All_CategoryResponse>() {
            @Override
            public void onResponse(Call<All_CategoryResponse> call, Response<All_CategoryResponse> response) {
                if (response.isSuccessful()) ;
                All_CategoryResponse all_categoryResponse = response.body();
                if (all_categoryResponse.status.equals("1")) {
                    progressDialog.dismiss();
                    final List<All_CategoryDataResponse> all_categoryDataResponse = all_categoryResponse.data;
                    expandableListDetail = new LinkedHashMap<>();
                    for (int i = 0; i < all_categoryDataResponse.size(); i++) {
                        List<AllSubCategoryResponse> Fruits = null;
                        Fruits = new ArrayList<AllSubCategoryResponse>();
                        for (int j = 0; j < all_categoryDataResponse.get(i).subcategoryinfo.size(); j++) {
                            Fruits.add(all_categoryDataResponse.get(i).subcategoryinfo.get(j));
                        }
                        expandableListDetail.put(all_categoryDataResponse.get(i).categoryName, Fruits);
                    }
                    expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
                    expandableListAdapter = new ExpandableListAdapterCategory(context, expandableListTitle, expandableListDetail,AllCategoryFragment.this);
                    expandableListView.setAdapter(expandableListAdapter);
                    expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                        @Override
                        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                            String cat_id = all_categoryDataResponse.get(groupPosition).subcategoryinfo.get(childPosition).categoryId;
                            String sub_id=all_categoryDataResponse.get(groupPosition).subcategoryinfo.get(childPosition).subcategoryId;
                            String cat_name =all_categoryDataResponse.get(groupPosition).subcategoryinfo.get(childPosition).subcategoryName;

                            Bundle bundle=new Bundle();
                            bundle.putString("category_id", cat_id);
                            bundle.putString("subcategory_id", sub_id);
                            bundle.putString("category_name", cat_name);
                            bundle.putString("menuList","data");
                            bundle.putString("all_category","Allcategory");

                            CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
                             fragmentTransaction =getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.fl_main_framelayout, categoryMainListFragment,"");
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();
                            categoryMainListFragment.setArguments(bundle);
                            return false;
                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<All_CategoryResponse> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

    @Override
    public void subItemSelected(AllSubCategoryResponse all_categoryDataResponse) {

        String cat_id = all_categoryDataResponse.categoryId;
        String sub_id=all_categoryDataResponse.subcategoryId;
        String cat_name =all_categoryDataResponse.subcategoryName;

        Bundle bundle=new Bundle();
        bundle.putString("category_id", cat_id);
        bundle.putString("subcategory_id", sub_id);
        bundle.putString("category_name", cat_name);
        bundle.putString("menuList","data");
        bundle.putString("all_category","Allcategory");

        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
        fragmentTransaction =getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, categoryMainListFragment,"");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        categoryMainListFragment.setArguments(bundle);
    }

    @Override
    public void subsubItemSelected(AllSubCategoryResponse all_categoryDataResponse,AllSubSubCategoryDataResponse rowItem) {


        String cat_id = all_categoryDataResponse.categoryId;
        String sub_id=all_categoryDataResponse.subcategoryId;
        String cat_name =all_categoryDataResponse.subcategoryName;
        String sub_sub_id =rowItem.subsubcategoryId;

        Bundle bundle=new Bundle();
        bundle.putString("category_id", cat_id);
        bundle.putString("subcategory_id", sub_id);
        bundle.putString("sub_sub_id", sub_sub_id);
        bundle.putString("category_name", cat_name);
        bundle.putString("all_category","");
        bundle.putString("menuList","data");
        bundle.putString("SubSubCategory","subsubcategory");

        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
        fragmentTransaction =getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, categoryMainListFragment,"");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        categoryMainListFragment.setArguments(bundle);
    }
}
