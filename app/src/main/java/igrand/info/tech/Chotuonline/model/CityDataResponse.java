package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CityDataResponse {

    @SerializedName("city_id")
    @Expose
    public String cityId;
    @SerializedName("city_name")
    @Expose
    public String cityName;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cityId", cityId).append("cityName", cityName).toString();
    }

}
