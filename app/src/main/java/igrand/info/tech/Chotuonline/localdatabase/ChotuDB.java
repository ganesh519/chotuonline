package igrand.info.tech.Chotuonline.localdatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import igrand.info.tech.Chotuonline.model.UserDetailsLogin;

/**
 * Created by prasannapulagam on 2/22/18.
 */

public class ChotuDB extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "ChotuOnline";
    // Temples table name
    private static final String CHECHBAC_RESULT_TABLE = "ChotuOnlineTable";
    private static final String CHECKBAC_ACTIVE_TABLE = "ChotuTable";
    private static final String VALIDATION_ID = "validation_id";
    private static final String QUANTITY = "quantity";
    private static final String KEY_ID = "id";

    private static final String USER_TABLE = "userTable";
    private static final String USER_ID = "userId";
    private static final String USER_NAME = "userName";
    private static final String USER_EMAIL = "userEmail";
    private static final String USER_MOBILE = "userMobile";
    private static final String USER_IMAGE = "userImage";
    private static final String USER_DOB = "userDob";


    private static String USER_TABLE_STRING = "CREATE TABLE " + USER_TABLE + " ( "
            + USER_ID + " TEXT PRIMARY KEY," + USER_NAME + " TEXT," + USER_EMAIL + " TEXT,"
            + " TEXT," + USER_MOBILE + " TEXT," + USER_IMAGE + " TEXT," + USER_DOB + " TEXT)";

    public ChotuDB
            (Context applicationContext) {
        super(applicationContext, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Create Table for BACtrack Result
        String CREATE_CHECHBAC_RESULT_TABLE = "CREATE TABLE " + CHECHBAC_RESULT_TABLE + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + VALIDATION_ID + " TEXT,"
                + QUANTITY + " TEXT" + ")";

        db.execSQL(CREATE_CHECHBAC_RESULT_TABLE);
        db.execSQL(USER_TABLE_STRING);
        /*//create CheckBAC Active table
        String CREATE_CHECHBAC_ACTIVE_TABLE = "CREATE TABLE " + CHECKBAC_ACTIVE_TABLE + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"

                + QUANTITY + " TEXT" + ")";
        db.execSQL(CREATE_CHECHBAC_ACTIVE_TABLE);*/



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CHECHBAC_RESULT_TABLE);
        db.execSQL(getDropQuery(USER_TABLE));
        onCreate(db);
       // db.execSQL("DROP TABLE IF EXISTS " + CHECKBAC_ACTIVE_TABLE);
    }
    private String getDropQuery(String tableName) {
        return "DROP TABLE " + tableName;
    }

    //add the CheckBAC Active list
    public void addChecBACResultList(BACRecord bacResultRecord) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, bacResultRecord.getIndexID());
        values.put(VALIDATION_ID, bacResultRecord.getVariationId());
        values.put(QUANTITY, bacResultRecord.getQuantity());


        //Inserting Row
        long result = database.insert(CHECHBAC_RESULT_TABLE, null, values);
        //close the local database
        database.close();

    }

    //get the list
    public List<BACRecord> getAllBACResultList() {
        List<BACRecord> bacRecordList = new ArrayList<BACRecord>();
        try {
            //Select Query
            String query = "SELECT * FROM " + CHECHBAC_RESULT_TABLE;
            SQLiteDatabase database = this.getWritableDatabase();
            Cursor cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    BACRecord bacResultRecord = new BACRecord();
                    bacResultRecord.setIndexID(cursor.getString(0));
                    bacResultRecord.setVariationId(cursor.getString(1));
                    bacResultRecord.setQuantity(cursor.getString(2));
                    bacRecordList.add(bacResultRecord);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bacRecordList;
    }

    //remove the local database list
    public void removeBACResultList( ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//        values.put(KEY_ID, bacResultRecord.getIndexID());
//        values.put(VALIDATION_ID, bacResultRecord.getVariationId());
//        values.put(QUANTITY, bacResultRecord.getQuantity());
        db.execSQL("DELETE FROM " + CHECHBAC_RESULT_TABLE);
        //close the local database
        db.close();
    }

    public int updateRecord(BACRecord bacResultRecord) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, bacResultRecord.getIndexID());
        values.put(VALIDATION_ID, bacResultRecord.getVariationId());
        values.put(QUANTITY, bacResultRecord.getQuantity());
        return db.update(CHECHBAC_RESULT_TABLE, values, KEY_ID + "=?", new String[]{String.valueOf(bacResultRecord.getIndexID())});
    }
    public void insertUserInfo(String userId, String userName, String email, String dob, String image, String mobileNum) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(USER_ID, userId);
        values.put(USER_NAME, userName);
        values.put(USER_EMAIL, email);
        values.put(USER_DOB, dob);
        values.put(USER_IMAGE, image);
        values.put(USER_MOBILE, mobileNum);

        db.insert(USER_TABLE, null, values);

        db.close();
    }

    public void updateUserInfo(String userId, String userName, String email, String dob, String image, String mobileNum) {

        String countQuery = "SELECT  * FROM " + USER_TABLE + " WHERE " + USER_ID + " = " + userId;
        SQLiteDatabase readDb = this.getReadableDatabase();
        Cursor cursor = readDb.rawQuery(countQuery, null);

        if (cursor.getCount() != 0) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(USER_NAME, userName);
            values.put(USER_EMAIL, email);
            values.put(USER_DOB, dob);
            values.put(USER_IMAGE, image);
            values.put(USER_MOBILE, mobileNum);

            // updating row
            db.update(USER_TABLE, values, USER_ID + " = ?",
                    new String[]{String.valueOf(userId)});
        } else {
            insertUserInfo(userId, userName, email, dob, image, mobileNum);
        }
        cursor.close();
    }


    public UserDetailsLogin getUserInfoFromDb() {

        String countQuery = "SELECT  * FROM " + USER_TABLE;
        SQLiteDatabase readDb = this.getReadableDatabase();
        Cursor cursor1 = readDb.rawQuery(countQuery, null);


        int count = 0;
        if (cursor1.getCount() != 0) {
            count = cursor1.getCount();
        }

        cursor1.close();

        UserDetailsLogin userDetails = null;

        if (count > 0) {
            SQLiteDatabase db = this.getReadableDatabase();

            String selectQuery = "SELECT * FROM " + USER_TABLE;
            Cursor cursor = db.rawQuery(selectQuery, null);

            userDetails = new UserDetailsLogin();

            if (cursor.moveToFirst()) {
                do {
                    userDetails.setUserId(cursor.getString(0));
                    userDetails.setUserName(cursor.getString(1));
                    userDetails.setEmail(cursor.getString(2));
                    userDetails.setDateOfBirth(cursor.getString(3));
                    userDetails.setPhonenumber(cursor.getString(4));
                    userDetails.setImageUrl(cursor.getString(5));
                    userDetails.setDateOfBirth(cursor.getString(6));
                } while (cursor.moveToNext());
            }
            cursor.close();
            db.close();
        }
        return userDetails;
    }

}


