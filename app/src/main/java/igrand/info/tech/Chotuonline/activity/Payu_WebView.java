package igrand.info.tech.Chotuonline.activity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import igrand.info.tech.Chotuonline.R;

public class Payu_WebView extends AppCompatActivity{
    WebView webView;
    ProgressBar progressBar;
    private TextView textView;
    ImageView back;
    JavaScriptInterface JSInterface;

    SwipeRefreshLayout swipeRefreshLayout;
    @SuppressLint("SetJavaScriptEnabled")
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payu_webview);
        back=findViewById(R.id.back);
        back.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAppExitDialog();
            }
        });
        swipeRefreshLayout=findViewById(R.id.swipe);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
//                                      progressBar.setVisibility(View.VISIBLE);
                                    }
                                }
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadweb();
//                swipeRefreshLayout.setRefreshing(true);
            }
        });
        loadweb();
    }
    public void loadweb(){
        webView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = webView.getSettings();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.clearCache(true);
        webView.clearHistory();

        JSInterface = new JavaScriptInterface(this);
        webView.addJavascriptInterface(JSInterface, "Android");

        String url=getIntent().getStringExtra("cash_key");
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient1());
    }

    @Override
    public void onBackPressed() {
        showAppExitDialog();
//        if (webView.canGoBack()){
//            webView.goBack();
//
//        }
//        else {
//            showAppExitDialog();
//        }

    }

    private class WebViewClient1 extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);


            if (!isNetworkavailable()){
                webView.clearCache(true);
                webView.clearHistory();
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.setEnabled(false);
        }

        public void onReceivedError(WebView webView, int errorCode, String description, String failingUrl) {
            try {
                webView.stopLoading();
            } catch (Exception e) {
            }

            if (webView.canGoBack()) {
                webView.goBack();
            }

            final AlertDialog alertDialog = new AlertDialog.Builder(Payu_WebView.this).create();
            alertDialog.setTitle("No Internet connection");
            alertDialog.setMessage("Check your internet connection and try again.");
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Try Again", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.cancel();
                    loadweb();
                }
            });


            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);

                }
            });

            alertDialog.show();
            super.onReceivedError(webView, errorCode, description, failingUrl);
        }
    }

    protected void showAppExitDialog(){

        AlertDialog.Builder builder = new AlertDialog.Builder(Payu_WebView.this);
        builder.setTitle("Please confirm");
        builder.setMessage("Do You want to close this Payment?");
        builder.setCancelable(true);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Payu_WebView.super.onBackPressed();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private boolean isNetworkavailable(){
        ConnectivityManager connectivityManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }
    protected void noconnection(){
        if (!isNetworkavailable()){
            final AlertDialog alertDialog = new AlertDialog.Builder(Payu_WebView.this).create();
            alertDialog.setTitle("No Internet connection");
            alertDialog.setMessage("Check your internet connection and try again.");
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Try Again", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.cancel();
                    loadweb();
                }
            });

            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);

                }
            });

            alertDialog.show();
        }
    }

    public class JavaScriptInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        JavaScriptInterface(Context c) {
            mContext = c;
        }
        @JavascriptInterface
        public void showToast(String toast){
            Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
            if (toast.equals("success")){
                final AlertDialog.Builder builder = new AlertDialog.Builder(Payu_WebView.this);
                View view = getLayoutInflater().inflate(R.layout.alert_success, null);
                builder.setView(view);
                Button button = view.findViewById(R.id.ok);
                AlertDialog b = builder.create();
                b.show();
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(Payu_WebView.this, MainActivity.class));
                        finish();
                    }
                });
            }
            else {
                final AlertDialog.Builder builder = new AlertDialog.Builder(Payu_WebView.this);
                View view = getLayoutInflater().inflate(R.layout.alert_failure, null);
                builder.setView(view);
                Button button = view.findViewById(R.id.ok);
                AlertDialog b = builder.create();
                b.show();
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(Payu_WebView.this, PaymentActivity.class));
                        finish();
                    }
                });

            }

        }
    }

}
