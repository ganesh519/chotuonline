package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.CancelOrderAdapter;
import igrand.info.tech.Chotuonline.localdatabase.CancelOrderDataResponse;
import igrand.info.tech.Chotuonline.model.CancelOrderResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Cancel_Order extends AppCompatActivity {
    RecyclerView recycler_cancel;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ImageView back;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancel_order);
        init();

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void init(){
        recycler_cancel = findViewById(R.id.recycler_cancel);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences.getString("user_id", "");

        final ProgressDialog progressDialog = new ProgressDialog(Cancel_Order.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CancelOrderResponse> cancelOrderResponseCall=apiInterface.CancelOrder(user_id);
        cancelOrderResponseCall.enqueue(new Callback<CancelOrderResponse>() {
            @Override
            public void onResponse(Call<CancelOrderResponse> call, Response<CancelOrderResponse> response) {
                if (response.isSuccessful());
                CancelOrderResponse cancelOrderResponse=response.body();
                if (cancelOrderResponse.status.equals("1")){
                    progressDialog.dismiss();
                    List<CancelOrderDataResponse> cancelOrderDataResponse=cancelOrderResponse.data;
                    if (cancelOrderDataResponse != null){
                        recycler_cancel.setAdapter(new CancelOrderAdapter(cancelOrderDataResponse, getApplicationContext()));
                        recycler_cancel.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                        recycler_cancel.setHasFixedSize(true);
                        recycler_cancel.setNestedScrollingEnabled(false);
                    }

                }
                else if (cancelOrderResponse.status.equals("0")){
                    progressDialog.dismiss();
                    Toast.makeText(Cancel_Order.this, cancelOrderResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CancelOrderResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Cancel_Order.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
