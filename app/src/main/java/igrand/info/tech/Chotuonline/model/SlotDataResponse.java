package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class SlotDataResponse {


    @SerializedName("day")
    @Expose
    public String day;
    @SerializedName("slots")
    @Expose
    public List<SlotDataDetailsResponse> slots = null;
    @SerializedName("dddd")
    @Expose
    public boolean first;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("day", day).append("slots", slots).toString();
    }

}
