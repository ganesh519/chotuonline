package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class JSignupResponse {



    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("user_details")
    @Expose
    public JSignupDataResponse userDetails;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("message", message).append("userDetails", userDetails).toString();
    }

}
