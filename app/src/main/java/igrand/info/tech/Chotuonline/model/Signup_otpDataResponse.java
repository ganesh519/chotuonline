package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Signup_otpDataResponse {

    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("user_email")
    @Expose
    public String userEmail;
    @SerializedName("user_name")
    @Expose
    public String userName;
    @SerializedName("user_type")
    @Expose
    public String userType;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("userEmail", userEmail).append("userName", userName).append("userType", userType).toString();
    }
}
