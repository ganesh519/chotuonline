package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AddCartResponse {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("no_of_items")
    @Expose
    public Integer noOfItems;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("message", message).append("noOfItems", noOfItems).toString();
    }

}
