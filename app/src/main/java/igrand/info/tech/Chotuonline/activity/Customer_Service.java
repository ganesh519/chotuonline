package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

//import com.zopim.android.sdk.api.ZopimChat;
//import com.zopim.android.sdk.model.VisitorInfo;
//import com.zopim.android.sdk.prechat.ZopimChatActivity;

import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.model.VisitorInfo;
import com.zopim.android.sdk.prechat.ZopimChatActivity;

import java.util.ArrayList;
import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.CallAdapter;
import igrand.info.tech.Chotuonline.model.CallModel;
import igrand.info.tech.Chotuonline.model.CallDataResponse;
import igrand.info.tech.Chotuonline.model.CallResponse;
import igrand.info.tech.Chotuonline.model.LoginDataResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Customer_Service extends AppCompatActivity {
    LinearLayout linear,faqs,callus;
    ImageView back;
    CardView cancel_order;
    LoginDataResponse loginResponse;
    ListView alert_listview;
    CallAdapter callAdapter;
    ApiInterface apiInterface;
    List<CallModel> callModel;
    String selected="";
    List<CallDataResponse> callDataResponses;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customerservice);
        cancel_order=findViewById(R.id.cancel_order);
        linear=findViewById(R.id.askus);
        back = findViewById(R.id.back);
        faqs = findViewById(R.id.faqs);
        callus = findViewById(R.id.callus);

        linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String name = sharedPreferences.getString("Name", "");
                String email=sharedPreferences.getString("Email","");
                String mobile=sharedPreferences.getString("Mobile","");
                if (name == null){
                    name="";
                }
                if (email == null){
                    email="";
                }
                if (mobile == null){
                    mobile="";
                }
                ZopimChat.init("Oi0xuiG9QEyRTBvO2cFwGwDIV6UIFm8g");
                VisitorInfo visitorInfo=new VisitorInfo.Builder().name(name).email(email).phoneNumber(mobile).build();
                ZopimChat.setVisitorInfo(visitorInfo);
                startActivity(new Intent(Customer_Service.this,ZopimChatActivity.class));
            }
        });
        faqs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Customer_Service.this,FAQsActivity.class));

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Customer_Service.this,Cancel_Order.class));
            }
        });


        final ProgressDialog progressDialog=new ProgressDialog(Customer_Service.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface=ApiClient.getClient().create(ApiInterface.class);
        Call<CallResponse> callResponseCall=apiInterface.Call();
        callResponseCall.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if (response.isSuccessful());
                CallResponse callResponse=response.body();
                if (callResponse.status.equals("1")){
                    progressDialog.dismiss();
                    callDataResponses=callResponse.data;
                    callModel=new ArrayList<>();
                    for (int i=0;i<callDataResponses.size();i++){
                        CallModel Model=new CallModel();
                        Model.setId(callDataResponses.get(i).id);
                        Model.setName(callDataResponses.get(i).number);
                        callModel.add(Model);
                    }
                }
                else if (callResponse.status.equals("0")){
                    progressDialog.dismiss();
                    Toast.makeText(Customer_Service.this, callResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Customer_Service.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        callus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(Customer_Service.this);
                View view = LayoutInflater.from(Customer_Service.this).inflate(R.layout.alert_call, null);
                alert_listview = view.findViewById(R.id.alert_listview);

                callAdapter=new CallAdapter(Customer_Service.this,android.R.layout.simple_list_item_1,callModel,selected);
                alert_listview.setAdapter(callAdapter);
                builder.setView(view);
                final AlertDialog b = builder.create();
                b.show();

                alert_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        view.setSelected(true);
                        selected=callModel.get(position).getName();
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", callDataResponses.get(position).number, null));
                        startActivity(intent);
                        b.dismiss();
                    }
                });

            }
        });
    }
}
