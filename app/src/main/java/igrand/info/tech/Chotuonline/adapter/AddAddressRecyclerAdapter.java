package igrand.info.tech.Chotuonline.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.Addressbook;
import igrand.info.tech.Chotuonline.activity.CheckOutActivity;
import igrand.info.tech.Chotuonline.activity.EditAddressActivity;
import igrand.info.tech.Chotuonline.model.AddressBookDataResoponse;
import igrand.info.tech.Chotuonline.model.DeleteAddressResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddressRecyclerAdapter extends RecyclerView.Adapter<AddAddressRecyclerAdapter.Holder> {
    Context context;
    List<AddressBookDataResoponse> addressBookDataResoponse;
    ApiInterface apiInterface, apiInterface1;
    int type;
    String check;
    Activity activity;
    public static String Address;

    public AddAddressRecyclerAdapter(Context context, List<AddressBookDataResoponse> addressBookDataResoponse, String check) {
        this.context = context;
        this.addressBookDataResoponse = addressBookDataResoponse;
        this.check = check;
    }

    @NonNull
    @Override
    public AddAddressRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_ddress, parent, false);

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AddAddressRecyclerAdapter.Holder holder, final int position) {

        holder.address.setText(addressBookDataResoponse.get(position).house + "," + addressBookDataResoponse.get(position).residential + "," +
                addressBookDataResoponse.get(position).landmark + "," + addressBookDataResoponse.get(position).area + "," + addressBookDataResoponse.get(position).city
                + "," + addressBookDataResoponse.get(position).state + "-" + addressBookDataResoponse.get(position).pincode + ".");
        holder.c_name.setText(addressBookDataResoponse.get(position).customerName);
        holder.mobile.setText(addressBookDataResoponse.get(position).customerPhoneNumber);
        if (addressBookDataResoponse.get(position).defaultAddress.equals("1")) {
            holder.home.setText(addressBookDataResoponse.get(position).addressType + " (Default)");
            holder.home.setTextColor(Color.parseColor("#018bc8"));
            holder.address.setTextColor(Color.parseColor("#018bc8"));
            holder.c_name.setText(addressBookDataResoponse.get(position).customerName);
            holder.c_name.setTextColor(Color.parseColor("#018bc8"));
            holder.mobile.setText(addressBookDataResoponse.get(position).customerPhoneNumber);
            holder.mobile.setTextColor(Color.parseColor("#018bc8"));

            String address_id=addressBookDataResoponse.get(position).addressbookId;

            Address=address_id;
        }
        else {
            holder.home.setText(addressBookDataResoponse.get(position).addressType);
        }

        holder.address_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address_id = addressBookDataResoponse.get(position).addressbookId;
                String stateid = addressBookDataResoponse.get(position).stateId;
                String cityid = addressBookDataResoponse.get(position).cityId;
                String areaid = addressBookDataResoponse.get(position).areaId;
                String house = addressBookDataResoponse.get(position).house;
                String resedent = addressBookDataResoponse.get(position).residential;
                String land = addressBookDataResoponse.get(position).landmark;
                String addressid = addressBookDataResoponse.get(position).addressType;
                String type = addressBookDataResoponse.get(position).defaultAddress;
                SharedPreferences sharedPreferences = context.getSharedPreferences("addressbook_id", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("AddressBook_id", address_id);
                editor.putString("state_id", stateid);
                editor.putString("city_id", cityid);
                editor.putString("area_id", areaid);
                editor.putString("houneno", house);
                editor.putString("resdential", resedent);
                editor.putString("landmark", land);
                editor.putString("addressType", addressid);
                editor.putString("defaultadd", type);

                editor.commit();
                Intent intent = new Intent(context, EditAddressActivity.class);
                intent.putExtra("key",check);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.startActivity(intent);
            }
        });


        holder.address_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");
                String addres_id = addressBookDataResoponse.get(position).addressbookId;

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<DeleteAddressResponse> call = apiInterface.AddressDelete(user_id, addres_id);
                call.enqueue(new Callback<DeleteAddressResponse>() {
                    @Override
                    public void onResponse(Call<DeleteAddressResponse> call, Response<DeleteAddressResponse> response) {
                        if (response.isSuccessful()) ;
                        DeleteAddressResponse deleteAddressResponse = response.body();
                        if (deleteAddressResponse.status.equals("1")) {
                            Toast.makeText(context, deleteAddressResponse.message, Toast.LENGTH_SHORT).show();

                            if (check.equalsIgnoreCase("checkout")) {
                                Intent intent = new Intent(context, CheckOutActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(intent);
                            }
                            else {

                                Intent intent = new Intent(context, Addressbook.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(intent);
                            }
                        }
                        else if (deleteAddressResponse.status.equals("0")) {
                            Toast.makeText(context, deleteAddressResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteAddressResponse> call, Throwable t) {
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressBookDataResoponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView home, address, c_name, mobile;
        ImageView address_edit;
        ImageView address_delete;
        LinearLayout linear;

        public Holder(View itemView) {
            super(itemView);
            home = itemView.findViewById(R.id.home);
            address = itemView.findViewById(R.id.address);
//            address_edit = itemView.findViewById(R.id.address_edit);
            address_delete = itemView.findViewById(R.id.address_delete);
            linear = itemView.findViewById(R.id.linear);
            mobile = itemView.findViewById(R.id.mobile);
            c_name = itemView.findViewById(R.id.c_name);
        }
    }
}
