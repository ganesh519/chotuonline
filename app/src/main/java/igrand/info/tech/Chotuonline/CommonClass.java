package igrand.info.tech.Chotuonline;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class CommonClass {


    public static ArrayList<String> cartarray=new ArrayList<String>();

    public static void showSnack(boolean isConnected,View view) {
        String message;
        int color;
        if (isConnected)
        {
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        }
        else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }





}
