package igrand.info.tech.Chotuonline.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.CartActivity;
import igrand.info.tech.Chotuonline.model.OrderDetailsDataResponse;
import igrand.info.tech.Chotuonline.model.OrderDetailsResponse;
import igrand.info.tech.Chotuonline.model.ReorderResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentSummary extends Fragment {

    ApiInterface apiInterface;
    TextView dateTime, orderstatus, address, orderno, invoiceno, paymentoption, order_items, sub_total, deliver_charges, total, Date, mobile,
            name;
    String date, status, addres, order_no, invoice_no, payment_option, orderitems, subtotal, deliverycharges, totall, date_slot, phone, Name;
    NestedScrollView summery_nestedscroll;
    TextView bt_reorder;
    String user_id;
    String Order_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_summary, container, false);

        Date = v.findViewById(R.id.Date);
        dateTime = v.findViewById(R.id.dateTime);
        orderstatus = v.findViewById(R.id.orderstatus);
        address = v.findViewById(R.id.address);
        orderno = v.findViewById(R.id.orderno);
        invoiceno = v.findViewById(R.id.invoiceno);
        paymentoption = v.findViewById(R.id.paymentoption);
        sub_total = v.findViewById(R.id.sub_total);
        deliver_charges = v.findViewById(R.id.deliver_charges);
        total = v.findViewById(R.id.total);
        order_items = v.findViewById(R.id.order_items);
        mobile = v.findViewById(R.id.mobile);
        name = v.findViewById(R.id.name);
        summery_nestedscroll = v.findViewById(R.id.summery_nestedscroll);
        bt_reorder=v.findViewById(R.id.bt_reorder);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreferences.getString("user_id", "");

        SharedPreferences sharedPreferences1 = getActivity().getSharedPreferences("OrderId", Context.MODE_PRIVATE);
        Order_id = sharedPreferences1.getString("order_id", "");

        SharedPreferences sharedPreference =getActivity().getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
        final String device = sharedPreference.getString("mobile_id","");

        bt_reorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiInterface=ApiClient.getClient().create(ApiInterface.class);
                Call<ReorderResponse> call1=apiInterface.Reorder(user_id,Order_id,device);
                call1.enqueue(new Callback<ReorderResponse>() {
                    @Override
                    public void onResponse(Call<ReorderResponse> call, Response<ReorderResponse> response) {
                        if (response.isSuccessful());
                        ReorderResponse reorderResponse=response.body();
                        if (reorderResponse.status.equals("1")){
                            Toast.makeText(getContext(), reorderResponse.message, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getContext(), CartActivity.class));
                        }
                        else if (reorderResponse.status.equals("0")){
                            Toast.makeText(getContext(), reorderResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ReorderResponse> call, Throwable t) {
                        Toast.makeText(getContext(),t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("wait...");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        final Call<OrderDetailsResponse> call = apiInterface.OrderDetails(user_id, Order_id);
        call.enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                if (response.isSuccessful()) ;
                OrderDetailsResponse orderDetailsResponse = response.body();
                if (orderDetailsResponse.status == 1) {
                    summery_nestedscroll.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    OrderDetailsDataResponse orderDetailsDataResponse = orderDetailsResponse.data;
                    date_slot = orderDetailsDataResponse.deliveryDate;
                    date = orderDetailsDataResponse.deliveryTime;
                    status = orderDetailsDataResponse.orderStatus;
                    addres = orderDetailsDataResponse.deliveryAddress;
                    order_no = orderDetailsDataResponse.orderNo;
                    invoice_no = orderDetailsDataResponse.invoiceNo;
                    payment_option = orderDetailsDataResponse.paymentOption;
                    orderitems = String.valueOf(orderDetailsDataResponse.orderItems + "items");
                    subtotal = orderDetailsDataResponse.subTotal;
                    deliverycharges = orderDetailsDataResponse.deliveryCharges;
                    totall = orderDetailsDataResponse.total;
                    phone = orderDetailsDataResponse.phoneNumber;
                    Name = orderDetailsDataResponse.customerName;

                    dateTime.setText(date);
                    orderstatus.setText(status);
                    address.setText(addres);
                    orderno.setText(order_no);
                    invoiceno.setText(invoice_no);
                    paymentoption.setText(payment_option);
                    sub_total.setText(subtotal);
                    deliver_charges.setText(deliverycharges);
                    total.setText(totall);
                    order_items.setText(orderitems);
                    Date.setText(date_slot);
                    mobile.setText(phone);
                    name.setText(Name);
                }
                else if (orderDetailsResponse.status == 0) {
                    summery_nestedscroll.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), orderDetailsResponse.message, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                summery_nestedscroll.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }
}
