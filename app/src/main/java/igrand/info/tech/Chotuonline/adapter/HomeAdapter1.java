package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.DownSliderResponse;
import igrand.info.tech.Chotuonline.model.HomeDataResponse;
import me.relex.circleindicator.CircleIndicator;

public class HomeAdapter1 extends RecyclerView.Adapter<HomeAdapter1.Holder> {
    Context context;
    private HomeDataResponse homeDataResponse;
    List<DownSliderResponse> downSliderResponse;

    public HomeAdapter1(Context context, HomeDataResponse homeDataResponse) {
        this.context = context;
        this.homeDataResponse = homeDataResponse;

    }

    @NonNull
    @Override
    public HomeAdapter1.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        if (viewType == ViewHandler.slider_view) {
            view = inflater.inflate(R.layout.home_slider, parent, false);
        } else {
            view = inflater.inflate(R.layout.home_slider, parent, false);
        }
        Holder holder = new Holder(view, viewType);
        return holder;

    }

    @Override
    public void onBindViewHolder(@NonNull HomeAdapter1.Holder holder, int position) {


        if (getItemViewType(position) == ViewHandler.slider_view) {
            List<DownSliderResponse> downSliderResponse = homeDataResponse.downSlider;
            SliderAdapter1 pagerAdapter = new SliderAdapter1(downSliderResponse, context);
            holder.viewPager.setAdapter(pagerAdapter);
            holder.circle_page_indicator.setViewPager(holder.viewPager);
        }

    }

    @Override
    public int getItemCount() {
        return ViewHandler.total_view_count;

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ViewHandler.slider_view;
        }
        return super.getItemViewType(position);
    }

    class Holder extends RecyclerView.ViewHolder {

        AutoScrollViewPager viewPager;
        CircleIndicator circle_page_indicator;

        public Holder(View itemView, int viewType) {

            super(itemView);

            if (viewType == ViewHandler.slider_view) {
                viewPager = itemView.findViewById(R.id.pager);
                circle_page_indicator = itemView.findViewById(R.id.indicator);
                viewPager.startAutoScroll(1000);
                viewPager.setInterval(2500);
                viewPager.setDirection(AutoScrollViewPager.RIGHT);

            }

        }

    }

    public interface ViewHandler {

        int total_view_count = 6;
        int slider_view = 0;
        int popular_product = 1;
        int popular_service = 2;
        int featured_product = 3;
        int featured_sevice = 4;
        int all_category = 5;
    }

    public interface OnClickListener {
        void onAdapterClick(View view, int position, int viewType);
    }

}
