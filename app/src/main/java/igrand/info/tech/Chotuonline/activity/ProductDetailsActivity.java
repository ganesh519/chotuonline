package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.CustomSpinnerAdapter1;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.VeiewAdapter;
import igrand.info.tech.Chotuonline.model.AddCartResponse;
import igrand.info.tech.Chotuonline.model.CartCountDataResponse;
import igrand.info.tech.Chotuonline.model.CartCountResponse;
import igrand.info.tech.Chotuonline.model.Category_spinner;
import igrand.info.tech.Chotuonline.model.HomeDataResponse;
import igrand.info.tech.Chotuonline.model.HomeProductimageResponse;
import igrand.info.tech.Chotuonline.model.ProductVariationDetailsDataResponse;
import igrand.info.tech.Chotuonline.model.ProductVariationDetailsResponse;
import igrand.info.tech.Chotuonline.model.ProductVariationResponse;
import igrand.info.tech.Chotuonline.model.Product_DetailsDataResponse;
import igrand.info.tech.Chotuonline.model.Product_DetailsResponse;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class

ProductDetailsActivity extends AppCompatActivity {

    TextView text;
    ImageView back, cart;
    ViewPager viewPager;
    private static int currentPage = 0;
    TextView show_value;
    ImageView decreas, increas;
    int counter = 1;
    LinearLayout linear_cart;
    RelativeLayout linear_add, Relative_increase;
    ProgressBar progressbar_add, progressbar_add_increase;
    ProductVariationDetailsResponse productVariationDetailsResponse;

    ApiInterface apiInterface;
    String company_name, product_name, description, sav, m_price, d_price, p_off, quanti, price1, imag1, imag2, imag3, imag4, Title;
    TextView company, productname, descr, save, margin_price, discount_price, per_off, quantity, price, title;
    private HomeDataResponse homeDataResponse;
    Context context;
    NestedScrollView product_scroll;
    RelativeLayout cart_relative;
    TextView cart_no;
    List<ProductVariationResponse> productVariationResponse;
    int count;

    String variationid, startlimit, endlimit;
    List<Category_spinner> category_spinner;
    TextView alert_text;
    RelativeLayout alert_relative;
    CustomSpinnerAdapter1 customSpinnerAdapter;
    ListView alert_listview;
    CircleIndicator indicator;
    List<HomeProductimageResponse> productimageResponse;
    List<ProductVariationDetailsDataResponse> productVariationDetailsDataResponse;
    public static String device;
    LinearLayout linear_product;
    public static int i;
    String variation_id;

    //DotsIndicator DotsIndicator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productdetails_activity);
        CartCount();

        alert_text = findViewById(R.id.alert_text);
        alert_relative = findViewById(R.id.alert_relative);

        cart_relative = findViewById(R.id.cart_relative);
        cart_no = findViewById(R.id.cart_no);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        linear_product=findViewById(R.id.linear_product);
        progressbar_add_increase = findViewById(R.id.progressbar_add_increase);
        progressbar_add = findViewById(R.id.progressbar_add);
        Relative_increase = findViewById(R.id.Relative_increase);
        title = findViewById(R.id.title);
        company = findViewById(R.id.company);
        productname = findViewById(R.id.productname);
        descr = findViewById(R.id.descr);
        save = findViewById(R.id.save);
        margin_price = findViewById(R.id.margin_price);
//        margin_price.setPaintFlags(margin_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        discount_price = findViewById(R.id.discount_price);
        per_off = findViewById(R.id.per_off);
        cart = findViewById(R.id.cart);

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProductDetailsActivity.this, CartActivity.class));
            }
        });

        show_value = findViewById(R.id.show_value);
        decreas = findViewById(R.id.decreas);
        increas = findViewById(R.id.increas);
        linear_add = findViewById(R.id.linear_add);
        linear_cart = findViewById(R.id.linear_cart);
        viewPager = (ViewPager) findViewById(R.id.pager);
//        product_scroll = findViewById(R.id.product_scroll);

        SharedPreferences sharedPreference1 =getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
        device = sharedPreference1.getString("mobile_id","");

        linear_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressbar_add.setVisibility(View.VISIBLE);

                String value = "1";
                String operator = "plus";

                SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences1.getString("user_id", "");

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddCartResponse> call = apiInterface.AddCart(user_id, variationid, value, startlimit, endlimit, operator,device);
                call.enqueue(new Callback<AddCartResponse>() {
                    @Override
                    public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                        if (response.isSuccessful()) ;
                        AddCartResponse addCartResponse = response.body();
                        if (addCartResponse.status.equals("1")) {
                            Integer i_count = addCartResponse.noOfItems;
                            show_value.setText(Integer.toString(i_count));
//                            popularProductResponse.get(position).isAdded=true;
                            linear_add.setVisibility(View.GONE);
                            progressbar_add.setVisibility(View.GONE);
                            Relative_increase.setVisibility(View.VISIBLE);
                            CartCount();
                            Toast.makeText(ProductDetailsActivity.this, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        } else if (addCartResponse.status.equals("0")) {
                            progressbar_add.setVisibility(View.GONE);
                            Toast.makeText(ProductDetailsActivity.this, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddCartResponse> call, Throwable t) {
                        progressbar_add.setVisibility(View.GONE);
                        Toast.makeText(ProductDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                linear_cart.setVisibility(View.VISIBLE);
            }
        });

        decreas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");
                count = (Integer.parseInt(show_value.getText().toString().trim()));
                progressbar_add_increase.setVisibility(View.VISIBLE);
                String value = "1";
                String operator = "minus";
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
//                SharedPreferences sharedPreference =getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
//                String device = sharedPreference.getString("mobile_id","");
                Call<AddCartResponse> cartResponseCall = apiInterface.AddCart(user_id, variationid, value, startlimit, endlimit, operator,device);
                cartResponseCall.enqueue(new Callback<AddCartResponse>() {
                    @Override
                    public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                        if (response.isSuccessful()) ;
                        AddCartResponse addCartResponse = response.body();
                        if (addCartResponse.status.equals("1")) {
                            Integer count = addCartResponse.noOfItems;
                            show_value.setText(Integer.toString(count));

                            progressbar_add_increase.setVisibility(View.GONE);
                            if (count < 1) {
                                linear_add.setVisibility(View.VISIBLE);
                                linear_cart.setVisibility(View.GONE);
                                CartCount();
                            }
//                            Toast.makeText(ProductDetailsActivity.this, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        } else if (addCartResponse.status.equals("0")) {
                            progressbar_add_increase.setVisibility(View.GONE);
                            Toast.makeText(ProductDetailsActivity.this, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddCartResponse> call, Throwable t) {
                        progressbar_add_increase.setVisibility(View.GONE);
                        Toast.makeText(ProductDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        increas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressbar_add_increase.setVisibility(View.VISIBLE);
                String value = "1";
                String operator = "plus";
                SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences1.getString("user_id", "");

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddCartResponse> cartResponseCall = apiInterface.AddCart(user_id, variationid, value, startlimit, endlimit, operator,device);
                cartResponseCall.enqueue(new Callback<AddCartResponse>() {
                    @Override
                    public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                        if (response.isSuccessful()) ;
                        AddCartResponse addCartResponse = response.body();
                        if (addCartResponse.status.equals("1")) {
                            progressbar_add_increase.setVisibility(View.GONE);
                            Integer i_count = addCartResponse.noOfItems;
                            show_value.setText(Integer.toString(i_count));
                            CartCount();
//                            Toast.makeText(ProductDetailsActivity.this, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        } else if (addCartResponse.status.equals("0")) {
                            progressbar_add_increase.setVisibility(View.GONE);
                            Toast.makeText(ProductDetailsActivity.this, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<AddCartResponse> call, Throwable t) {
                        progressbar_add_increase.setVisibility(View.GONE);
                        Toast.makeText(ProductDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SharedPreferences sharedPreferences = ProductDetailsActivity.this.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences.getString("user_id", "");

        SharedPreferences sharedPreference = ProductDetailsActivity.this.getSharedPreferences("Variation", Context.MODE_PRIVATE);
        variation_id = sharedPreference.getString("v_id", "");

        String Variation = getIntent().getStringExtra("variation");
        String vvv = getIntent().getStringExtra("v_id");

        if (Variation.equals("1234567")) {

            final ProgressDialog progressDialog = new ProgressDialog(ProductDetailsActivity.this);
            progressDialog.setMessage("wait...");
            progressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ProductVariationDetailsResponse> call = apiInterface.ProductVariationDetails(vvv, user_id,device);
            call.enqueue(new Callback<ProductVariationDetailsResponse>() {
                @Override
                public void onResponse(Call<ProductVariationDetailsResponse> call, Response<ProductVariationDetailsResponse> response) {
                    if (response.isSuccessful()) ;
                    productVariationDetailsResponse = response.body();
                    if (productVariationDetailsResponse.status.equals("1")) {
                        linear_product.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        productVariationDetailsDataResponse = productVariationDetailsResponse.data;

                        for (int i = 0; i < productVariationDetailsDataResponse.size(); i++) {
                            company_name = productVariationDetailsDataResponse.get(i).brand;
                            product_name = productVariationDetailsDataResponse.get(i).productName;
                            description = productVariationDetailsDataResponse.get(i).description;

                            String ctn=productVariationDetailsDataResponse.get(i).noOfItems;

                            if (ctn.equals("0")){
                                linear_add.setVisibility(View.VISIBLE);
                                Relative_increase.setVisibility(View.GONE);
                            }
                            else {
                                linear_add.setVisibility(View.GONE);
                                Relative_increase.setVisibility(View.VISIBLE);
                                show_value.setText(String.valueOf(ctn));
                            }

                            company.setText(company_name);
                            productname.setText(product_name);

                            String Decr = Html.fromHtml(description).toString();

                            descr.setText(Decr);
                            title.setText(product_name);

                            if (productVariationDetailsDataResponse != null) {
                                category_spinner = new ArrayList<>();
                                sav = " ₹" + String.valueOf(productVariationDetailsDataResponse.get(i).youSaved);
                                m_price = "₹ " + productVariationDetailsDataResponse.get(i).actualPrice;
                                d_price = "₹ " + productVariationDetailsDataResponse.get(i).unitPrice;
                                p_off = String.valueOf(productVariationDetailsDataResponse.get(i).discount) + "%" + "\n" + "OFF";
                                quanti = productVariationDetailsDataResponse.get(i).quantity;
                                price1 = "₹ " + productVariationDetailsDataResponse.get(i).unitPrice;


                                variationid = productVariationDetailsDataResponse.get(i).productVariationId;
                                startlimit = productVariationDetailsDataResponse.get(i).startLimit;
                                endlimit = productVariationDetailsDataResponse.get(i).endLimit;

                                save.setText(sav);
                                margin_price.setText(m_price);
                                discount_price.setText(d_price);
                                per_off.setText(p_off);
                                alert_text.setText(productVariationDetailsDataResponse.get(i).quantity+"₹"+productVariationDetailsDataResponse.get(i).unitPrice);

                                final List<HomeProductimageResponse> homeProductimageResponses=productVariationDetailsDataResponse.get(i).variationImage;

                                SingleImages(homeProductimageResponses,product_name);


                                final int finalI = i;
                                alert_relative.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        final AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this);
                                        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.alert_productlisting, null);
                                        alert_listview = view.findViewById(R.id.alert_listview);
                                        customSpinnerAdapter = new CustomSpinnerAdapter1(ProductDetailsActivity.this, android.R.layout.simple_list_item_1, category_spinner, alert_text.getText().toString());

                                        TextView productname = view.findViewById(R.id.productname);
                                        productname.setText(productVariationDetailsDataResponse.get(finalI).productName);

                                        alert_listview.setAdapter(customSpinnerAdapter);
                                        builder.setView(view);
                                        final AlertDialog b = builder.create();
                                        b.show();
                                        category_spinner.clear();

                                        productVariationDetailsDataResponse = productVariationDetailsResponse.data;
                                        for (int j = 0; j < productVariationDetailsDataResponse.size(); j++) {
                                            final Category_spinner area = new Category_spinner();
                                            area.setImage1("₹" + String.valueOf(productVariationDetailsDataResponse.get(j).youSaved));
                                            area.setId(productVariationDetailsDataResponse.get(j).productVariationId);
                                            area.setQuantiti(productVariationDetailsDataResponse.get(j).quantity);
                                            area.setPricee("" + "₹" + productVariationDetailsDataResponse.get(j).unitPrice);
                                            area.setDprice("₹ " + productVariationDetailsDataResponse.get(j).actualPrice);
                                            area.setOff(Float.valueOf(productVariationDetailsDataResponse.get(j).discount) + "%" + "\n" + "OFF");
                                            area.setStartlimit(productVariationDetailsDataResponse.get(j).startLimit);
                                            area.setEndlimit(productVariationDetailsDataResponse.get(j).endLimit);
                                            area.setImage2(productVariationDetailsDataResponse.get(j).productName);
                                            category_spinner.add(area);
                                            alert_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int j, long id) {
                                                    view.setSelected(true);
                                                    save.setText(category_spinner.get(j).getImage1());
                                                    margin_price.setText(category_spinner.get(j).getDprice());
//                                                    margin_price.setPaintFlags(margin_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                                    discount_price.setText(category_spinner.get(j).getPricee());
                                                    per_off.setText(category_spinner.get(j).getOff());
                                                    SingleImages(productVariationDetailsDataResponse.get(j).variationImage,category_spinner.get(j).getImage2());
                                                    alert_text.setText(category_spinner.get(j).getQuantiti()+category_spinner.get(j).getPricee());
                                                    variationid = category_spinner.get(j).getId();
                                                    startlimit = category_spinner.get(j).getStartlimit();
                                                    endlimit = category_spinner.get(j).getEndlimit();

                                                    String cnt=productVariationDetailsDataResponse.get(j).noOfItems;
                                                    if (cnt.equals("0"))
                                                    {
                                                        linear_add.setVisibility(View.VISIBLE);
                                                        Relative_increase.setVisibility(View.GONE);

                                                    }
                                                    else {
                                                        linear_add.setVisibility(View.GONE);
                                                        Relative_increase.setVisibility(View.VISIBLE);
                                                        show_value.setText(cnt);
                                                    }

                                                    b.dismiss();
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    }
                    else if (productVariationDetailsResponse.status.equals("0")) {
                        linear_product.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        Toast.makeText(ProductDetailsActivity.this, productVariationDetailsResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ProductVariationDetailsResponse> call, Throwable t) {
                    linear_product.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    Toast.makeText(ProductDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        if (Variation.equalsIgnoreCase("1234")) {

            ProductDetails(variation_id, user_id,device);

        }
        if (Variation.equalsIgnoreCase("5678")) {

            String Yousave = getIntent().getStringExtra("sav");
            String UnitPrice = getIntent().getStringExtra("UnitPrice");
            String disc = getIntent().getStringExtra("disc");
            String Qunti = getIntent().getStringExtra("Qunti");
            variationid= getIntent().getStringExtra("variation_id");
            startlimit = getIntent().getStringExtra("StartLimit");
            endlimit = getIntent().getStringExtra("endliMIT");
            String img1 = getIntent().getStringExtra("img1");
            String actualPrice = getIntent().getStringExtra("actualPrice");
            final List<HomeProductimageResponse> homeProductimageResponse= (List<HomeProductimageResponse>) getIntent().getSerializableExtra("imgage");
            String NoOfitems=getIntent().getStringExtra("Noofitems");

            if (NoOfitems.equals("0")){
                linear_add.setVisibility(View.VISIBLE);
                Relative_increase.setVisibility(View.GONE);
            }
            else {
                linear_add.setVisibility(View.GONE);
                Relative_increase.setVisibility(View.VISIBLE);
                show_value.setText(String.valueOf(NoOfitems));
            }


            sav = " ₹" + String.valueOf(Yousave);
            m_price = "₹ " + actualPrice;
            d_price = "₹ " + UnitPrice;
            p_off = String.valueOf(disc) + "%" + "\n" + "OFF";
            price1 = "₹ " + UnitPrice;

            SharedPreferences sharedPreferences1 = ProductDetailsActivity.this.getSharedPreferences("product", Context.MODE_PRIVATE);
            String product_id = sharedPreferences1.getString("p_id", "");

            SharedPreferences sharedPreferenc = ProductDetailsActivity.this.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
            String user_id1 = sharedPreferenc.getString("user_id", "");

            final ProgressDialog progressDialog = new ProgressDialog(ProductDetailsActivity.this);
            progressDialog.setMessage("wait...");
            progressDialog.show();

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Product_DetailsResponse> call = apiInterface.ProductDetails(product_id, user_id1,device);

            call.enqueue(new Callback<Product_DetailsResponse>() {
                @Override
                public void onResponse(Call<Product_DetailsResponse> call, Response<Product_DetailsResponse> response) {
                    if (response.isSuccessful()) ;
                    Product_DetailsResponse product_detailsResponse = response.body();
                    if (product_detailsResponse.status == 1) {
                        linear_product.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        final Product_DetailsDataResponse product_detailsDataResponse = product_detailsResponse.data;
                        company_name = product_detailsDataResponse.brand;
                        product_name = product_detailsDataResponse.productName;
                        description = product_detailsDataResponse.description;

                        String Decr = Html.fromHtml(description).toString();


                        company.setText(company_name);
                        productname.setText(product_name);
                        descr.setText(Decr);
                        title.setText(product_name);
                        productVariationResponse = product_detailsDataResponse.productVariation;

                        if (productVariationResponse != null) {
//                            List<ProductimageResponse> productimageResponse=productVariationResponse.get(0).productImages;
                            category_spinner = new ArrayList<>();
                            alert_text.setText(productVariationResponse.get(0).quantity+"₹"+productVariationResponse.get(0).unitPrice);
                            save.setText(sav);
                            margin_price.setText(m_price);
                            discount_price.setText(d_price);
                            per_off.setText(p_off);

                            SingleImages(homeProductimageResponse,product_name);

                            alert_relative.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this);
                                    View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.alert_productlisting, null);
                                    alert_listview = view.findViewById(R.id.alert_listview);
                                    customSpinnerAdapter = new CustomSpinnerAdapter1(ProductDetailsActivity.this, android.R.layout.simple_list_item_1, category_spinner, alert_text.getText().toString());

                                    TextView productname = view.findViewById(R.id.productname);
                                    productname.setText(product_detailsDataResponse.productName);

                                    alert_listview.setAdapter(customSpinnerAdapter);
                                    builder.setView(view);
                                    final AlertDialog b = builder.create();
                                    b.show();
                                    category_spinner.clear();

                                    productVariationResponse = product_detailsDataResponse.productVariation;
                                    for (int j = 0; j < productVariationResponse.size(); j++) {
//                                        product_detailsDataResponse.productName = productVariationDetailsDataResponse.get(j).productName;
                                       productimageResponse=productVariationResponse.get(j).productImages;
                                        final Category_spinner area = new Category_spinner();
                                        area.setSave(" ₹" +String.valueOf(productVariationResponse.get(j).youSaved));
                                        area.setId(productVariationResponse.get(j).variationId);
                                        area.setQuantiti(productVariationResponse.get(j).quantity);
                                        area.setPricee("" + "₹" + productVariationResponse.get(j).unitPrice);
                                        area.setDprice("₹ " + productVariationResponse.get(j).actualPrice);
                                        area.setOff(String.valueOf(productVariationResponse.get(j).discount) + "%" + "\n" + "OFF");

//                                        for (int k=0;k<productimageResponse.size();k++){
//                                            area.setImage(productimageResponse.get(k).image1);
//
//                                        }

//                                        area.setImage1(productVariationResponse.get(j).image2);
//                                        area.setImage2(productVariationResponse.get(j).image3);
//                                        area.setImage3(productVariationResponse.get(j).image4);
                                        area.setStartlimit(productVariationResponse.get(j).startLimit);
                                        area.setEndlimit(productVariationResponse.get(j).endLimit);
//                                        area.setPname(productVariationResponse.get(j).productname);
                                        category_spinner.add(area);
                                    }

                                    alert_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                                            view.setSelected(true);
                                            margin_price.setText(category_spinner.get(i).getDprice());
//                                            margin_price.setPaintFlags(margin_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                            discount_price.setText(category_spinner.get(i).getPricee());
                                            per_off.setText(category_spinner.get(i).getOff());

                                            SingleImages(productVariationResponse.get(i).productImages,category_spinner.get(i).getImage2());
                                            alert_text.setText(category_spinner.get(i).getQuantiti()+category_spinner.get(i).getPricee());
                                            variationid = category_spinner.get(i).getId();
                                            startlimit = category_spinner.get(i).getStartlimit();
                                            endlimit = category_spinner.get(i).getEndlimit();
                                            save.setText(category_spinner.get(i).getSave());

                                            String count=productVariationResponse.get(i).noOfItems;

                                            if (count.equals("0")){

                                                linear_add.setVisibility(View.VISIBLE);
                                                Relative_increase.setVisibility(View.GONE);
                                            }
                                            else {
                                                linear_add.setVisibility(View.GONE);
                                                Relative_increase.setVisibility(View.VISIBLE);
                                                show_value.setText(count);
                                            }

                                            b.dismiss();
                                        }
                                    });
                                }
                            });
                        }
                    }
                    else if (product_detailsResponse.status == 0) {
                        linear_product.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        Toast.makeText(ProductDetailsActivity.this, product_detailsResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<Product_DetailsResponse> call, Throwable t) {
                    linear_product.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    Toast.makeText(ProductDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    private void CartCount() {

        SharedPreferences sharedPreference = getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreference.getString("user_id", "");

        SharedPreferences sharedPreferenc =getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
        String device = sharedPreferenc.getString("mobile_id","");


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CartCountResponse> cartCountResponseCall = apiInterface.CartCount(user_id,device);
        cartCountResponseCall.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()) ;
                CartCountResponse cartCountResponse = response.body();
                if (cartCountResponse.status.equals("1")) {
                    CartCountDataResponse cartCountDataResponse = cartCountResponse.data;
                    int count_no = cartCountDataResponse.count;

                    cart_no.setText(String.valueOf(count_no));
                    if (count_no == 0) {
                        cart_relative.setVisibility(View.GONE);
                    }
                    else {
                        cart_relative.setVisibility(View.VISIBLE);
                        cart_no.setText(String.valueOf(count_no));
                    }

                } else if (cartCountResponse.status.equals("0")) {
//                    Toast.makeText(ProductDetailsActivity.this, cartCountResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                Toast.makeText(ProductDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

        finish();
//        startActivity(new Intent(ProductDetailsActivity.this, MainActivity.class));
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }
//
//    private void SlideImages(String m1,String pname) {
//        String[] images = {m1};
//
//        VeiewAdapter viewPagerAdapter = new VeiewAdapter(images,pname, ProductDetailsActivity.this);
//        viewPager.setAdapter(viewPagerAdapter);
//        indicator.setViewPager(viewPager);
//    }

    private void SingleImages(List<HomeProductimageResponse> homeProductimageResponse,String name) {
        VeiewAdapter viewPagerAdapter = new VeiewAdapter(homeProductimageResponse, name,ProductDetailsActivity.this);
        viewPager.setAdapter(viewPagerAdapter);
        indicator.setViewPager(viewPager);
    }

    public void ProductDetails(String vv, final String user_id, final String device) {

        final ProgressDialog progressDialog = new ProgressDialog(ProductDetailsActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProductVariationDetailsResponse> call = apiInterface.ProductVariationDetails(vv, user_id,device);
        call.enqueue(new Callback<ProductVariationDetailsResponse>() {
            @Override
            public void onResponse(Call<ProductVariationDetailsResponse> call, Response<ProductVariationDetailsResponse> response) {
                if (response.isSuccessful()) ;
                final ProductVariationDetailsResponse productVariationDetailsResponse = response.body();
                if (productVariationDetailsResponse.status.equals("1")) {
                    linear_product.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    productVariationDetailsDataResponse = productVariationDetailsResponse.data;
                    for (i = 0; i < productVariationDetailsDataResponse.size(); i++) {
                        company_name = productVariationDetailsDataResponse.get(0).brand;
                        product_name = productVariationDetailsDataResponse.get(0).productName;
                        description = productVariationDetailsDataResponse.get(0).description;

                        String items=productVariationDetailsDataResponse.get(0).noOfItems;

                        if (items.equals("0")){
                            linear_add.setVisibility(View.VISIBLE);
                            Relative_increase.setVisibility(View.GONE);
                        }
                        else {
                            linear_add.setVisibility(View.GONE);
                            Relative_increase.setVisibility(View.VISIBLE);
                            show_value.setText(items);
                        }

                        String Decr = Html.fromHtml(description).toString();

                        company.setText(company_name);
                        productname.setText(product_name);
                        descr.setText(Decr);
                        title.setText(product_name);

                        if (productVariationDetailsDataResponse != null) {
                            category_spinner = new ArrayList<>();
                            sav = " ₹" + productVariationDetailsDataResponse.get(0).youSaved;
                            m_price = "₹ " + productVariationDetailsDataResponse.get(0).actualPrice;
                            d_price = "₹ " + productVariationDetailsDataResponse.get(0).unitPrice;
                            p_off = String.valueOf(productVariationDetailsDataResponse.get(0).discount) + "%" + "\n" + "OFF";
                            quanti = productVariationDetailsDataResponse.get(0).quantity;
                            price1 = "₹ " + productVariationDetailsDataResponse.get(0).unitPrice;

                            variationid = productVariationDetailsDataResponse.get(0).productVariationId;
                            startlimit = productVariationDetailsDataResponse.get(0).startLimit;
                            endlimit = productVariationDetailsDataResponse.get(0).endLimit;

                            save.setText(sav);
                            margin_price.setText(m_price);
                            discount_price.setText(d_price);
                            per_off.setText(p_off);
                            alert_text.setText(productVariationDetailsDataResponse.get(0).quantity+"₹"+productVariationDetailsDataResponse.get(0).unitPrice);

                            final List<HomeProductimageResponse> homeProductimageResponse=productVariationDetailsDataResponse.get(0).variationImage;
                            SingleImages(homeProductimageResponse,product_name);

                            final int finalI = i;

                            alert_relative.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

//                                    ProductDetails(variation_id, user_id,device);

                                    final AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this);
                                    View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.alert_productlisting, null);
                                    alert_listview = view.findViewById(R.id.alert_listview);
                                    customSpinnerAdapter = new CustomSpinnerAdapter1(ProductDetailsActivity.this, android.R.layout.simple_list_item_1, category_spinner, alert_text.getText().toString());

                                    TextView productname = view.findViewById(R.id.productname);
                                    productname.setText(productVariationDetailsDataResponse.get(finalI).productName);

                                    alert_listview.setAdapter(customSpinnerAdapter);
                                    builder.setView(view);
                                    final AlertDialog b = builder.create();
                                    b.show();
                                    category_spinner.clear();

                                    productVariationDetailsDataResponse = productVariationDetailsResponse.data;
                                    for (int j = 0; j < productVariationDetailsDataResponse.size(); j++) {
                                        final Category_spinner area = new Category_spinner();
//                                        area.setSave(Float.valueOf(productVariationDetailsDataResponse.get(j).youSaved));
                                        area.setImage1("₹" +productVariationDetailsDataResponse.get(j).youSaved);
                                        area.setId(productVariationDetailsDataResponse.get(j).productVariationId);
                                        area.setQuantiti(productVariationDetailsDataResponse.get(j).quantity);
                                        area.setPricee("" + "₹" + productVariationDetailsDataResponse.get(j).unitPrice);
                                        area.setDprice("₹ " + productVariationDetailsDataResponse.get(j).actualPrice);
                                        area.setOff(Float.valueOf(productVariationDetailsDataResponse.get(j).discount) + "%" + "\n" + "OFF");
                                        area.setStartlimit(productVariationDetailsDataResponse.get(j).startLimit);
                                        area.setEndlimit(productVariationDetailsDataResponse.get(j).endLimit);
                                        area.setImage2(productVariationDetailsDataResponse.get(j).productName);
                                        category_spinner.add(area);

                                        alert_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int j, long id) {

//                                                ProductDetails(variation_id, user_id,device);

                                                view.setSelected(true);
                                                margin_price.setText(category_spinner.get(j).getDprice());
//                                                margin_price.setPaintFlags(margin_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                                discount_price.setText(category_spinner.get(j).getPricee());
                                                per_off.setText(category_spinner.get(j).getOff());
                                                SingleImages(productVariationDetailsDataResponse.get(j).variationImage,category_spinner.get(j).getImage2());
                                                alert_text.setText(category_spinner.get(j).getQuantiti()+category_spinner.get(j).getPricee());
                                                variationid = category_spinner.get(j).getId();
                                                startlimit = category_spinner.get(j).getStartlimit();
                                                endlimit = category_spinner.get(j).getEndlimit();
                                                save.setText(category_spinner.get(j).getImage1());

                                                String cont=productVariationDetailsDataResponse.get(j).noOfItems;

                                                if (cont.equals("0"))
                                                {
                                                    linear_add.setVisibility(View.VISIBLE);
                                                    Relative_increase.setVisibility(View.GONE);
                                                }
                                                else {
                                                    linear_add.setVisibility(View.GONE);
                                                    Relative_increase.setVisibility(View.VISIBLE);
                                                    show_value.setText(cont);

                                                }

                                                b.dismiss();
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                }
                else if (productVariationDetailsResponse.status.equals("0")) {
                    linear_product.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    Toast.makeText(ProductDetailsActivity.this, productVariationDetailsResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProductVariationDetailsResponse> call, Throwable t) {
                linear_product.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(ProductDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}