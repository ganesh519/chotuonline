package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.AddAddressRecyclerAdapter1;
import igrand.info.tech.Chotuonline.model.AddressBookDataResoponse;
import igrand.info.tech.Chotuonline.model.AddressBookResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Addressbook extends AppCompatActivity {
    RelativeLayout add_address;
    ApiInterface apiInterface;
    RecyclerView add_recycler;
    Context context;
    ImageView back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addressbook);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        add_address = findViewById(R.id.add_address);
        add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Addressbook.this, AddressActivity.class).putExtra("type", "add"));
                finish();

            }
        });

        add_recycler = findViewById(R.id.add_recycler);
        final ProgressDialog progressDialog = new ProgressDialog(Addressbook.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
            String user_id = sharedPreferences.getString("user_id", "");

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddressBookResponse> calladd = apiInterface.AddressBook(user_id);
        calladd.enqueue(new Callback<AddressBookResponse>() {
            @Override
            public void onResponse(Call<AddressBookResponse> call, Response<AddressBookResponse> response) {
                if (response.isSuccessful()) ;
                AddressBookResponse addressBookResponse = response.body();
                if (addressBookResponse.status.equals("1")) {
                    progressDialog.dismiss();
                    List<AddressBookDataResoponse> addressBookDataResoponse = addressBookResponse.data;
                    if (addressBookDataResoponse != null) {
//                        Collections.reverse(addressBookDataResoponse);
                        AddAddressRecyclerAdapter1 addAddressRecyclerAdapter = new AddAddressRecyclerAdapter1(getApplicationContext(), addressBookDataResoponse, "address");
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        add_recycler.setLayoutManager(layoutManager);
//                                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        add_recycler.setNestedScrollingEnabled(false);
                        add_recycler.setHasFixedSize(true);
//                        addAddressRecyclerAdapter.notifyDataSetChanged();
                        add_recycler.setAdapter(addAddressRecyclerAdapter);
                    }
                } else if (addressBookResponse.status.equals("0")) {
                    progressDialog.dismiss();
                    Toast.makeText(Addressbook.this, addressBookResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddressBookResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Addressbook.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
