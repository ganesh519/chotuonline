package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AllSubSubCategoryDataResponse {



    @SerializedName("category_id")
    @Expose
    public String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    public String subcategoryId;
    @SerializedName("subsubcategory_id")
    @Expose
    public String subsubcategoryId;
    @SerializedName("subsubcategory_name")
    @Expose
    public String subsubcategoryName;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("categoryId", categoryId).append("subcategoryId", subcategoryId).append("subsubcategoryId", subsubcategoryId).append("subsubcategoryName", subsubcategoryName).toString();
    }
}
