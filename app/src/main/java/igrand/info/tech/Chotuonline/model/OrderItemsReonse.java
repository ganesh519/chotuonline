package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class OrderItemsReonse {

    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("display_name")
    @Expose
    public String displayName;
    @SerializedName("brand")
    @Expose
    public String brand;
    @SerializedName("image1")
    @Expose
    public String image1;
    @SerializedName("actual_price")
    @Expose
    public String actualPrice;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("quantity")
    @Expose
    public String quantity;
    @SerializedName("product_quantity")
    @Expose
    public String productQuantity;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productId", productId).append("productName", productName).append("displayName", displayName).append("brand", brand).append("image1", image1).append("actualPrice", actualPrice).append("price", price).append("quantity", quantity).append("productQuantity", productQuantity).toString();
    }
}
