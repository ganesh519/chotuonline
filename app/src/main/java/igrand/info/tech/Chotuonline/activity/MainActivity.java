package igrand.info.tech.Chotuonline.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


import de.hdodenhof.circleimageview.CircleImageView;
import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.OnClickExpand;
import igrand.info.tech.Chotuonline.R;

import igrand.info.tech.Chotuonline.fragments.AllCategoryFragment;
import igrand.info.tech.Chotuonline.fragments.CategoryMainListFragment;
import igrand.info.tech.Chotuonline.fragments.ExpandableListAdapterCategory;
import igrand.info.tech.Chotuonline.fragments.HomeFragment;
import igrand.info.tech.Chotuonline.model.AllSubCategoryResponse;
import igrand.info.tech.Chotuonline.model.AllSubSubCategoryDataResponse;
import igrand.info.tech.Chotuonline.model.All_CategoryDataResponse;
import igrand.info.tech.Chotuonline.model.All_CategoryResponse;
import igrand.info.tech.Chotuonline.model.CartCountDataResponse;
import igrand.info.tech.Chotuonline.model.CartCountResponse;
import igrand.info.tech.Chotuonline.model.Navigation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;




public class MainActivity extends AppCompatActivity implements OnClickExpand {

    private static Context ctx;
    ListView listView;
    ImageView menu_iv,edit_iv, iv_navheader_edit;
    public static ImageView user_iv,logo,main_search;
    public static TextView category_text;
    ArrayList<Navigation> nav_list;
    NavigationView navigationView;
    Menu nav_Menu;
    LinearLayout ll_mainbottom_home, ll_mainbottom_category, search, ll_mainbottom_offer;
    RelativeLayout ll_mainbottom_cart;
    ExpandableListAdapter listAdapter1;
    ExpandableListView expListView;
    List<String> listDataHeader;
    List<String> listDataHeader1;
    HashMap<String, List<String>> listDataChild;
    HashMap<String, List<String>> listDataChild1;
    LinearLayout nav_header_accountView, nav_header_categories,nav_header_without_Login;
    TextView tv_mainMenu;
    TextView home_text,Category_text,search_text,offers_text,cart_text;
    RelativeLayout signup_withou,login_withou;

    ExpandableListView expandableListView;
    android.widget.ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    LinkedHashMap<String, List<AllSubCategoryResponse>> expandableListDetail;
    HashMap<String, List<String>> expandableListDetail_new;
    CircleImageView menu_profile_image;
    TextView menu_name,menu_mobile;
    private static ApiInterface apiInterface;
    private static TextView cart_no;
    private static RelativeLayout cart_relative;
    public  DrawerLayout drawer;
    Typeface custom_font;
    FragmentManager fragmentManager;
    Fragment currentFragment;
    FragmentTransaction fragmentTransaction;

    @Override
    public void subItemSelected(AllSubCategoryResponse all_categoryDataResponse) {
        drawer.closeDrawer(GravityCompat.START);

        String cat_id = all_categoryDataResponse.categoryId;
        String sub_id=all_categoryDataResponse.subcategoryId;
        String cat_name =all_categoryDataResponse.subcategoryName;

        Bundle bundle=new Bundle();
        bundle.putString("category_id", cat_id);
        bundle.putString("subcategory_id", sub_id);
        bundle.putString("category_name", cat_name);
        bundle.putString("menuList","data");
        bundle.putString("all_category","Allcategory");

        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
        fragmentTransaction =getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, categoryMainListFragment,"");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        categoryMainListFragment.setArguments(bundle);
    }

    @Override
    public void subsubItemSelected(AllSubCategoryResponse all_categoryDataResponse,AllSubSubCategoryDataResponse rowItem) {
        drawer.closeDrawer(GravityCompat.START);

        String cat_id = all_categoryDataResponse.categoryId;
        String sub_id=all_categoryDataResponse.subcategoryId;
        String cat_name =all_categoryDataResponse.subcategoryName;
        String sub_sub_id =rowItem.subsubcategoryId;

        Bundle bundle=new Bundle();
        bundle.putString("category_id", cat_id);
        bundle.putString("subcategory_id", sub_id);
        bundle.putString("sub_sub_id", sub_sub_id);
        bundle.putString("category_name", cat_name);
        bundle.putString("all_category","");
        bundle.putString("menuList","data");
        bundle.putString("SubSubCategory","subsubcategory");

        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
        fragmentTransaction =getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, categoryMainListFragment,"");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        categoryMainListFragment.setArguments(bundle);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initxml();
//        setNavigation();
        drawer = findViewById(R.id.drawer_layout);

        expListView = findViewById(R.id.menuList);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<All_CategoryResponse> categoryResponseCall=apiInterface.AllCategory();
        categoryResponseCall.enqueue(new Callback<All_CategoryResponse>() {
            @Override
            public void onResponse(Call<All_CategoryResponse> call, Response<All_CategoryResponse> response) {
                if (response.isSuccessful());
                All_CategoryResponse all_categoryResponse=response.body();
                if (all_categoryResponse.status.equals("1")){
                    final List<All_CategoryDataResponse> all_categoryDataResponse=all_categoryResponse.data;
                    expandableListDetail=new LinkedHashMap<>();
                    for (int i = 0; i <all_categoryDataResponse.size() ; i++) {

                        List<AllSubCategoryResponse> Fruits=null;
                        Fruits = new ArrayList<AllSubCategoryResponse>();
                        for (int j = 0; j <all_categoryDataResponse.get(i).subcategoryinfo.size(); j++) {
                            Fruits.add(all_categoryDataResponse.get(i).subcategoryinfo.get(j));

                        }
                        expandableListDetail.put(all_categoryDataResponse.get(i).categoryName, Fruits);
                    }

                    expandableListView = findViewById(R.id.categorylist);
                    expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
                    expandableListAdapter = new ExpandableListAdapterCategory(getApplicationContext(), expandableListTitle, expandableListDetail,MainActivity.this);
                    expandableListView.setAdapter(expandableListAdapter);
                }
            }

            @Override
            public void onFailure(Call<All_CategoryResponse> call, Throwable t) {

            }
        });
        listDataHeader = new ArrayList<>();
        listDataChild = new LinkedHashMap<>();

        listDataHeader.add("Home");
        SharedPreferences sharedPreference = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreference.getString("user_id", "");
        if(!TextUtils.isEmpty(user_id)){
            listDataHeader.add("My Accounts");

        }

        listDataHeader.add("Shop By Category");
        listDataHeader.add("Notifications");
        listDataHeader.add("Offers");
        listDataHeader.add("FAQs");
        listDataHeader.add("Contact US");
        listDataHeader.add("Terms & Conditions");
        listDataHeader.add("About US");
        listDataHeader.add("Share App");
        if (!TextUtils.isEmpty(user_id)){
            listDataHeader.add("Customer Service");
        }
        listDataHeader.add("Review Us");
        if(!TextUtils.isEmpty(user_id)){
            listDataHeader.add("Logout");

        }
        final List<String> list = new ArrayList<>();
        List<String> list1 = new ArrayList<>();
        list1.add("My Orders");
        list1.add("My Address Book");
        list1.add("My Profile");
        list1.add("Change pasword");
        list1.add("Change Mobile Number");

        listDataChild.put("Home", list);
        listDataChild.put("My Accounts", list1);
        listDataChild.put("Shop By Category", list);
        listDataChild.put("Notifications", list);
        listDataChild.put("Offers",list);
        listDataChild.put("FAQs", list);
        listDataChild.put("Contact US", list);
        listDataChild.put("Terms & Conditions", list);
        listDataChild.put("About US", list);
        listDataChild.put("Share App",list);
        listDataChild.put("Customer Service",list);
        listDataChild.put("Review Us", list);
        listDataChild.put("Logout", list);

        listAdapter1 = new ExpandableListAdapter(getApplicationContext(), listDataHeader, listDataChild);

        //expandableListDetail = ExpandableListDataPump.getData();


        expListView.setAdapter(listAdapter1);
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                boolean value = true;
                SharedPreferences sharedPreference = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreference.getString("user_id", "");
                if(!TextUtils.isEmpty(user_id)){
                    if (groupPosition == 0) {
                        ChangeTextColor(true,false,false,false,false);
                        HomeFragment fragment = new HomeFragment();
                        setFragment(fragment);


                    } else if (groupPosition == 1) {
                        value = false;

                    } else if (groupPosition == 2) {
                        value = false;
                        expListView.setVisibility(View.GONE);
                        expandableListView.setVisibility(View.VISIBLE);
                        nav_header_accountView.setVisibility(View.GONE);
                        nav_header_categories.setVisibility(View.VISIBLE);
                    }
                    else if (groupPosition == 3) {

                        startActivity(new Intent(MainActivity.this, NotificationsActivity.class).putExtra("Notifications","notifications"));
                    }
                    else if (groupPosition == 4){
                        startActivity(new Intent(MainActivity.this, OffersActivity.class));

                    }
                    else if (groupPosition == 5) {

                        startActivity(new Intent(MainActivity.this, FAQsActivity.class));

                    }
                    else if (groupPosition == 6) {

                        startActivity(new Intent(MainActivity.this, ContactUsActivity.class));

                    }
                    else if (groupPosition == 7) {

                        startActivity(new Intent(MainActivity.this, TermsConditionActivity.class));
                    }
                    else if (groupPosition == 8) {

                        startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
                    }
                    else if (groupPosition == 9){

                        try {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_SUBJECT, "Chotu Online");
                            String sAux = "\nPlease install this application \n\n";
                            sAux = sAux + "https://play.google.com/store/apps/details?id=igrand.info.tech.Chotuonline";
                            i.putExtra(Intent.EXTRA_TEXT, sAux);
                            startActivity(Intent.createChooser(i, "choose one"));
                        } catch (Exception e) {
                            //e.toString();
                        }

                    }
                    else if (groupPosition == 10){
                        startActivity(new Intent(MainActivity.this,Customer_Service.class));

                    }
                    else if (groupPosition == 11){

                        Intent intent=new Intent(android.content.Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=igrand.info.tech.Chotuonline"));
                        startActivity(intent);

                    }
                    else if (groupPosition == 12) {

                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.clear();
                        editor.commit();

                        SharedPreferences sharedPreference1= getApplicationContext().getSharedPreferences("AddressData", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = sharedPreference1.edit();
                        editor1.clear();
                        editor1.commit();

                        Intent intent = new Intent(MainActivity.this, LoginActivity.class).putExtra("signup","login");
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }
                else {
                    if (groupPosition == 0) {
                        ChangeTextColor(true,false,false,false,false);
                        HomeFragment fragment = new HomeFragment();
                        setFragment(fragment);
                    }
                    else if (groupPosition == 1) {
                        value = false;
                        expListView.setVisibility(View.GONE);
                        expandableListView.setVisibility(View.VISIBLE);
                        nav_header_accountView.setVisibility(View.GONE);
                        nav_header_without_Login.setVisibility(View.GONE);
                        nav_header_categories.setVisibility(View.VISIBLE);
                    }
                    else if (groupPosition == 2) {

                        startActivity(new Intent(MainActivity.this, NotificationsActivity.class).putExtra("Notifications","notifications"));
                    }
                    else if (groupPosition == 3){
                        startActivity(new Intent(MainActivity.this, OffersActivity.class));
                    }
                    else if (groupPosition == 4) {

                        startActivity(new Intent(MainActivity.this, FAQsActivity.class));

                    }
                    else if (groupPosition == 5) {

                        startActivity(new Intent(MainActivity.this, ContactUsActivity.class));

                    }
                    else if (groupPosition == 6) {

                        startActivity(new Intent(MainActivity.this, TermsConditionActivity.class));
                    }
                    else if (groupPosition == 7) {

                        startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
                    }
                    else if (groupPosition == 8){

                        try {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_SUBJECT, "Chotu Online");
                            String sAux = "\nPlease install this application \n\n";
                            sAux = sAux + "https://play.google.com/store/apps/details?id=igrand.info.tech.Chotuonline";
                            i.putExtra(Intent.EXTRA_TEXT, sAux);
                            startActivity(Intent.createChooser(i, "choose one"));
                        }
                        catch (Exception e) {
                            //e.toString();
                        }

                    }
                    else if (groupPosition == 9){
                        Intent intent=new Intent(android.content.Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=igrand.info.tech.Chotuonline"));
                        startActivity(intent);

                    }

                }
                if (value) {
                    DrawerLayout drawer = findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                }
                else {
                    DrawerLayout drawer = findViewById(R.id.drawer_layout);
                    //  drawer.closeDrawer(GravityCompat.START);

                }

                return false;
            }
        });


        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupgroupPosition, int childgroupPosition, long id) {

                if (childgroupPosition == 0) {
                    startActivity(new Intent(MainActivity.this, MyOrderActivity.class));
                    drawer.closeDrawer(GravityCompat.START);
                }
                else if (childgroupPosition == 1) {

                    startActivity(new Intent(MainActivity.this, Addressbook.class).putExtra("Addressbook","MainActivity"));
                    drawer.closeDrawer(GravityCompat.START);
                }
                else if (childgroupPosition == 2) {

                    startActivity(new Intent(MainActivity.this, MyAccountActivity.class));
                    drawer.closeDrawer(GravityCompat.START);
                }
                else if (childgroupPosition == 3) {
                    startActivity(new Intent(MainActivity.this, ChangePasswordActivity.class));
                    drawer.closeDrawer(GravityCompat.START);
                }
                else if (childgroupPosition == 4) {
                    startActivity(new Intent(MainActivity.this, ChangeMobileNumberActivity.class));
                    drawer.closeDrawer(GravityCompat.START);
                }

                return false;
            }
        });
    }

    private void setFragment(android.support.v4.app.Fragment fragment) {

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, fragment,"");
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }

    private void initxml() {
        ctx = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        home_text=findViewById(R.id.home_text);
        Category_text=findViewById(R.id.Category_text);
        search_text=findViewById(R.id.search_text);
        offers_text=findViewById(R.id.offers_text);
        cart_text=findViewById(R.id.cart_text);

        navigationView = findViewById(R.id.nav_view);
        ll_mainbottom_home = findViewById(R.id.ll_mainbottom_home);
        ll_mainbottom_category = findViewById(R.id.ll_mainbottom_category);
        search = findViewById(R.id.search);
        ll_mainbottom_offer = findViewById(R.id.ll_mainbottom_offer);
        ll_mainbottom_cart = findViewById(R.id.ll_mainbottom_cart);
        menu_iv = (ImageView) findViewById(R.id.menu);
        user_iv = (ImageView) findViewById(R.id.iv_main_user);
        //user_iv.setVisibility(View.VISIBLE);
        logo=findViewById(R.id.logo);
        // logo.setVisibility(View.VISIBLE);
        main_search=findViewById(R.id.main_search);
        // main_search.setVisibility(View.GONE);
        category_text=findViewById(R.id.category_text);
        //category_text.setVisibility(View.GONE);

        cart_relative=findViewById(R.id.cart_relative);
        cart_no=findViewById(R.id.cart_no);

        CartCount();

        main_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,SearchActivity.class));
            }
        });

        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);
        menu_mobile=headerLayout.findViewById(R.id.menu_mobile);
        menu_profile_image=headerLayout.findViewById(R.id.menu_profile_image);
        menu_name=headerLayout.findViewById(R.id.menu_name);

        SharedPreferences sharedPreferences = getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String image=sharedPreferences.getString("profileimage","");
        String name=sharedPreferences.getString("Name","");
        String mobile=sharedPreferences.getString("Mobile","");

        menu_name.setText(name);
        menu_mobile.setText(mobile);
        if (image.equals("")){
            menu_profile_image.setImageResource(R.drawable.no_image);
        }
        else {
            Picasso.with(getApplicationContext()).load(image).error(R.drawable.no_image).into(menu_profile_image);
        }
        nav_Menu = navigationView.getMenu();
        nav_header_accountView =  headerLayout.findViewById(R.id.nav_header_accountView);
        login_withou =  headerLayout.findViewById(R.id.Login_withou);
        signup_withou =  headerLayout.findViewById(R.id.signup_withou);
        nav_header_without_Login = headerLayout.findViewById(R.id.nav_header_without_Login);
        nav_header_categories =  headerLayout.findViewById(R.id.nav_header_categories);
        tv_mainMenu = headerLayout.findViewById(R.id.tv_mainMenu);
        tv_mainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreference = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreference.getString("user_id", "");
                if (TextUtils.isEmpty(user_id)){
                    nav_header_categories.setVisibility(View.GONE);
                    expListView.setVisibility(View.VISIBLE);
                    expandableListView.setVisibility(View.GONE);
                    nav_header_accountView.setVisibility(View.GONE);
                    nav_header_without_Login.setVisibility(View.VISIBLE);
                }
                else {
                    nav_header_accountView.setVisibility(View.VISIBLE);
                    nav_header_categories.setVisibility(View.GONE);
                    expListView.setVisibility(View.VISIBLE);
                    expandableListView.setVisibility(View.GONE);
                }
            }
        });

        SharedPreferences sharedPreference = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreference.getString("user_id", "");
        if (TextUtils.isEmpty(user_id)){
            nav_header_without_Login.setVisibility(View.VISIBLE);
            nav_header_accountView.setVisibility(View.GONE);

        }
        else {
            nav_header_without_Login.setVisibility(View.GONE);
            nav_header_accountView.setVisibility(View.VISIBLE);


        }

        login_withou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,LoginActivity.class).putExtra("signup","login"));

            }
        });
        signup_withou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,LoginActivity.class).putExtra("signup","Sign"));


            }
        });

        edit_iv = headerLayout.findViewById(R.id.iv_navheader_edit);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        edit_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), EditProfileActivity.class));
            }
        });
        ChangeTextColor(true,false,false,false,false);
        fragmentManager=getSupportFragmentManager();
//        HomeFragment fragment = new HomeFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout,new HomeFragment(), "homee");
//        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        menu_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        user_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");
                if (TextUtils.isEmpty(user_id)){
                    startActivity(new Intent(ctx, LoginActivity.class).putExtra("signup","login"));

                }else {
                    startActivity(new Intent(ctx, MyAccountActivity.class));

                }


            }
        });

        ll_mainbottom_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeTextColor(true,false,false,false,false);
                HomeFragment fragment = new HomeFragment();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fl_main_framelayout, fragment, "homee");
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        ll_mainbottom_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeTextColor(false,true,false,false,false);
                AllCategoryFragment fragment = new AllCategoryFragment();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fl_main_framelayout, fragment,"");
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeTextColor(false,false,true,false,false);
                startActivity(new Intent(MainActivity.this,SearchActivity.class));
            }
        });
        ll_mainbottom_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeTextColor(false,false,false,true,false);
                startActivity(new Intent(MainActivity.this, NotificationsActivity.class).putExtra("Notifications","notifications"));
            }
        });

        ll_mainbottom_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeTextColor(false,false,false,false,true);
                startActivity(new Intent(MainActivity.this, CartActivity.class));
            }
        });

    }

    @Override
    public void onBackPressed() {
        currentFragment=fragmentManager.findFragmentById(R.id.fl_main_framelayout);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        }
        else if (currentFragment.getTag().equals("homee")){
            showAlert();
        }
        else {
            super.onBackPressed();
        }

    }

    public static void CartCount(){

        SharedPreferences sharedPreference =ctx.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreference.getString("user_id", "");
        SharedPreferences sharedPreferenc =ctx.getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
        String device = sharedPreferenc.getString("mobile_id","");

        apiInterface=ApiClient.getClient().create(ApiInterface.class);
        Call<CartCountResponse> cartCountResponseCall=apiInterface.CartCount(user_id,device);
        cartCountResponseCall.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful());
                CartCountResponse cartCountResponse=response.body();
                if (cartCountResponse.status.equals("1")){
                    CartCountDataResponse cartCountDataResponse=cartCountResponse.data;
                    int count_no=cartCountDataResponse.count;
                    cart_no.setText(String.valueOf(count_no));
                    if (count_no == 0){
                        cart_relative.setVisibility(View.GONE);
                    }
                    else {
                        cart_relative.setVisibility(View.VISIBLE);
                        cart_no.setText(String.valueOf(count_no));
                    }

                }
                else if (cartCountResponse.status.equals("0")){
                    cart_relative.setVisibility(View.GONE);
//                    Toast.makeText(ctx, cartCountResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                Toast.makeText(ctx, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void showAlert() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
        alertDialog.setTitle("Exit");
        alertDialog.setMessage("Are you sure you want close this application?");


        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                startActivity(new Intent(ctx, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                sessionManager.logoutUser();
                finishAffinity();
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void ChangeTextColor(boolean home,boolean category,boolean search,boolean offers,boolean cart){
        if(home)
        {
            home_text.setTextColor(Color.parseColor("#E97824"));
        }
        else
        {
            home_text.setTextColor(Color.parseColor("#FFFFFF"));
        }
        if (category)
        {
            Category_text.setTextColor(Color.parseColor("#E97824"));
        }
        else
        {
            Category_text.setTextColor(Color.parseColor("#FFFFFF"));
        }
        if (search)
        {
            search_text.setTextColor(Color.parseColor("#E97824"));
        }
        else
        {
            search_text.setTextColor(Color.parseColor("#FFFFFF"));
        }
        if (offers)
        {
            offers_text.setTextColor(Color.parseColor("#E97824"));
        }
        else
        {
            offers_text.setTextColor(Color.parseColor("#FFFFFF"));
        }
        if (cart)
        {
            cart_text.setTextColor(Color.parseColor("#E97824"));
        }
        else
        {
            cart_text.setTextColor(Color.parseColor("#FFFFFF"));
        }
    }


}
