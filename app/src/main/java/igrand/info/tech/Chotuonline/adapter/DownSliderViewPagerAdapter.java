package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.fragments.CategoryMainListFragment;
import igrand.info.tech.Chotuonline.model.DownSliderResponse;



public class DownSliderViewPagerAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    List<DownSliderResponse> downSliderResponse;
    private String name;

    public DownSliderViewPagerAdapter(List<DownSliderResponse> downSliderResponse,Context context) {
        this.context = context;
        this.downSliderResponse = downSliderResponse;
    }

    @Override
    public int getCount() {
        return downSliderResponse.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.downslider_layout, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

        Picasso.with(context).load(downSliderResponse.get(position).image).into(imageView);
        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String category_id=downSliderResponse.get(position).catId;
                String type=downSliderResponse.get(position).linkType;
                String type_id=downSliderResponse.get(position).linkId;
                String category_name=downSliderResponse.get(position).linkName;

                if (type.equals("Product")){

                    if (type_id.equals("0")){
                        Toast.makeText(context, "Product", Toast.LENGTH_SHORT).show();
                    }
                    else {


                    }

                }else if (type.equals("Sub Category")){
                    if (type_id.equals("0")){
                        Toast.makeText(context, "Sub Category", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Bundle bundle=new Bundle();
                        bundle.putString("category_id", category_id);
                        bundle.putString("subcategory_id", type_id);
                        bundle.putString("category_name", category_name);
                        bundle.putString("menuList","data");
                        bundle.putString("all_category","Allcategory");

                        AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
                        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
                        appCompatActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fl_main_framelayout,categoryMainListFragment,"").addToBackStack("").commit();
                        categoryMainListFragment.setArguments(bundle);

                    }
                }
                else if (type.equals("Sub Sub Category")){
                    if (type_id.equals("0")){
                        Toast.makeText(context, "Sub Sub Category", Toast.LENGTH_SHORT).show();
                    }
                    else {
//                        Bundle bundle=new Bundle();
//                        bundle.putString("category_id", category_id);
//                        bundle.putString("subcategory_id", sub_categoryid);
//                        bundle.putString("sub_sub_id", type_id);
//                        bundle.putString("category_name", category_name);
//                        bundle.putString("all_category","");
//                        bundle.putString("menuList","data");
//                        bundle.putString("SubSubCategory","subsubcategory");
//
//                        AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
//                        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
//                        appCompatActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fl_main_framelayout,categoryMainListFragment,"").addToBackStack("").commit();
//                        categoryMainListFragment.setArguments(bundle);
                    }
                }
                else if (type.equals("Category")){
                    if (type_id.equals("0")){
                        Toast.makeText(context, "Category", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Bundle bundle=new Bundle();
                        bundle.putString("category_id", type_id);
                        bundle.putString("subcategory_id", "");
                        bundle.putString("category_name", category_name);
                        bundle.putString("all_category","gfgfg");
                        bundle.putString("SubSubCategory","");

                        AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
                        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransaction = appCompatActivity.getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fl_main_framelayout, categoryMainListFragment,"");
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        categoryMainListFragment.setArguments(bundle);
                    }
                }


            }
        });


        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }

}
