package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.CallModel;

public class CallAdapter  extends ArrayAdapter<String> {

    private Context activity;
    private List<CallModel> data;
    public Resources res;
    String mSelected;
    CallModel tempValues = null;
    LayoutInflater inflater;

    /*************  CustomSpinnerAdapter Constructor *****************/
    public CallAdapter(
            Context activity,
            int textViewResourceId,
            List data,String mSelected
    ) {
        super(activity, textViewResourceId, data);

        /********** Take passed values **********/
        this.activity = activity;
        this.data = data;
        this.mSelected = mSelected;

        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.call_rows, parent, false);

        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (CallModel) data.get(position);

        TextView label = (TextView) row.findViewById(R.id.phone_no);


        // Set values for spinner each row
        label.setText(tempValues.getName());
        label.setTag(tempValues.getId());

        if (mSelected.equals(tempValues.getName())){
            label.setTextColor(Color.parseColor("#2596c8"));

        }
        else {
            label.setTextColor(Color.parseColor("#000000"));

        }

        return row;
    }
}