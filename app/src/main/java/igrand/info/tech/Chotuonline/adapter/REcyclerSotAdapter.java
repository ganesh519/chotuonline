package igrand.info.tech.Chotuonline.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.List;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.CheckOutActivity;
import igrand.info.tech.Chotuonline.model.SlotDataDetailsResponse;
import igrand.info.tech.Chotuonline.model.SlotDataResponse;

public class REcyclerSotAdapter extends RecyclerView.Adapter<REcyclerSotAdapter.Holder> {
    Context context;
    List<SlotDataResponse> slotDataResponses;
    public static String date;
    String idd;
    TextView slot_date;
    private int lastPosition = -1;

    public REcyclerSotAdapter(Context context, List<SlotDataResponse> slotDataResponses, String idd, TextView slot_date) {
        this.context = context;
        this.slotDataResponses = slotDataResponses;
        this.idd = idd;
        this.slot_date=slot_date;
    }

    @NonNull
    @Override
    public REcyclerSotAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_slot, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final REcyclerSotAdapter.Holder holder, final int position) {

        setAnimation(holder.itemView,position);
        String name = CheckOutActivity.slot_text.getText().toString().trim();
        String id = CheckOutActivity.Identy;
        holder.text_date.setText(slotDataResponses.get(position).day);
//        holder.list_slot.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        holder.list_slot.setAdapter(new CustomSlotAdapter(slotDataResponses.get(position).slots, context, id));
        holder.list_slot.setScrollContainer(true);
        final List<SlotDataDetailsResponse> slotDataDetailsResponse=slotDataResponses.get(position).slots;
//        slotDataDetailsResponse.get(position).first=true;



        holder.list_slot.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                view.setSelected(true);
                CheckOutActivity.slot_text.setText(slotDataDetailsResponse.get(pos).name);
                CheckOutActivity.Identy = String.valueOf(slotDataDetailsResponse.get(pos).sNo);
                date=slotDataResponses.get(position).day;

                SharedPreferences sharedPreferences1 = context.getSharedPreferences("date", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences1.edit();
                editor.putString("DateSlot", date);
                editor.commit();
                slotDataResponses.get(position).first=true;

                int disable = slotDataDetailsResponse.get(pos).disablity;
                if (disable == 0) {
                    holder.list_slot.getChildAt(pos).setEnabled(false);
                }
                else {
                    holder.list_slot.getChildAt(pos).setEnabled(true);
                    CheckOutActivity.slot_date.setText(date);
                    CheckOutActivity.b.dismiss();
                }
//
//                CheckOutActivity.slot_date.setText(date);
//                CheckOutActivity.b.dismiss();

            }
        });

        date=slotDataResponses.get(position).day;
        SharedPreferences sharedPreferences1 = context.getSharedPreferences("date", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences1.edit();
        editor.putString("DateSlot", date);
        editor.commit();

    }


    @Override
    public int getItemCount() {
        return slotDataResponses.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView text_date,slot_txt;
        ListView list_slot;

        public Holder(View itemView) {
            super(itemView);
            text_date = itemView.findViewById(R.id.text_date);
//            slot_txt=itemView.findViewById(R.id.slot_txt);
            list_slot = itemView.findViewById(R.id.list_slot);
        }
    }


    public class CustomSlotAdapter extends BaseAdapter {
        List<SlotDataDetailsResponse> slotDataResponse;
        Context context;
        String name;

        CustomSlotAdapter(List<SlotDataDetailsResponse> slotDataResponse, Context context, String name) {
            this.slotDataResponse = slotDataResponse;
            this.context = context;
            this.name=name;
        }

        @Override
        public int getCount() {
            return slotDataResponse.size();
        }

        @Override
        public Object getItem(int position) {
            return slotDataResponse.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView=LayoutInflater.from(context).inflate(R.layout.slot,parent,false);
            TextView textView=convertView.findViewById(R.id.qantity);
            ImageView imageView=convertView.findViewById(R.id.watch);
            ImageView imageView1=convertView.findViewById(R.id.watch_red);
            ImageView tick=convertView.findViewById(R.id.tick);
            textView.setText(slotDataResponse.get(position).name);
            if (name.equals(String.valueOf(slotDataResponse.get(position).sNo))){
                    textView.setTextColor(Color.parseColor("#DE4B39"));
                    imageView.setVisibility(View.GONE);
                    imageView1.setVisibility(View.VISIBLE);
                    tick.setVisibility(View.VISIBLE);
            }
            else
                {
//                textView.setTextColor(Color.parseColor("#000"));
                    imageView.setVisibility(View.VISIBLE);
                    imageView1.setVisibility(View.GONE);
                    tick.setVisibility(View.GONE);
                }
            return convertView;
        }
    }

    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
