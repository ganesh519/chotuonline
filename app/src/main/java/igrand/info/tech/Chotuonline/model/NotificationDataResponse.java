package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class NotificationDataResponse {



    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("date_time")
    @Expose
    public String dateTime;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("description", description).append("dateTime", dateTime).toString();
    }
}
