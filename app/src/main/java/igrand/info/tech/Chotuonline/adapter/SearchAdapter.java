package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.ProductDetailsActivity;
import igrand.info.tech.Chotuonline.model.SearchDataResponse;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.Holder> {
    List<SearchDataResponse> searchDataResponses;
    Context context;
    int counter = 0;
    private int lastPosition = -1;

    public SearchAdapter(List<SearchDataResponse> searchDataResponses, Context context) {
        this.searchDataResponses = searchDataResponses;
        this.context = context;
    }

    @NonNull
    @Override
    public SearchAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_search, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchAdapter.Holder holder, final int position) {

        setAnimation(holder.itemView,position);
        Picasso.with(context).load(searchDataResponses.get(position).productImage).into(holder.image);
        holder.title.setText(searchDataResponses.get(position).productBrand);
        holder.name.setText(searchDataResponses.get(position).productName+ " - "+searchDataResponses.get(position).quantity+" "+searchDataResponses.get(position).units);
        holder.price.setText(" ₹ " + searchDataResponses.get(position).price);



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pid = searchDataResponses.get(position).productId;
                String v_id=searchDataResponses.get(position).variationId;
                String startlimit=searchDataResponses.get(position).startLimit;
                String endlimit=searchDataResponses.get(position).endLimit;

                SharedPreferences sharedPreferences = context.getSharedPreferences("product", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("p_id", pid);
                editor.commit();

                SharedPreferences sharedPreferences1 = context.getSharedPreferences("Variation", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor1 = sharedPreferences1.edit();
                editor1.putString("v_id", v_id);
                editor1.commit();

                Intent intent = new Intent(context, ProductDetailsActivity.class);
                intent.putExtra("variation","1234");
                intent.putExtra("vid",v_id);
                intent.putExtra("StartLimit",startlimit);
                intent.putExtra("endliMIT",endlimit);
                intent.putExtra("img1",searchDataResponses.get(position).image1);
                intent.putExtra("img2",searchDataResponses.get(position).image2);
                intent.putExtra("img3",searchDataResponses.get(position).image3);
                intent.putExtra("img4",searchDataResponses.get(position).image4);
                intent.putExtra("UnitPrice",searchDataResponses.get(position).unitPrice);
                intent.putExtra("Price",searchDataResponses.get(position).price);
                intent.putExtra("sav",searchDataResponses.get(position).youSaved);
                intent.putExtra("disc",searchDataResponses.get(position).discount);
                intent.putExtra("Qunti",searchDataResponses.get(position).quantity);
                intent.putExtra("actualPrice",searchDataResponses.get(position).actualPrice);
                intent.putExtra("Noofitems",searchDataResponses.get(position).noOfItems);

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return searchDataResponses.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title, name, price;

        public Holder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);

        }
    }
    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
