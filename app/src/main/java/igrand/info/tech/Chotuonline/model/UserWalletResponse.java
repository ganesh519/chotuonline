package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class UserWalletResponse {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("payable_amount")
    @Expose
    public Integer payableAmount;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("message", message).append("payableAmount", payableAmount).toString();
    }

}
