package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.ForgotOtpResponse;
import igrand.info.tech.Chotuonline.model.ForgotpassResendResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotOtp extends AppCompatActivity {

    EditText signup_otp;
    String otp, code;
    ApiInterface apiInterface, apiInterface1;
    Button proceed;
    TextView resend;
    SmsVerifyCatcher smsVerifyCatcher;
    String u_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text);

        signup_otp = findViewById(R.id.signup_otp);
        proceed = findViewById(R.id.proceed);
        resend = findViewById(R.id.resend);
        smsVerifyCatcher = new SmsVerifyCatcher(ForgotOtp.this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                code = parseCode(message);
                signup_otp.setText(code);
            }
        });

        final String Mobile=getIntent().getStringExtra("Formobile");
        final String User_id=getIntent().getStringExtra("FUser_id");



//        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("ForgotMobile", Context.MODE_PRIVATE);
//        String mobile = sharedPreferences.getString("Formobile", "");
//
//        SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("Forgot", Context.MODE_PRIVATE);
//        String user_id = sharedPreferences1.getString("Fuser_id", "");

        final ProgressDialog progressDialog1 = new ProgressDialog(ForgotOtp.this);
        progressDialog1.setMessage("wait...");
        progressDialog1.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ForgotpassResendResponse> call1 = apiInterface.ForgotpassResend(User_id, Mobile);
        call1.enqueue(new Callback<ForgotpassResendResponse>() {
            @Override
            public void onResponse(Call<ForgotpassResendResponse> call, Response<ForgotpassResendResponse> response) {
                if (response.isSuccessful()) ;
                ForgotpassResendResponse forgotpassResendResponse = response.body();
                if (forgotpassResendResponse.status.equals("1")) {
                    progressDialog1.dismiss();
                    Toast.makeText(ForgotOtp.this, forgotpassResendResponse.message, Toast.LENGTH_SHORT).show();
                }
                else if (forgotpassResendResponse.status.equals("0")) {
                    progressDialog1.dismiss();
                    Toast.makeText(ForgotOtp.this, forgotpassResendResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ForgotpassResendResponse> call, Throwable t) {
                progressDialog1.dismiss();
                Toast.makeText(ForgotOtp.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp = signup_otp.getText().toString();

                final ProgressDialog progressDialog = new ProgressDialog(ForgotOtp.this);
                progressDialog.setMessage("wait...");
                progressDialog.show();

                apiInterface1 = ApiClient.getClient().create(ApiInterface.class);
                Call<ForgotOtpResponse> call1 = apiInterface1.ForgotOtp(otp);
                call1.enqueue(new Callback<ForgotOtpResponse>() {
                    @Override
                    public void onResponse(Call<ForgotOtpResponse> call, Response<ForgotOtpResponse> response) {
                        if (response.isSuccessful()) ;
                        ForgotOtpResponse forgotOtpResponse = response.body();
                        if (forgotOtpResponse.status.equals("1")) {
                            progressDialog.dismiss();
                            u_id = forgotOtpResponse.data;
                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("Forgot", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("Fuser_id", u_id);
                            editor.commit();
                            Toast.makeText(ForgotOtp.this, forgotOtpResponse.message, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ForgotOtp.this, ForgotResetPassword.class));
                            finish();
                        }
                        else if (forgotOtpResponse.status.equals("0")) {
                            progressDialog.dismiss();
                            Toast.makeText(ForgotOtp.this, forgotOtpResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ForgotOtpResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(ForgotOtp.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

        });

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("ForgotMobile", Context.MODE_PRIVATE);
//                String mobile = sharedPreferences.getString("Formobile", "");
//
//                SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("Forgot", Context.MODE_PRIVATE);
//                String user_id = sharedPreferences1.getString("Fuser_id", "");

                final ProgressDialog progressDialog1 = new ProgressDialog(ForgotOtp.this);
                progressDialog1.setMessage("wait...");
                progressDialog1.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<ForgotpassResendResponse> call1 = apiInterface.ForgotpassResend(User_id, Mobile);
                call1.enqueue(new Callback<ForgotpassResendResponse>() {
                    @Override
                    public void onResponse(Call<ForgotpassResendResponse> call, Response<ForgotpassResendResponse> response) {
                        if (response.isSuccessful()) ;
                        ForgotpassResendResponse forgotpassResendResponse = response.body();
                        if (forgotpassResendResponse.status.equals("1")) {
                            progressDialog1.dismiss();
                            Toast.makeText(ForgotOtp.this, forgotpassResendResponse.message, Toast.LENGTH_SHORT).show();
                        }
                        else if (forgotpassResendResponse.status.equals("0")) {
                            progressDialog1.dismiss();
                            Toast.makeText(ForgotOtp.this, forgotpassResendResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ForgotpassResendResponse> call, Throwable t) {
                        progressDialog1.dismiss();
                        Toast.makeText(ForgotOtp.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
