package igrand.info.tech.Chotuonline.model;

public class MyOrder {

    private String date;
    private String id;
    private String price;
    private String items;

    public MyOrder(String date, String id, String price, String items) {
        this.date = date;
        this.id = id;
        this.price = price;
        this.items = items;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }
}
