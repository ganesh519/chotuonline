package igrand.info.tech.Chotuonline.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.FirebaseResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    public static final int SPALSH_TIME_OUT = 3000;
    Context ctx;
    Intent intent;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        String MobileId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        SharedPreferences sharedPreference =getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString("mobile_id", MobileId);
        editor.commit();

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences.getString("user_id", "");
        intent = new Intent(SplashActivity.this, MainActivity.class);

        if (TextUtils.isEmpty(user_id)) {
            intent = new Intent(SplashActivity.this, MainActivity.class);
        }
        initxml();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
            }
        },SPALSH_TIME_OUT);

        SharedPreferences sharedPreference1 = getApplicationContext().getSharedPreferences("Token", Context.MODE_PRIVATE);
        String token=sharedPreference1.getString("TOKEN","");
        String type="android";

        apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<FirebaseResponse> call=apiInterface.FirebaseResponse(type,token,MobileId);
        call.enqueue(new Callback<FirebaseResponse>() {
            @Override
            public void onResponse(Call<FirebaseResponse> call, Response<FirebaseResponse> response) {
                if (response.isSuccessful());
                FirebaseResponse firebaseResponse=response.body();
//                if (firebaseResponse.status.equals("1")){
//                    Toast.makeText(SplashActivity.this, firebaseResponse.message, Toast.LENGTH_SHORT).show();
//                }
//                else if (firebaseResponse.status.equals("0")){
//                    Toast.makeText(SplashActivity.this, firebaseResponse.message, Toast.LENGTH_SHORT).show();
//
//                }
            }

            @Override
            public void onFailure(Call<FirebaseResponse> call, Throwable t) {
                Toast.makeText(SplashActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void initxml() {
        ctx = this;
    }
}
