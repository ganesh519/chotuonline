package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.NotificationDataResponse;

public class NotificationRecyclerAdapter extends RecyclerView.Adapter<NotificationRecyclerAdapter.Holder> {
    List<NotificationDataResponse> notificationDataResponse;
    Context context;
    private int lastPosition = -1;

    public NotificationRecyclerAdapter(List<NotificationDataResponse> notificationDataResponse, Context context) {
        this.notificationDataResponse = notificationDataResponse;
        this.context = context;
    }

    @NonNull
    @Override
    public NotificationRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notifications, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationRecyclerAdapter.Holder holder, int position) {

        setAnimation(holder.itemView, position);
        holder.noti_date.setText(notificationDataResponse.get(position).dateTime);
        holder.text_name.setText(notificationDataResponse.get(position).name);
        holder.text_description.setText(notificationDataResponse.get(position).description);

    }

    @Override
    public int getItemCount() {
        return notificationDataResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView text_name, text_description, noti_date;

        public Holder(View itemView) {
            super(itemView);
            text_name = itemView.findViewById(R.id.text_name);
            text_description = itemView.findViewById(R.id.text_description);
            noti_date = itemView.findViewById(R.id.noti_date);

        }
    }

    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
