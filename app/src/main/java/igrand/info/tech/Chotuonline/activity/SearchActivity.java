package igrand.info.tech.Chotuonline.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.SearchAdapter;
import igrand.info.tech.Chotuonline.model.SearchDataResponse;
import igrand.info.tech.Chotuonline.model.SearchResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {
    View view;
    ImageView seacrh_iv, back;
    RecyclerView recyclerView;
    EditText input_et;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_search);
        initXml();
    }

    private void initXml() {
        seacrh_iv = findViewById(R.id.iv_search_icon);
        recyclerView = findViewById(R.id.search_recyclerview);
        input_et = findViewById(R.id.et_search_input);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        input_et.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                if (!s.toString().equals("")) {
                    getSearching(s.toString());
                }

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                listAdapter.getFilter().filter(s);

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


//        seacrh_iv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String text = input_et.getText().toString();
//                if (!text.equals("")){
//                    getSearching(text);
//                }
//
//
//            }
//        });
    }

    private void getSearching(String text) {

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences.getString("user_id", "");

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SearchResponse> call = apiInterface.Search(text, user_id);
        call.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                if (response.isSuccessful()) ;
                SearchResponse searchResponse = response.body();
                if (searchResponse.status.equals("1")) {
                    List<SearchDataResponse> searchDataResponse = searchResponse.data;
                    if (searchDataResponse != null) {
                        recyclerView.setAdapter(new SearchAdapter(searchDataResponse, getApplicationContext()));
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setNestedScrollingEnabled(false);
                    }
                    else if (searchResponse.status.equals("0")) {
                        Toast.makeText(getApplicationContext(), searchResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }
                else if (searchResponse.status.equals("0")){
                    Toast.makeText(SearchActivity.this, searchResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Toast.makeText(SearchActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
