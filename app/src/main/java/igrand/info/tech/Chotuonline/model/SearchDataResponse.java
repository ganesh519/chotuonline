package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class SearchDataResponse {

    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("product_brand")
    @Expose
    public String productBrand;
    @SerializedName("product_image")
    @Expose
    public String productImage;
    @SerializedName("quantity")
    @Expose
    public String quantity;
    @SerializedName("var_code")
    @Expose
    public String varCode;
    @SerializedName("units")
    @Expose
    public String units;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("variation_id")
    @Expose
    public String variationId;
    @SerializedName("start_limit")
    @Expose
    public String startLimit;
    @SerializedName("end_limit")
    @Expose
    public String endLimit;
    @SerializedName("no_of_items")
    @Expose
    public String noOfItems;
    @SerializedName("cart_type")
    @Expose
    public Integer cartType;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("vat")
    @Expose
    public String vat;
    @SerializedName("variation_code")
    @Expose
    public String variationCode;
    @SerializedName("variation_quantity")
    @Expose
    public String variationQuantity;
    @SerializedName("actual_price")
    @Expose
    public String actualPrice;
    @SerializedName("unit_price")
    @Expose
    public String unitPrice;
    @SerializedName("you_saved")
    @Expose
    public Float youSaved;
    @SerializedName("image1")
    @Expose
    public String image1;
    @SerializedName("image2")
    @Expose
    public String image2;
    @SerializedName("image3")
    @Expose
    public String image3;
    @SerializedName("image4")
    @Expose
    public String image4;
    @SerializedName("stock_left")
    @Expose
    public String stockLeft;
    @SerializedName("discount")
    @Expose
    public Integer discount;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productId", productId).append("productName", productName).append("productBrand", productBrand).append("productImage", productImage).append("quantity", quantity).append("varCode", varCode).append("units", units).append("price", price).append("variationId", variationId).append("startLimit", startLimit).append("endLimit", endLimit).append("noOfItems", noOfItems).append("cartType", cartType).append("description", description).append("vat", vat).append("variationCode", variationCode).append("variationQuantity", variationQuantity).append("actualPrice", actualPrice).append("unitPrice", unitPrice).append("youSaved", youSaved).append("image1", image1).append("image2", image2).append("image3", image3).append("image4", image4).append("stockLeft", stockLeft).append("discount", discount).toString();
    }
}
