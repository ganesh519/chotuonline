package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class PrivacyDataResponse {



    @SerializedName("page_name")
    @Expose
    public String pageName;
    @SerializedName("description")
    @Expose
    public String description;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("pageName", pageName).append("description", description).toString();
    }

}
