package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class CartViewDataResponse {

    @SerializedName("total_items")
    @Expose
    public Integer totalItems;
    @SerializedName("items_info")
    @Expose
    public List<CartViewItemsResponse> itemsInfo = null;
    @SerializedName("subtotal")
    @Expose
    public String subtotal;
    @SerializedName("total_save_amount")
    @Expose
    public String totalSaveAmount;
    @SerializedName("delivery_charges")
    @Expose
    public String deliveryCharges;
    @SerializedName("total_amount")
    @Expose
    public String totalAmount;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("totalItems", totalItems).append("itemsInfo", itemsInfo).append("subtotal", subtotal).append("totalSaveAmount", totalSaveAmount).append("deliveryCharges", deliveryCharges).append("totalAmount", totalAmount).toString();
    }
}
