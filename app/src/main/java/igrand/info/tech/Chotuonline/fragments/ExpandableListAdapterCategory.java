package igrand.info.tech.Chotuonline.fragments;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import java.util.HashMap;
import java.util.List;

import igrand.info.tech.Chotuonline.OnClickExpand;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.MainActivity;
import igrand.info.tech.Chotuonline.model.AllSubCategoryResponse;
import igrand.info.tech.Chotuonline.model.AllSubSubCategoryDataResponse;

public class ExpandableListAdapterCategory extends BaseExpandableListAdapter {

    private static final Object LayoutInflater ="LayoutInflater" ;
    private Context context;
    private List<String> expandableListTitle;
    private HashMap<String, List<AllSubCategoryResponse>> expandableListDetail;
    private OnClickExpand onClickExpand;

    public ExpandableListAdapterCategory(Context context, List<String> expandableListTitle,
                                         HashMap<String, List<AllSubCategoryResponse>> expandableListDetail,OnClickExpand onClickExpand) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
        this.onClickExpand = onClickExpand;
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {

        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).get(expandedListPosition);

    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {

        return expandedListPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
//        if (convertView == null) {
//
//
////            LayoutInflater layoutInflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////            View view = LayoutInflater(parent.getContext()).inflate(R.layout.row_about, parent, false);
////            convertView = layoutInflater.inflate(R.layout.list_groupcategory, null);
//        }


        if (convertView == null){
            convertView= android.view.LayoutInflater.from(parent.getContext()).inflate(R.layout.list_groupcategory,parent,false);

        }

        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.expandedListItem);
        listTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        listTitleTextView.setText(listTitle);
        convertView.invalidate();
        return convertView;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        //final String expandedListText = (String) getChild(listPosition, expandedListPosition);
        final AllSubCategoryResponse aaa = (AllSubCategoryResponse) getChild(listPosition, expandedListPosition);

        if (convertView == null) {
            convertView= android.view.LayoutInflater.from(parent.getContext()).inflate(R.layout.list_itemcategory,parent,false);

        }

        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.listTitle);
        ImageView img = (ImageView) convertView.findViewById(R.id.img_arrow);
        expandedListTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
        expandedListTextView.setText(aaa.subcategoryName);
        final LinearLayout mLinearLayout = convertView.findViewById(R.id.linear_expand);

        ListView list_expand = convertView.findViewById(R.id.list_expand);
        CustomAdapter aa = new CustomAdapter(context, aaa);
        list_expand.setAdapter(aa);
        if (aaa.subsubcategoryinfo.size() == 0) {
            mLinearLayout.setVisibility(View.GONE);
            img.setVisibility(View.GONE);
        }
        else {
            img.setVisibility(View.VISIBLE);
            //mLinearLayout.setVisibility(View.VISIBLE);

           /* if (mLinearLayout.getVisibility() == View.GONE) {
                mLinearLayout.setVisibility(View.VISIBLE);
            } else {
                mLinearLayout.setVisibility(View.GONE);
            }
*/

        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (aaa.subsubcategoryinfo.size() == 0) {
                    mLinearLayout.setVisibility(View.GONE);
                    onClickExpand.subItemSelected(aaa);
                }
                else {
                    if (mLinearLayout.getVisibility() == View.GONE) {
                        mLinearLayout.setVisibility(View.VISIBLE);
                    } else {
                        mLinearLayout.setVisibility(View.GONE);
//                        mLinearLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        convertView.invalidate();
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

    class CustomAdapter extends BaseAdapter {
        Context context;
        //List<AllSubSubCategoryDataResponse> rowItems;
        AllSubCategoryResponse allSubCategoryResponse;
        public CustomAdapter(Context context,  AllSubCategoryResponse items) {
            this.context = context;
            this.allSubCategoryResponse = items;
        }

        /*private view holder class*/
        private class ViewHolder {
            ImageView imageView;
            TextView txtTitle;
            TextView txtDesc;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
//            ViewHolder holder = null;
          //  if (convertView == null) {

        //    }

//            holder = new ViewHolder();

            if (convertView == null){
                convertView= android.view.LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);

            }
//            convertView.setTag(holder);
           TextView txtTitle = (TextView) convertView.findViewById(R.id.listTitle1);

//            else {
//                holder = (ViewHolder) convertView.getTag();
//            }

           // AllSubSubCategoryDataResponse rowItem = (AllSubSubCategoryDataResponse) getItem(position);
            final AllSubSubCategoryDataResponse rowItem=(AllSubSubCategoryDataResponse)allSubCategoryResponse.subsubcategoryinfo.get(position);
            Log.e("name ",""+rowItem.subsubcategoryName);
          txtTitle.setText(rowItem.subsubcategoryName);

            txtTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   onClickExpand.subsubItemSelected(allSubCategoryResponse,rowItem);
                }
            });
            convertView.invalidate();
            return convertView;
        }
        @Override
        public int getCount() {
            return allSubCategoryResponse.subsubcategoryinfo.size();
        }
        @Override
        public Object getItem(int position) {
            return allSubCategoryResponse.subsubcategoryinfo.get(position);
        }
        @Override
        public long getItemId(int position) {
            return allSubCategoryResponse.subsubcategoryinfo.indexOf(getItem(position));
        }
    }
}