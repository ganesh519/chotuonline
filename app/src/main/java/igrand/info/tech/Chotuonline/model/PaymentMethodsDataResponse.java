package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class PaymentMethodsDataResponse {

    @SerializedName("payment_id")
    @Expose
    public String paymentId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("dafault_status")
    @Expose
    public String dafaultStatus;
    @SerializedName("sort")
    @Expose
    public String sort;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("paymentId", paymentId).append("name", name).append("status", status).append("dafaultStatus", dafaultStatus).append("sort", sort).toString();
    }

}
