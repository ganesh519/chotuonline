package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.fragments.CategoryMainListFragment;
import igrand.info.tech.Chotuonline.model.AllCategoryResponse;



public class CategoryRecyclerAdapter extends RecyclerView.Adapter<CategoryRecyclerAdapter.Holder> {

    List<AllCategoryResponse> allCategoryResponse;
    Context context;

    public CategoryRecyclerAdapter(List<AllCategoryResponse> allCategoryResponse, Context context) {
        this.allCategoryResponse = allCategoryResponse;
        this.context = context;

    }

    @NonNull
    @Override
    public CategoryRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        view = inflater.inflate(R.layout.grid_layout, parent, false);
        Holder holder = new Holder(view, viewType);
        return holder;

    }

    @Override
    public void onBindViewHolder(@NonNull CategoryRecyclerAdapter.Holder holder, final int position) {
        Picasso.with(context).load(allCategoryResponse.get(position).image).into(holder.product_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cat_id = allCategoryResponse.get(position).id;
                String cat_name = allCategoryResponse.get(position).name;

                Bundle bundle=new Bundle();
                bundle.putString("category_id", cat_id);
                bundle.putString("subcategory_id", "");
                bundle.putString("category_name", cat_name);
                bundle.putString("all_category","gfgfg");
                bundle.putString("SubSubCategory","");

                AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
                CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
                appCompatActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fl_main_framelayout,categoryMainListFragment,"").addToBackStack("").commit();
                categoryMainListFragment.setArguments(bundle);
//                android.support.v4.app.FragmentTransaction fragmentTransaction = appCompatActivity.getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.fl_main_framelayout, categoryMainListFragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();

            }
        });

    }

    @Override
    public int getItemCount() {
        return allCategoryResponse.size();
    }


    class Holder extends RecyclerView.ViewHolder {
        ImageView product_image;

        public Holder(View itemView, int viewType) {

            super(itemView);
            product_image = (ImageView) itemView.findViewById(R.id.product_image);

        }
    }

}
