package igrand.info.tech.Chotuonline.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import igrand.info.tech.Chotuonline.R;

/**
 * Created by administator on 7/11/2018.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    // Different State For The Expandable List
    private final int[] EMPTY_STATE_SET = {};
    private final int[] GROUP_EXPANDED_STATE_SET = {android.R.attr.state_expanded};
    private final int[][] GROUP_STATE_SETS = {
            EMPTY_STATE_SET, // 0
            GROUP_EXPANDED_STATE_SET // 1
    };

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;


    ImageView imageView, imageView1, right;
//    private int[] images;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
//        this.images = images;
    }

    @Override
    public Object getChild(int groupgroupPosition, int childgroupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupgroupPosition))
                .get(childgroupPosition);
    }

    @Override
    public long getChildId(int groupgroupPosition, int childgroupPosition) {
        return childgroupPosition;
    }

    @Override
    public View getChildView(int groupgroupPosition, final int childgroupPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupgroupPosition, childgroupPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, null);

        }

        TextView txtListChild = convertView.findViewById(R.id.listTitle1);
        txtListChild.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupgroupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupgroupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupgroupPosition) {
        return this._listDataHeader.get(groupgroupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupgroupPosition) {
        return groupgroupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String headerTitle = (String) getGroup(groupPosition);

        LayoutInflater inflater = (LayoutInflater) this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.list_group, null);

        imageView = convertView.findViewById(R.id.remove);
        imageView1 = convertView.findViewById(R.id.add);
        right = convertView.findViewById(R.id.right);

        SharedPreferences sharedPreference =_context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreference.getString("user_id", "");

        if (TextUtils.isEmpty(user_id)){
            if (groupPosition == 1){
                right.setVisibility(View.VISIBLE);

            }

        }
        else {

            if (groupPosition == 1) {
                if (_listDataHeader.get(groupPosition).equals("My Accounts")) {
                    if (isExpanded) {
                        imageView1.setVisibility(View.GONE);
                        imageView.setVisibility(View.VISIBLE);


                    } else {
                        imageView1.setVisibility(View.VISIBLE);
                        imageView.setVisibility(View.GONE);
                    }
                }
            }

            if (groupPosition == 2) {


                right.setVisibility(View.VISIBLE);
            }
        }

        // Checking if the parent is not having any child, hiding the drop-down arrow
//        ImageView img = convertView.findViewById(R.id.explist_indicator);
//        img.setImageResource(images[groupgroupPosition]);

        TextView lblListHeader = convertView
                .findViewById(R.id.expandedListItem);
        lblListHeader.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childgroupPosition) {
        return true;
    }
}


