package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.CustomSpinnerAdapter1;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.MainActivity;
import igrand.info.tech.Chotuonline.activity.ProductDetailsActivity;
import igrand.info.tech.Chotuonline.model.AddCartResponse;
import igrand.info.tech.Chotuonline.model.Category_spinner;
import igrand.info.tech.Chotuonline.model.Category_variationResponse;
import igrand.info.tech.Chotuonline.model.SubSubCategoryDataResponse;
import igrand.info.tech.Chotuonline.model.SubSubCategoryVariationResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubCategoryMainListAdapter  extends RecyclerView.Adapter<SubCategoryMainListAdapter.Holder> {

    List<SubSubCategoryDataResponse> subSubCategoryDataResponse;
    Context context;
    ApiInterface apiInterface1;
    String userid;
    int counter = 1;
    String variation_id, startlimit, endlimit;

    List<Category_spinner> category_spinner;
    List<Category_spinner> category_spinner1;
    String selectedItem;
    List<SubSubCategoryVariationResponse> subSubCategoryVariationResponse;
    List<Category_variationResponse> category_variationResponsessds;
    CustomSpinnerAdapter1 customSpinnerAdapter;
    ListView alert_listview;
    String mVeriationId="";
    String quantity,operator;
    int cout;
    private int lastPosition = -1;

    public SubCategoryMainListAdapter(List<SubSubCategoryDataResponse> SubSubCategoryDataResponse, Context context, String userid) {
        this.subSubCategoryDataResponse = SubSubCategoryDataResponse;
        this.context = context;
        this.userid = userid;

    }

    @NonNull
    @Override
    public SubCategoryMainListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_shampoo, parent, false);

        return new SubCategoryMainListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SubCategoryMainListAdapter.Holder holder, final int position) {

        setAnimation(holder.itemView,position);
        holder.title.setText(subSubCategoryDataResponse.get(position).brand);
        holder.name.setText(subSubCategoryDataResponse.get(position).productName);

        subSubCategoryVariationResponse = subSubCategoryDataResponse.get(position).variationInfo;
        if (subSubCategoryVariationResponse != null) {
            subSubCategoryDataResponse.get(position).mVeriationId = subSubCategoryVariationResponse.get(0).variationId;

            category_spinner = new ArrayList<>();
            holder.dprice.setText("₹ " + subSubCategoryVariationResponse.get(0).actualPrice);
            holder.dprice.setPaintFlags(holder.dprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.price.setText("₹ " + subSubCategoryVariationResponse.get(0).unitPrice);
            holder.off.setText(String.valueOf(subSubCategoryVariationResponse.get(0).discount) + "%" + "\n" + "OFF");
            Picasso.with(context).load(subSubCategoryVariationResponse.get(0).image1).into(holder.image);
            holder.alert_text.setText(subSubCategoryVariationResponse.get(0).quantity+"₹"+subSubCategoryVariationResponse.get(0).unitPrice);
        }
        holder.alert_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View view = LayoutInflater.from(context).inflate(R.layout.alert_productlisting, null);
                alert_listview = view.findViewById(R.id.alert_listview);
                customSpinnerAdapter = new CustomSpinnerAdapter1(context, android.R.layout.simple_list_item_1, category_spinner,holder.alert_text.getText().toString());
                TextView productname = view.findViewById(R.id.productname);
                productname.setText(subSubCategoryDataResponse.get(position).productName);

                alert_listview.setAdapter(customSpinnerAdapter);
                builder.setView(view);
                final AlertDialog b = builder.create();
                b.show();
                category_spinner.clear();

                subSubCategoryVariationResponse = subSubCategoryDataResponse.get(position).variationInfo;

                for (int j = 0; j < subSubCategoryVariationResponse.size(); j++) {
                    final Category_spinner area = new Category_spinner();
                    area.setId(subSubCategoryVariationResponse.get(j).variationId);
                    area.setQuantiti(subSubCategoryVariationResponse.get(j).quantity);
                    area.setPricee("" + "₹" + subSubCategoryVariationResponse.get(j).unitPrice);
                    area.setDprice("₹ " + subSubCategoryVariationResponse.get(j).actualPrice);
                    area.setOff(Float.valueOf(subSubCategoryVariationResponse.get(j).discount) + "%" + "\n" + "OFF");
                    area.setImage(subSubCategoryVariationResponse.get(j).image1);
                    category_spinner.add(area);


                }

                alert_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

                        view.setSelected(true);
                        holder.dprice.setText(category_spinner.get(i).getDprice());
                        holder.dprice.setPaintFlags(holder.dprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        holder.price.setText(category_spinner.get(i).getPricee());
                        holder.off.setText(category_spinner.get(i).getOff());
                        Picasso.with(context).load(category_spinner.get(i).getImage()).into(holder.image);
                        holder.alert_text.setText(category_spinner.get(i).getQuantiti()+category_spinner.get(i).getPricee());

                        variation_id = subSubCategoryVariationResponse.get(i).variationId;
                        quantity = "1";
                        operator = "plus";
                        startlimit = subSubCategoryVariationResponse.get(i).startLimit;
                        endlimit = subSubCategoryVariationResponse.get(i).endLimit;
                        subSubCategoryDataResponse.get(position).first = true;

                        subSubCategoryDataResponse.get(position).mVeriationId=subSubCategoryVariationResponse.get(i).variationId;

                        String ctn=subSubCategoryVariationResponse.get(position).noOfItems;
                        if (ctn.equals("0")){
                            holder.linear_add.setVisibility(View.VISIBLE);
                            holder.Relative_increase.setVisibility(View.GONE);
                        }
                        else {

                            holder.linear_add.setVisibility(View.GONE);
                            holder.Relative_increase.setVisibility(View.VISIBLE);
                            holder.show_value.setText(ctn);
                        }

                        b.dismiss();
                    }
                });

            }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<SubSubCategoryVariationResponse> subsubresp = subSubCategoryDataResponse.get(position).variationInfo;
                for (int i = 0; i<subsubresp.size(); i++){

                    subSubCategoryDataResponse.get(position).startLmt = subsubresp.get(i).startLimit;
                    subSubCategoryDataResponse.get(position).endLmt = subsubresp.get(i).endLimit;
                    subSubCategoryDataResponse.get(position).mVeriationId = subsubresp.get(i).variationId;

                }



                String vid = subSubCategoryDataResponse.get(position).mVeriationId;
//                SharedPreferences sharedPreferences = context.getSharedPreferences("Variation", Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedPreferences.edit();
//                editor.putString("v_id", vid);
//                 editor.commit();

                String startlimit=subSubCategoryDataResponse.get(position).startLmt;
                String endlimit=subSubCategoryDataResponse.get(position).endLmt;
                SharedPreferences sharedPreference = context.getSharedPreferences("product", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor1=sharedPreference.edit();
                editor1.putString("variation_id",vid);
                editor1.putString("StartLimit",startlimit);
                editor1.putString("EndLimit",endlimit);
                editor1.commit();

                Intent intent = new Intent(context, ProductDetailsActivity.class);
                intent.putExtra("variation","1234567");
                intent.putExtra("v_id", vid);
                context.startActivity(intent);

            }
        });


        if (!subSubCategoryDataResponse.get(position).isAdded) {
            holder.linear_add.setVisibility(View.VISIBLE);
            holder.Relative_increase.setVisibility(View.GONE);
        } else {
            holder.linear_add.setVisibility(View.GONE);
            holder.Relative_increase.setVisibility(View.VISIBLE);
        }


        try {
            if (subSubCategoryDataResponse.get(position).variationInfo != null) {
                if (subSubCategoryVariationResponse.get(0).noOfItems.equals("0") || subSubCategoryVariationResponse.get(0).noOfItems.equals("")) {
                    holder.linear_add.setVisibility(View.VISIBLE);
                    holder.Relative_increase.setVisibility(View.GONE);
                }
                else {
                    holder.linear_add.setVisibility(View.GONE);
                    holder.Relative_increase.setVisibility(View.VISIBLE);
                    holder.show_value.setText(subSubCategoryVariationResponse.get(0).noOfItems);
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        holder.linear_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!subSubCategoryDataResponse.get(position).first){
                    subSubCategoryVariationResponse = subSubCategoryDataResponse.get(position).variationInfo;
                    for (int j = 0; j < subSubCategoryVariationResponse.size(); j++) {
                        variation_id = subSubCategoryVariationResponse.get(0).variationId;
                        quantity = "1";
                        operator = "plus";
                        startlimit = subSubCategoryVariationResponse.get(0).startLimit;
                        endlimit = subSubCategoryVariationResponse.get(0).endLimit;
                    }
                }
                holder.progressbar_add.setVisibility(View.VISIBLE);
                SharedPreferences sharedPreferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");
                SharedPreferences sharedPreference =context.getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
                String device = sharedPreference.getString("mobile_id","");

                apiInterface1 = ApiClient.getClient().create(ApiInterface.class);
                Call<AddCartResponse> cartResponseCall = apiInterface1.AddCart(user_id,variation_id,quantity,
                        startlimit, endlimit,operator,device);
                cartResponseCall.enqueue(new Callback<AddCartResponse>() {
                    @Override
                    public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                        if (response.isSuccessful()) ;
                        AddCartResponse addCartResponse = response.body();
                        if (addCartResponse.status.equals("1")) {
                            subSubCategoryDataResponse.get(position).isAdded=true;
                            holder.linear_add.setVisibility(View.GONE);
                            holder.progressbar_add.setVisibility(View.GONE);
                            holder.Relative_increase.setVisibility(View.VISIBLE);
                            MainActivity.CartCount();
                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();


                        } else if (addCartResponse.status.equals("0")) {
                            holder.progressbar_add.setVisibility(View.GONE);
                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddCartResponse> call, Throwable t) {
                        holder.progressbar_add.setVisibility(View.GONE);
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
                holder.linear_cart.setVisibility(View.VISIBLE);
            }
        });


        holder.decreas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.progressbar_add_increase.setVisibility(View.VISIBLE);
                SharedPreferences sharedPreferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");
                cout = (Integer.parseInt(holder.show_value.getText().toString().trim()));
                if (!subSubCategoryDataResponse.get(position).first) {
                    subSubCategoryVariationResponse = subSubCategoryDataResponse.get(position).variationInfo;
                    for (int j = 0; j < subSubCategoryVariationResponse.size(); j++) {
                        variation_id = subSubCategoryVariationResponse.get(0).variationId;
                        quantity = "1";
                        operator = "minus";
                        startlimit = subSubCategoryVariationResponse.get(0).startLimit;
                        endlimit = subSubCategoryVariationResponse.get(0).endLimit;
                    }
                }
                SharedPreferences sharedPreference =context.getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
                String device = sharedPreference.getString("mobile_id","");
                apiInterface1 = ApiClient.getClient().create(ApiInterface.class);
                Call<AddCartResponse> cartResponseCall = apiInterface1.AddCart(user_id, variation_id, quantity, startlimit, endlimit, operator,device);
                cartResponseCall.enqueue(new Callback<AddCartResponse>() {
                    @Override
                    public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                        if (response.isSuccessful()) ;
                        AddCartResponse addCartResponse = response.body();
                        if (addCartResponse.status.equals("1")) {
                            int Dec_qun = addCartResponse.noOfItems;
                            holder.show_value.setText(Integer.toString(Dec_qun));
                            subSubCategoryDataResponse.get(position).isAdded = true;
                            MainActivity.CartCount();
                            if (Dec_qun < 1) {
                                holder.linear_add.setVisibility(View.VISIBLE);
                                holder.linear_cart.setVisibility(View.GONE);
                                subSubCategoryDataResponse.get(position).isAdded = false;
                            }
                            holder.progressbar_add_increase.setVisibility(View.GONE);
//                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                        else if (addCartResponse.status.equals("0")) {
                            holder.progressbar_add_increase.setVisibility(View.GONE);
                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<AddCartResponse> call, Throwable t) {
                        holder.progressbar_add_increase.setVisibility(View.GONE);
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });


        holder.increas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");
                holder.progressbar_add_increase.setVisibility(View.VISIBLE);
                if (!subSubCategoryDataResponse.get(position).first) {
                    subSubCategoryVariationResponse = subSubCategoryDataResponse.get(position).variationInfo;
                    for (int j = 0; j < subSubCategoryVariationResponse.size(); j++) {
                        variation_id = subSubCategoryVariationResponse.get(0).variationId;
                        quantity = "1";
                        operator = "minus";
                        startlimit = subSubCategoryVariationResponse.get(0).startLimit;
                        endlimit = subSubCategoryVariationResponse.get(0).endLimit;
                    }
                }
                SharedPreferences sharedPreference =context.getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
                String device = sharedPreference.getString("mobile_id","");
                apiInterface1 = ApiClient.getClient().create(ApiInterface.class);
                Call<AddCartResponse> cartResponseCall = apiInterface1.AddCart(user_id, variation_id, quantity,
                        startlimit, endlimit, operator,device);
                cartResponseCall.enqueue(new Callback<AddCartResponse>() {
                    @Override
                    public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                        if (response.isSuccessful()) ;
                        AddCartResponse addCartResponse = response.body();
                        if (addCartResponse.status.equals("1")) {
                            int Dec_qun = addCartResponse.noOfItems;
                            holder.show_value.setText(Integer.toString(Dec_qun));
                            holder.progressbar_add_increase.setVisibility(View.GONE);
//                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        } else if (addCartResponse.status.equals("0")) {
                            holder.progressbar_add_increase.setVisibility(View.GONE);
                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddCartResponse> call, Throwable t) {
                        holder.progressbar_add_increase.setVisibility(View.GONE);
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return subSubCategoryDataResponse.size();
    }


    class Holder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title, name, price, dprice, off;
        LinearLayout linear_cart;
        RelativeLayout linear_add, Relative_increase;
        TextView show_value;
        ImageView increas, decreas;

        RelativeLayout alert_relative;
        TextView alert_text;
        ProgressBar progressbar_add, progressbar_add_increase;

        public Holder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            dprice = itemView.findViewById(R.id.dprice);
            off = itemView.findViewById(R.id.off);

            alert_text = itemView.findViewById(R.id.alert_text);
            alert_relative = itemView.findViewById(R.id.alert_relative);


            show_value = itemView.findViewById(R.id.show_value);
            decreas = itemView.findViewById(R.id.decreas);
            increas = itemView.findViewById(R.id.increas);
            linear_add = itemView.findViewById(R.id.linear_add);
            linear_cart = itemView.findViewById(R.id.linear_cart);
            Relative_increase=itemView.findViewById(R.id.Relative_increase);

            progressbar_add_increase = itemView.findViewById(R.id.progressbar_add_increase);
            progressbar_add = itemView.findViewById(R.id.progressbar_add);
        }
    }
    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
