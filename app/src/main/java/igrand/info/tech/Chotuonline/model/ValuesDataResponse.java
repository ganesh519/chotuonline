package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class ValuesDataResponse {


    @SerializedName("address_type")
    @Expose
    public List<AddressTypeResponse> addressType = null;
    @SerializedName("state")
    @Expose
    public List<StateResponse> state = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addressType", addressType).append("state", state).toString();
    }
}
