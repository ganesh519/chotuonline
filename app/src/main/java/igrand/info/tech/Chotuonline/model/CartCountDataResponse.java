package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CartCountDataResponse {


    @SerializedName("count")
    @Expose
    public Integer count;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("count", count).toString();
    }
}
