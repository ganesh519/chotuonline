package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.ForgotpasswordResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends AppCompatActivity {

    ImageView iv_myaccount_back;
    Button forgot_bt;
    TextView textView;
    EditText et_changemobile_number;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        et_changemobile_number = findViewById(R.id.et_changemobile_number);

        iv_myaccount_back = findViewById(R.id.back);
        iv_myaccount_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        forgot_bt = findViewById(R.id.bt_changepass_update);
        forgot_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPassword();
            }
        });
    }

    public void getPassword() {

        String mobile = et_changemobile_number.getText().toString().trim();
        if (mobile.equals("")) {
            Toast.makeText(this, "Enter Registered Mobile Number or Email address", Toast.LENGTH_SHORT).show();
        }
        else {
            final ProgressDialog progressDialog = new ProgressDialog(ForgotPassword.this);
            progressDialog.setMessage("wait...");
            progressDialog.show();

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ForgotpasswordResponse> call = apiInterface.ForgotPassword(mobile);
            call.enqueue(new Callback<ForgotpasswordResponse>() {
                @Override
                public void onResponse(Call<ForgotpasswordResponse> call, Response<ForgotpasswordResponse> response) {
                    if (response.isSuccessful()) ;
                    ForgotpasswordResponse forgotpasswordResponse = response.body();
                    if (forgotpasswordResponse.status.equals("1")) {
                        String phone = forgotpasswordResponse.data;
                        String u_id=forgotpasswordResponse.id;
//                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("ForgotMobile", Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = sharedPreferences.edit();
//                        editor.putString("Formobile", phone);
//                        editor.commit();
//                        Toast.makeText(ForgotPassword.this, forgotpasswordResponse.message, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ForgotPassword.this, ForgotOtp.class);
                        intent.putExtra("Formobile",phone);
                        intent.putExtra("FUser_id",u_id);
                        startActivity(intent);
                        et_changemobile_number.setText("");
                        progressDialog.dismiss();
                    }
                    else if (forgotpasswordResponse.status.equals("0")) {
                        Toast.makeText(ForgotPassword.this, forgotpasswordResponse.message, Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ForgotpasswordResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(ForgotPassword.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
