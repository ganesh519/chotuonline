package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.ProductDetailsActivity;
import igrand.info.tech.Chotuonline.fragments.CategoryMainListFragment;
import igrand.info.tech.Chotuonline.model.DetailsResponse;
import igrand.info.tech.Chotuonline.model.HomeProductimageResponse;
import igrand.info.tech.Chotuonline.model.UpSliderResponse;



public class SliderAdapter2 extends PagerAdapter {
    Context context;
    List<UpSliderResponse> upSliderResponse;
    CategoryMainListFragment fragment;
    FragmentTransaction fragmentTransaction;


    public SliderAdapter2(List<UpSliderResponse> upSliderResponse, Context context) {
        this.context = context;
        this.upSliderResponse = upSliderResponse;

    }

    @Override
    public int getCount() {
        return upSliderResponse.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slider, container, false);
        ImageView myImage = view.findViewById(R.id.image1);
        Picasso.with(context).load(upSliderResponse.get(position).image).into(myImage);
        container.addView(view);

        myImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String category_id = upSliderResponse.get(position).catId;
                String type = upSliderResponse.get(position).linkType;
                String p_id = upSliderResponse.get(position).linkId;
                String category_name = upSliderResponse.get(position).linkName;
                String sub_sub_categoryid = upSliderResponse.get(position).subcatId;

                final List<DetailsResponse> detailsResponse = upSliderResponse.get(position).productDetails;
                List<HomeProductimageResponse> homeProductimageResponse = detailsResponse.get(position).productImages;

                if (type.equals("Sub Category")) {
                    if (p_id.equals("0")) {
                        Toast.makeText(context, "Their is no Products", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Bundle bundle = new Bundle();
                        bundle.putString("category_id", category_id);
                        bundle.putString("subcategory_id", p_id);
                        bundle.putString("category_name", category_name);
                        bundle.putString("menuList", "data");
                        bundle.putString("all_category", "Allcategory");

                        AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
                        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
                        appCompatActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fl_main_framelayout, categoryMainListFragment, "").addToBackStack("").commit();
                        categoryMainListFragment.setArguments(bundle);

                    }
                }
                else if (type.equals("Sub Sub Category")) {
                    if (p_id.equals("0")) {
                        Toast.makeText(context, "Their is no Products", Toast.LENGTH_SHORT).show();
                    }
                    else {
//                        Bundle bundle = new Bundle();
//                        bundle.putString("category_id", category_id);
//                        bundle.putString("subcategory_id", p_id);
//                        bundle.putString("sub_sub_id", sub_sub_categoryid);
//                        bundle.putString("category_name", category_name);
//                        bundle.putString("all_category", "");
//                        bundle.putString("menuList", "data");
//                        bundle.putString("SubSubCategory", "subsubcategory");
//
//                        AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
//                        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
//                        appCompatActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fl_main_framelayout, categoryMainListFragment, "").addToBackStack("").commit();
//                        categoryMainListFragment.setArguments(bundle);

                    }
                }
                else if (type.equals("Category")) {

                    if (p_id.equals("0")) {
                        Toast.makeText(context, "Their is no Products", Toast.LENGTH_SHORT).show();

                    }
                    else {
                        Bundle bundle = new Bundle();
                        bundle.putString("category_id", p_id);
                        bundle.putString("subcategory_id", "");
                        bundle.putString("category_name", category_name);
                        bundle.putString("all_category", "gfgfg");
                        bundle.putString("SubSubCategory", "");

                        AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
                        CategoryMainListFragment categoryMainListFragment = new CategoryMainListFragment();
                        appCompatActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fl_main_framelayout, categoryMainListFragment, "").addToBackStack("").commit();
                        categoryMainListFragment.setArguments(bundle);
                    }
                }
                if (!detailsResponse.equals("")) {
                    for (int i = 0; i < detailsResponse.size(); i++) {
                        String vid = detailsResponse.get(i).variationId;
                        String disc = String.valueOf(detailsResponse.get(i).discount);
                        String startlim = detailsResponse.get(i).startLimit;
                        String endlim = detailsResponse.get(i).endLimit;
                        String quant = detailsResponse.get(i).quantity;
                        String Yousav = String.valueOf(detailsResponse.get(i).youSaved);
                        String UnitPrice = detailsResponse.get(i).unitPrice;
                        String actualPrice = detailsResponse.get(i).actualPrice;
                        String noofitems = String.valueOf(detailsResponse.get(i).noOfItems);

                        if (type.equals("Product")) {

                            if (p_id.equals("0")) {
                                Toast.makeText(context, "Their is no Products", Toast.LENGTH_SHORT).show();

                            }
                            else {
                                SharedPreferences sharedPreferences = context.getSharedPreferences("product", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("p_id", p_id);
                                editor.commit();
                                Intent intent = new Intent(context, ProductDetailsActivity.class);
                                intent.putExtra("variation", "5678");
                                intent.putExtra("p_id", p_id);
                                intent.putExtra("variation_id", vid);
                                intent.putExtra("Quantity", quant);
                                intent.putExtra("disc", disc);
                                intent.putExtra("StartLimit", startlim);
                                intent.putExtra("endliMIT", endlim);
                                intent.putExtra("sav", Yousav);
                                intent.putExtra("UnitPrice", UnitPrice);
                                intent.putExtra("imgage", (Serializable) homeProductimageResponse);
                                intent.putExtra("actualPrice", actualPrice);
                                intent.putExtra("Noofitems", noofitems);
                                context.startActivity(intent);
                            }
                        }
                    }
                }
            }
        });

        return view;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
