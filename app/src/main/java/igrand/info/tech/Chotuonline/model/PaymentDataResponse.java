package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class PaymentDataResponse {

    @SerializedName("wallet_balance")
    @Expose
    public String walletBalance;
    @SerializedName("total_amount")
    @Expose
    public Float totalAmount;
    @SerializedName("total_payable")
    @Expose
    public Float totalPayable;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("walletBalance", walletBalance).append("totalAmount", totalAmount).append("totalPayable", totalPayable).toString();
    }


}
