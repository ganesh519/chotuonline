package igrand.info.tech.Chotuonline.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.Cancel_Order;
import igrand.info.tech.Chotuonline.localdatabase.CancelOrderDataResponse;
import igrand.info.tech.Chotuonline.model.SingleCancelOrderResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CancelOrderAdapter extends RecyclerView.Adapter<CancelOrderAdapter.Holder> {

    List<CancelOrderDataResponse> cancelOrderDataResponse;
    Context context;
    ApiInterface apiInterface;
    private int lastPosition = -1;

    public CancelOrderAdapter(List<CancelOrderDataResponse> cancelOrderDataResponse, Context context) {
        this.cancelOrderDataResponse = cancelOrderDataResponse;
        this.context = context;
    }

    @NonNull
    @Override
    public CancelOrderAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_order, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CancelOrderAdapter.Holder holder, int position) {

        setAnimation(holder.itemView,position);
        holder.date.setText(cancelOrderDataResponse.get(position).orderDatetime);
        holder.id.setText(cancelOrderDataResponse.get(position).orderNumber);
        holder.p_price.setText("Rs " + cancelOrderDataResponse.get(position).totalAmount);
        holder.status.setText(cancelOrderDataResponse.get(position).orderStatus);
        holder.items.setText(String.valueOf(cancelOrderDataResponse.get(position).noOfItems + "items"));
        final String order_id=cancelOrderDataResponse.get(position).orderId;


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                alertDialog.setTitle("Cancel Order");
                alertDialog.setMessage("Are you sure you want to cancel this Order?");
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SharedPreferences sharedPreferences = v.getContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                        String user_id = sharedPreferences.getString("user_id", "");

                        final ProgressDialog progressDialog = new ProgressDialog(v.getContext());
                        progressDialog.setMessage("wait...");
                        progressDialog.show();

                        apiInterface=ApiClient.getClient().create(ApiInterface.class);
                        Call<SingleCancelOrderResponse> cancelOrderResponseCall=apiInterface.SingleOrderCancel(user_id,order_id);
                        cancelOrderResponseCall.enqueue(new Callback<SingleCancelOrderResponse>() {
                            @Override
                            public void onResponse(Call<SingleCancelOrderResponse> call, Response<SingleCancelOrderResponse> response) {
                                if (response.isSuccessful());
                                SingleCancelOrderResponse singleCancelOrderResponse=response.body();
                                if (singleCancelOrderResponse.status.equals("1")){
                                    progressDialog.dismiss();
                                    Toast.makeText(v.getContext(), singleCancelOrderResponse.message, Toast.LENGTH_SHORT).show();
                                    ((Cancel_Order)v.getContext()).init();
                                }
                                else if (singleCancelOrderResponse.status.equals("0")){
                                    progressDialog.dismiss();
                                    Toast.makeText(v.getContext(), singleCancelOrderResponse.message, Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<SingleCancelOrderResponse> call, Throwable t) {
                                progressDialog.dismiss();
                                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                            }
                        });
                    }
                });
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                alertDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cancelOrderDataResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder{

        TextView date, id, p_price, items, status;

        public Holder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            id = itemView.findViewById(R.id.id);
            p_price = itemView.findViewById(R.id.p_price);
            status = itemView.findViewById(R.id.status);
            items = itemView.findViewById(R.id.items);
        }

    }
    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
