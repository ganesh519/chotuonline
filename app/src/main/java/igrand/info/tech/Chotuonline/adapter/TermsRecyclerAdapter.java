package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.TermsDataResponse;

public class TermsRecyclerAdapter extends RecyclerView.Adapter<TermsRecyclerAdapter.Holder> {
    List<TermsDataResponse> termsDataResponse;
    Context context;
    private int lastPosition = -1;

    public TermsRecyclerAdapter(List<TermsDataResponse> termsDataResponse, Context ctx) {
        this.termsDataResponse=termsDataResponse;
        this.context=ctx;
    }

    @NonNull
    @Override
    public TermsRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_terms, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TermsRecyclerAdapter.Holder holder, int position) {
        setAnimation(holder.itemView,position);
        holder.text_terms.setText(termsDataResponse.get(position).description);
        holder.text_terms_name.setText(termsDataResponse.get(position).name);
//        holder.text_terms.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);

    }

    @Override
    public int getItemCount() {
        return termsDataResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView text_terms,text_terms_name;

        public Holder(View itemView) {
            super(itemView);
            text_terms=itemView.findViewById(R.id.text_terms);
            text_terms_name=itemView.findViewById(R.id.text_terms_name);
        }
    }

    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
