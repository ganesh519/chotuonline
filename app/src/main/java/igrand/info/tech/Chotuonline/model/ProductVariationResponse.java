package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class ProductVariationResponse {
    @SerializedName("variation_id")
    @Expose
    public String variationId;
    @SerializedName("variation_code")
    @Expose
    public String variationCode;
    @SerializedName("quantity")
    @Expose
    public String quantity;
    @SerializedName("actual_price")
    @Expose
    public String actualPrice;
    @SerializedName("unit_price")
    @Expose
    public String unitPrice;
    @SerializedName("you_saved")
    @Expose
    public Integer youSaved;
    @SerializedName("stock_left")
    @Expose
    public String stockLeft;
    @SerializedName("discount")
    @Expose
    public Integer discount;
    @SerializedName("start_limit")
    @Expose
    public String startLimit;
    @SerializedName("end_limit")
    @Expose
    public String endLimit;
    @SerializedName("no_of_items")
    @Expose
    public String noOfItems;
    @SerializedName("cart_type")
    @Expose
    public Integer cartType;
    @SerializedName("product_images")
    @Expose
    public List<HomeProductimageResponse> productImages = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("variationId", variationId).append("variationCode", variationCode).append("quantity", quantity).append("actualPrice", actualPrice).append("unitPrice", unitPrice).append("youSaved", youSaved).append("stockLeft", stockLeft).append("discount", discount).append("startLimit", startLimit).append("endLimit", endLimit).append("noOfItems", noOfItems).append("cartType", cartType).append("productImages", productImages).toString();
    }

}
