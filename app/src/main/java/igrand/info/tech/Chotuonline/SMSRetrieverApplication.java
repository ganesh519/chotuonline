package igrand.info.tech.Chotuonline;

import android.app.Application;

public class SMSRetrieverApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AppSignatureHelper appSignatureHelper=new AppSignatureHelper(this);
        appSignatureHelper.getAppSignatures();
    }
}
