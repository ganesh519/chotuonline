package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class LoginDataResponse {


    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("user_email")
    @Expose
    public String userEmail;
    @SerializedName("user_first_name")
    @Expose
    public String userFirstName;
    @SerializedName("user_last_name")
    @Expose
    public String userLastName;
    @SerializedName("user_phone_number")
    @Expose
    public String userPhoneNumber;
    @SerializedName("user_type")
    @Expose
    public String userType;
    @SerializedName("user_photo")
    @Expose
    public String userPhoto;
    @SerializedName("user_alt_number")
    @Expose
    public String userAltNumber;
    @SerializedName("user_date_of_birth")
    @Expose
    public String userDateOfBirth;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("userEmail", userEmail).append("userFirstName", userFirstName).append("userLastName", userLastName).append("userPhoneNumber", userPhoneNumber).append("userType", userType).append("userPhoto", userPhoto).append("userAltNumber", userAltNumber).append("userDateOfBirth", userDateOfBirth).toString();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getUserAltNumber() {
        return userAltNumber;
    }

    public void setUserAltNumber(String userAltNumber) {
        this.userAltNumber = userAltNumber;
    }

    public String getUserDateOfBirth() {
        return userDateOfBirth;
    }

    public void setUserDateOfBirth(String userDateOfBirth) {
        this.userDateOfBirth = userDateOfBirth;
    }
}
