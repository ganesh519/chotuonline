package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AreaDataResponse {

    @SerializedName("area_id")
    @Expose
    public String areaId;
    @SerializedName("area_name")
    @Expose
    public String areaName;
    @SerializedName("area_pincode")
    @Expose
    public String areaPincode;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("areaId", areaId).append("areaName", areaName).append("areaPincode", areaPincode).toString();
    }


}
