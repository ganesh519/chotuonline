package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class UpSliderResponse {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("link")
    @Expose
    public String link;
    @SerializedName("sort_order")
    @Expose
    public String sortOrder;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("link_type")
    @Expose
    public String linkType;
    @SerializedName("link_id")
    @Expose
    public String linkId;
    @SerializedName("link_name")
    @Expose
    public String linkName;
    @SerializedName("cat_id")
    @Expose
    public String catId;
    @SerializedName("subcat_id")
    @Expose
    public String subcatId;
    @SerializedName("product_details")
    @Expose
    public List<DetailsResponse> productDetails = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("image", image).append("link", link).append("sortOrder", sortOrder).append("status", status).append("linkType", linkType).append("linkId", linkId).append("linkName", linkName).append("catId", catId).append("productDetails", productDetails).toString();
    }

}
