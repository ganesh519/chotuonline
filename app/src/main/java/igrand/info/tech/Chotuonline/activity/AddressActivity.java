package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.CommonClass;
import igrand.info.tech.Chotuonline.CustomSpinnerAdapter;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.AddAddressResponse;
import igrand.info.tech.Chotuonline.model.AddressTypeResponse;
import igrand.info.tech.Chotuonline.model.AreaDataResponse;
import igrand.info.tech.Chotuonline.model.AreaResponse;
import igrand.info.tech.Chotuonline.model.Category_spinner;
import igrand.info.tech.Chotuonline.model.CityDataResponse;
import igrand.info.tech.Chotuonline.model.CityResponse;
import igrand.info.tech.Chotuonline.model.StateResponse;
import igrand.info.tech.Chotuonline.model.ValuesDataResponse;
import igrand.info.tech.Chotuonline.model.ValuesResponse;
import igrand.info.tech.Chotuonline.util.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressActivity extends AppCompatActivity {

    ImageView back_iv;
    ApiInterface apiInterface, apiInterface1;
    Button submit;
    EditText Houseno, resdential, landmark;
    CheckBox checkbox;
    String house, resdent, land;
    Spinner addressType, state, city, area;
    int type;
    String address_id, state_id, city_id;
    String area_isd="";
    List<StateResponse> stateResponse;
    private boolean isConnected;
    LinearLayout linear_add;
    CustomSpinnerAdapter customSpinnerAdapter1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_activity);
        linear_add = findViewById(R.id.linear_add);
        Houseno = findViewById(R.id.Houseno);
        resdential = findViewById(R.id.resdential);
        landmark = findViewById(R.id.landmark);
        checkbox = findViewById(R.id.checkbox);
        submit = findViewById(R.id.submit);

        addressType = findViewById(R.id.addressType);
        state = findViewById(R.id.state);
        city = findViewById(R.id.city);
        area = findViewById(R.id.area);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<ValuesResponse> call = apiInterface.Address();
        call.enqueue(new Callback<ValuesResponse>() {
            @Override
            public void onResponse(Call<ValuesResponse> call, Response<ValuesResponse> response) {
                if (response.isSuccessful()) ;
                ValuesResponse valuesResponse = response.body();
                if (valuesResponse.status == 1) {
                    final ValuesDataResponse valuesDataResponse = valuesResponse.data;
                    List<AddressTypeResponse> addressTypeResponse = valuesDataResponse.addressType;

                    final List<Category_spinner> address_spinner = new ArrayList<>();
                    for (int i = 0; i < addressTypeResponse.size(); i++) {
                        final Category_spinner area = new Category_spinner();
                        area.setQuantiti(addressTypeResponse.get(i).name);
                        address_spinner.add(area);
                    }
                    final CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, address_spinner);
                    addressType.setAdapter(customSpinnerAdapter);
                    addressType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            address_id = address_spinner.get(position).getQuantiti();
                            final List<Category_spinner> state_spinner = new ArrayList<>();
                            stateResponse = valuesDataResponse.state;
                            for (int i = 0; i < stateResponse.size(); i++) {
                                final Category_spinner state1 = new Category_spinner();
                                state1.setQuantiti(stateResponse.get(i).stateName);
                                state1.setId(stateResponse.get(i).stateId);
                                state_spinner.add(state1);

                                CustomSpinnerAdapter customSpinnerAdapter1 = new CustomSpinnerAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, state_spinner);
                                state.setAdapter(customSpinnerAdapter1);
                                state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        state_id = state_spinner.get(position).getId();
                                        city(state_id);
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }

                else if (valuesResponse.status ==0){
                    Toast.makeText(AddressActivity.this, valuesResponse.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ValuesResponse> call, Throwable t) {
                Toast.makeText(AddressActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    type = 1;
                } else {
                    type = 0;
                }
            }
        });
        back_iv = findViewById(R.id.back);
        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isConnected = ConnectionDetector.isConnected();
                if (isConnected) {
                    getAddress();
                } else {
                    CommonClass.showSnack(isConnected, linear_add);
                }
            }
        });

    }

    private void getAddress() {

        house = Houseno.getText().toString();
        resdent = resdential.getText().toString();
        land = landmark.getText().toString();

        if (area_isd.equals("")){
            Toast.makeText(this, "Select Area", Toast.LENGTH_SHORT).show();
        }
        else if (address_id.equals("")) {
            Toast.makeText(this, "Select Address", Toast.LENGTH_SHORT).show();
        }
        else if (state_id.equals("")) {
            Toast.makeText(this, "Select State", Toast.LENGTH_SHORT).show();
        }
        else if (city_id.equals("")) {
            Toast.makeText(this, "Select City", Toast.LENGTH_SHORT).show();
        }
        else if (house.equals("")) {
            Toast.makeText(this, "Enter House No", Toast.LENGTH_SHORT).show();
        }
        else if (resdent.equals("")) {
            Toast.makeText(this, "Enter Resdential Address", Toast.LENGTH_SHORT).show();
        }
        else if (land.equals("")) {
            Toast.makeText(this, "Enter Landmark", Toast.LENGTH_SHORT).show();

        } else {
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
            String user_id = sharedPreferences.getString("user_id", "");

            final ProgressDialog progressDialog = new ProgressDialog(AddressActivity.this);
            progressDialog.setMessage("wait...");
            progressDialog.show();
            apiInterface1 = ApiClient.getClient().create(ApiInterface.class);
            Call<AddAddressResponse> call1 = apiInterface1.AddAddress(user_id, address_id, state_id, city_id,
                    house, resdent, area_isd, land, type);
            call1.enqueue(new Callback<AddAddressResponse>() {
                @Override
                public void onResponse(Call<AddAddressResponse> call, Response<AddAddressResponse> response) {
                    if (response.isSuccessful()) ;
                    AddAddressResponse addAddressResponse = response.body();
                    if (addAddressResponse.status.equals("1")) {
                        progressDialog.dismiss();
                        Toast.makeText(AddressActivity.this, addAddressResponse.message, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(AddressActivity.this, Addressbook.class).putExtra("Addressbook","AddAddress"));
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("AddressData", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        finish();
                    }
                    else if (addAddressResponse.status.equals("0")) {
                        progressDialog.dismiss();
                        Toast.makeText(AddressActivity.this, addAddressResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(AddressActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void city(String idd) {

        Call<CityResponse> cityResponseCall = apiInterface.City(idd);
        cityResponseCall.enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                if (response.isSuccessful()) ;
                CityResponse cityResponse = response.body();
                if (cityResponse.status == 1) {
                    final List<Category_spinner> city_spinner = new ArrayList<>();
                    List<CityDataResponse> cityDataResponse = cityResponse.data;
                    for (int i = 0; i < cityDataResponse.size(); i++) {
                        final Category_spinner state1 = new Category_spinner();
                        state1.setQuantiti(cityDataResponse.get(i).cityName);
                        state1.setId(cityDataResponse.get(i).cityId);
                        city_spinner.add(state1);

                        CustomSpinnerAdapter customSpinnerAdapter1 = new CustomSpinnerAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, city_spinner);
                        city.setAdapter(customSpinnerAdapter1);
                        city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                city_id = city_spinner.get(position).getId();
                                Area(city_id);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    }
                }
                else if (cityResponse.status ==0){
                    Toast.makeText(AddressActivity.this, cityResponse.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {
                Toast.makeText(AddressActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Area(String city_id) {

        Call<AreaResponse> callarea = apiInterface.Area(city_id);
        callarea.enqueue(new Callback<AreaResponse>() {
            @Override
            public void onResponse(Call<AreaResponse> call, Response<AreaResponse> response) {
                if (response.isSuccessful()) ;
                AreaResponse areaResponse = response.body();
                if (areaResponse.status == 1) {
                    final List<Category_spinner> area_spinner = new ArrayList<>();
                    List<AreaDataResponse> areaDataResponse = areaResponse.data;
                    if (areaDataResponse != null) {
                        for (int i = 0; i < areaDataResponse.size(); i++) {
                            final Category_spinner state1 = new Category_spinner();
                            state1.setQuantiti(areaDataResponse.get(i).areaName);
                            state1.setId(areaDataResponse.get(i).areaId);
                            area_spinner.add(state1);

                            customSpinnerAdapter1 = new CustomSpinnerAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, area_spinner);
                            area.setAdapter(customSpinnerAdapter1);
                            area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    area_isd = area_spinner.get(position).getId();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }
                    }
                    else {
                        area.setAdapter(null);
                    }
                }
                else if (areaResponse.status ==0){
                    Toast.makeText(AddressActivity.this, areaResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AreaResponse> call, Throwable t) {
                Toast.makeText(AddressActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
