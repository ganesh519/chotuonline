package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.PaymentmethodsRecyclerAdapter;
import igrand.info.tech.Chotuonline.model.CashonDeliveryResponse;
import igrand.info.tech.Chotuonline.model.PaymentDataResponse;
import igrand.info.tech.Chotuonline.model.PaymentMethodsDataResponse;
import igrand.info.tech.Chotuonline.model.PaymentMethodsResponse;
import igrand.info.tech.Chotuonline.model.PaymentResponse;
import igrand.info.tech.Chotuonline.model.UserWalletResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends AppCompatActivity {
    Button Pay;
    ImageView back;
    LinearLayout linear_cart11;
    public static ApiInterface apiInterface;
    TextView cart_total, walletbalance, Total_amount;
    LinearLayout Linear_payment,linear_patments;
    CheckBox check;
    RecyclerView recycler_paymentoptions;
    Integer value;
    int WalletBalance;
    String cash;
    String paymentid,type,total_balance;
    String type1;
    FragmentManager fragmentManager;
    Fragment currentFragment;
    FragmentTransaction fragmentTransaction;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_activity);
        back = findViewById(R.id.back);
        cart_total = findViewById(R.id.cart_total);
        walletbalance = findViewById(R.id.walletbalance);
        Total_amount = findViewById(R.id.Total_amount);
        Linear_payment = findViewById(R.id.Linear_payment);
        linear_patments=findViewById(R.id.linear_patments);
        check = findViewById(R.id.check);
        recycler_paymentoptions = findViewById(R.id.recycler_paymentoptions);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              finish();
            }
        });

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentMethodsResponse> paymentMethodsResponseCall = apiInterface.PaymentMethods();
        paymentMethodsResponseCall.enqueue(new Callback<PaymentMethodsResponse>() {
            @Override
            public void onResponse(Call<PaymentMethodsResponse> call, Response<PaymentMethodsResponse> response) {
                if (response.isSuccessful());
                PaymentMethodsResponse paymentMethodsResponse = response.body();
                if (paymentMethodsResponse.status.equals("1")) {
                    List<PaymentMethodsDataResponse> paymentMethodsDataResponse = paymentMethodsResponse.data;
                    recycler_paymentoptions.setAdapter(new PaymentmethodsRecyclerAdapter(paymentMethodsDataResponse, getApplicationContext()));
                    recycler_paymentoptions.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                    recycler_paymentoptions.setHasFixedSize(true);
                    recycler_paymentoptions.setNestedScrollingEnabled(false);

                }
                else if (paymentMethodsResponse.status.equals("0")){
                    Toast.makeText(PaymentActivity.this, paymentMethodsResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PaymentMethodsResponse> call, Throwable t) {
                Toast.makeText(PaymentActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


        Pay = findViewById(R.id.Pay);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        final String user_id = sharedPreferences.getString("user_id", "");

        final ProgressDialog progressDialog = new ProgressDialog(PaymentActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentResponse> call = apiInterface.PaymentOptions(user_id);
        call.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                if (response.isSuccessful()) ;
                PaymentResponse paymentResponse = response.body();
                if (paymentResponse.status.equals("1")) {
                    progressDialog.dismiss();
                    PaymentDataResponse paymentDataResponse = paymentResponse.data;
                    String total_amount=String.valueOf(paymentDataResponse.totalAmount);
                    cart_total.setText(total_amount);
                    WalletBalance=Integer.parseInt(paymentDataResponse.walletBalance);

                    total_balance=String.valueOf(paymentDataResponse.totalPayable);
                    Total_amount.setText(total_balance);
                    walletbalance.setText(Integer.toString(WalletBalance));

                } else if (paymentResponse.status.equals("0")) {
                    progressDialog.dismiss();

                }
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                progressDialog.dismiss();

            }
        });

        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    value = 1;
                    WalletCheck(value);
                } else {
                    value = 0;
                    WalletCheck(value);
                }
            }

        });

            Pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    type1=PaymentmethodsRecyclerAdapter.cash;

                    if (!type1.equals("")){

                        Paymentprocess();

                    }
                    else if (type1.equals("wallet")){
                        Paymentprocess();
                    }

                    else {
                        Toast.makeText(PaymentActivity.this, "Please Select Payment Method", Toast.LENGTH_SHORT).show();

                    }


                }
            });


        linear_cart11 = findViewById(R.id.linear_cart11);
        linear_cart11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PaymentActivity.this, MainActivity.class));
            }
        });

    }

    private void WalletCheck(int value){

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        final String user_id = sharedPreferences.getString("user_id", "");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserWalletResponse> call1 = apiInterface.UserWallet(user_id, value);
        call1.enqueue(new Callback<UserWalletResponse>() {
            @Override
            public void onResponse(Call<UserWalletResponse> call, Response<UserWalletResponse> response) {
                if (response.isSuccessful()) ;
                UserWalletResponse userWalletResponse = response.body();
                if (userWalletResponse.status.equals("1")) {

                    total_balance=String.valueOf(userWalletResponse.payableAmount);
                    Total_amount.setText(total_balance);
                    if (userWalletResponse.payableAmount == 0){
                        linear_patments.setVisibility(View.GONE);
                        PaymentmethodsRecyclerAdapter.cash="wallet";
                    }
                    else {
                        linear_patments.setVisibility(View.VISIBLE);
                    }
                }
                else if (userWalletResponse.status.equals("0")) {
                    Toast.makeText(PaymentActivity.this, userWalletResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserWalletResponse> call, Throwable t) {
                Toast.makeText(PaymentActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Paymentprocess(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        final String user_id = sharedPreferences.getString("user_id", "");

        SharedPreferences sharedPreference = getApplicationContext().getSharedPreferences("date", Context.MODE_PRIVATE);
        String DateSlot=sharedPreference.getString("DateSlot","");

//        String DateSlot1= REcyclerSotAdapter.date;
        String id=PaymentmethodsRecyclerAdapter.cash;
        if (id.equals("wallet")){
            paymentid="0";
        }
        else {
           paymentid=id;

        }
        final String TimeSlot = getIntent().getStringExtra("Timeslot");
        final String AddressId = getIntent().getStringExtra("address");

        final ProgressDialog progressDialog = new ProgressDialog(PaymentActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CashonDeliveryResponse> cashonDeliveryResponseCall = apiInterface.CashOnDelivery(user_id, AddressId, DateSlot
                , TimeSlot, value, paymentid);
        cashonDeliveryResponseCall.enqueue(new Callback<CashonDeliveryResponse>() {
            @Override
            public void onResponse(Call<CashonDeliveryResponse> call, Response<CashonDeliveryResponse> response) {
                if (response.isSuccessful()) ;
                CashonDeliveryResponse cashonDeliveryResponse = response.body();
                if (cashonDeliveryResponse.status.equals("1")) {
                    progressDialog.dismiss();
                    type=cashonDeliveryResponse.type;
                    if (type.equals("cash-or-card")){
                        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivity.this);
                        View view = getLayoutInflater().inflate(R.layout.alert_success, null);
                        builder.setView(view);
                        Button button = view.findViewById(R.id.ok);
                        AlertDialog b = builder.create();
                        b.show();
                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PaymentmethodsRecyclerAdapter.cash="";
                                startActivity(new Intent(PaymentActivity.this, MainActivity.class));
                                finish();
                            }
                        });
                    }

                    else if (type.equals("payu")){
                        PaymentmethodsRecyclerAdapter.cash="";
                        cash=cashonDeliveryResponse.cash;
                        String web_url="https://chotuonline.in/web/app/payu-redirect.php?cash="+cash;
                        Intent intent=new Intent(PaymentActivity.this,Payu_WebView.class);
                        intent.putExtra("cash_key",web_url);
                        startActivity(intent);
                    }
                    else if (type.equals("wallet")){
                        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivity.this);
                        View view = getLayoutInflater().inflate(R.layout.alert_success, null);
                        builder.setView(view);
                        Button button = view.findViewById(R.id.ok);
                        AlertDialog b = builder.create();
                        b.show();
                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PaymentmethodsRecyclerAdapter.cash="";
                                startActivity(new Intent(PaymentActivity.this, MainActivity.class));
                                finish();
                            }
                        });
                    }
//                    else {
//                        Toast.makeText(PaymentActivity.this, "Please select Payment method", Toast.LENGTH_SHORT).show();
//                    }
                }
                else if (cashonDeliveryResponse.status.equals("0")) {
                    progressDialog.dismiss();
                    Toast.makeText(PaymentActivity.this, cashonDeliveryResponse.message, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<CashonDeliveryResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(PaymentActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }




}
