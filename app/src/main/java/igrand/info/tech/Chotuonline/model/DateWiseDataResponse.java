package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class DateWiseDataResponse {



    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("disablity")
    @Expose
    public Integer disablity;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("disablity", disablity).toString();
    }

}
