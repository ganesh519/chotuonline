package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class SubSubCategoryDataResponse {


    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("brand")
    @Expose
    public String brand;
    @SerializedName("display_name")
    @Expose
    public String displayName;

    @SerializedName("sd")
    @Expose
    public String mVeriationId;

    @SerializedName("stlim")
    @Expose
    public String startLmt;

    @SerializedName("endlimt")
    @Expose
    public String endLmt;

    @SerializedName("variation_info")
    @Expose
    public List<SubSubCategoryVariationResponse> variationInfo = null;

    @SerializedName("sdfgd")
    @Expose
    public boolean isAdded;
    @SerializedName("rgrgr")
    @Expose
    public boolean first;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productId", productId).append("productName", productName).append("brand", brand).append("displayName", displayName).append("variationInfo", variationInfo).toString();
    }


}
