package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AllCategorySubCategoryDataREsponse {

    @SerializedName("subcategory_id")
    @Expose
    public String subcategoryId;
    @SerializedName("subcategory_name")
    @Expose
    public String subcategoryName;
    @SerializedName("dfgh")
    @Expose
    public boolean isSelected;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("subcategoryId", subcategoryId).append("subcategoryName", subcategoryName).toString();
    }
}
