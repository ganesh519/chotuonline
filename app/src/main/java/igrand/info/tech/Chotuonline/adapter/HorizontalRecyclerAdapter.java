package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.MainActivity;
import igrand.info.tech.Chotuonline.activity.ProductDetailsActivity;
import igrand.info.tech.Chotuonline.model.AddCartResponse;
import igrand.info.tech.Chotuonline.model.HomeProductimageResponse;
import igrand.info.tech.Chotuonline.model.TodayDealResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;




public class HorizontalRecyclerAdapter extends RecyclerView.Adapter<HorizontalRecyclerAdapter.Holder> {

    List<TodayDealResponse> popularProductResponse;
    Context context;
    ApiInterface apiInterface;
    String v_id, quantity, start_limit, end_limit, Minus, operator,Image;
    private int lastPosition = -1;

    public HorizontalRecyclerAdapter(List<TodayDealResponse> popularProductResponset, Context context) {
        this.popularProductResponse = popularProductResponset;
        this.context = context;
    }

    int count;

    @NonNull
    @Override
    public HorizontalRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_layout, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HorizontalRecyclerAdapter.Holder holder, final int position) {

        setAnimation(holder.itemView,position);

        final List<HomeProductimageResponse> homeProductimageResponse=popularProductResponse.get(position).productImages;
        for (int i = 0; i<homeProductimageResponse.size(); i++){
            Picasso.with(context).load(homeProductimageResponse.get(i).image).error(R.drawable.no_image).into(holder.product_image);
//            Image=homeProductimageResponse.get(i).image1;

        }
        holder.product_title.setText(popularProductResponse.get(position).brand);
        holder.product_name.setText(popularProductResponse.get(position).productName);
        holder.price.setText("₹" + popularProductResponse.get(position).unitPrice);
        holder.dprice.setText("₹ " + popularProductResponse.get(position).actualPrice);
        holder.dprice.setPaintFlags(holder.dprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.text_offer.setText(popularProductResponse.get(position).discount + "%" + "\n" + "OFF");
        holder.text_ml.setText(popularProductResponse.get(position).quantity);
        holder.text_price.setText("₹ " + popularProductResponse.get(position).unitPrice);

        v_id = popularProductResponse.get(position).variationId;
        start_limit = popularProductResponse.get(position).startLimit;
        end_limit = popularProductResponse.get(position).endLimit;

        if (!popularProductResponse.get(position).isAdded) {
            holder.linear_add.setVisibility(View.VISIBLE);
            holder.Relative_increase.setVisibility(View.GONE);
        }
        else {
            holder.linear_add.setVisibility(View.GONE);
            holder.Relative_increase.setVisibility(View.VISIBLE);
        }

        String coun=popularProductResponse.get(position).noOfItems;
        if (coun.equals("0")) {
            holder.linear_add.setVisibility(View.VISIBLE);
            holder.Relative_increase.setVisibility(View.GONE);

        }
        else {
            holder.linear_add.setVisibility(View.GONE);
            holder.Relative_increase.setVisibility(View.VISIBLE);
            holder.show_value.setText(coun);
        }

        holder.linear_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.progressbar_add.setVisibility(View.VISIBLE);
                v_id = popularProductResponse.get(position).variationId;
                quantity = "1";
                operator = "plus";
                start_limit = popularProductResponse.get(position).startLimit;
                end_limit = popularProductResponse.get(position).endLimit;

                SharedPreferences sharedPreferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");

                SharedPreferences sharedPreference =context.getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
                String device = sharedPreference.getString("mobile_id","");

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddCartResponse> call = apiInterface.AddCart(user_id, v_id, quantity, start_limit, end_limit, operator,device);
                call.enqueue(new Callback<AddCartResponse>() {
                    @Override
                    public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                        if (response.isSuccessful()) ;
                        AddCartResponse addCartResponse = response.body();
                        if (addCartResponse.status.equals("1")) {

                            popularProductResponse.get(position).isAdded = true;
                            holder.linear_add.setVisibility(View.GONE);
                            holder.progressbar_add.setVisibility(View.GONE);
                            holder.Relative_increase.setVisibility(View.VISIBLE);
                            holder.show_value.setText(String.valueOf(addCartResponse.noOfItems));
                            MainActivity.CartCount();
                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                        else if (addCartResponse.status.equals("0")) {
                            holder.progressbar_add.setVisibility(View.GONE);
                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddCartResponse> call, Throwable t) {
                        holder.progressbar_add.setVisibility(View.GONE);
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

                holder.linear_cart.setVisibility(View.VISIBLE);

            }
        });


        holder.decreas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.progressbar_add_increase.setVisibility(View.VISIBLE);
                SharedPreferences sharedPreferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");
                count = (Integer.parseInt(holder.show_value.getText().toString().trim()));

                v_id = popularProductResponse.get(position).variationId;
                start_limit = popularProductResponse.get(position).startLimit;
                end_limit = popularProductResponse.get(position).endLimit;
                operator = "minus";
                quantity = "1";
                SharedPreferences sharedPreference =context.getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
                String device = sharedPreference.getString("mobile_id","");

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddCartResponse> cartResponseCall = apiInterface.AddCart(user_id, v_id, quantity, start_limit, end_limit, operator,device);
                cartResponseCall.enqueue(new Callback<AddCartResponse>() {
                    @Override
                    public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                        if (response.isSuccessful()) ;
                        AddCartResponse addCartResponse = response.body();
                        if (addCartResponse.status.equals("1")) {
                            int Dec_qun = addCartResponse.noOfItems;
                            holder.show_value.setText(String.valueOf(Dec_qun));
                            popularProductResponse.get(position).isAdded = true;
                            MainActivity.CartCount();

                            if (Dec_qun < 1) {

                                holder.linear_add.setVisibility(View.VISIBLE);
                                holder.linear_cart.setVisibility(View.GONE);
                                popularProductResponse.get(position).isAdded = false;
                            }

                            holder.progressbar_add_increase.setVisibility(View.GONE);

//                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();

                        }
                        else if (addCartResponse.status.equals("0")) {
                            holder.progressbar_add_increase.setVisibility(View.GONE);
                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddCartResponse> call, Throwable t) {
                        holder.progressbar_add_increase.setVisibility(View.GONE);
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });

        holder.increas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");
                quantity = "1";
                operator = "plus";
                start_limit = popularProductResponse.get(position).startLimit;
                v_id = popularProductResponse.get(position).variationId;
                end_limit = popularProductResponse.get(position).endLimit;
                holder.progressbar_add_increase.setVisibility(View.VISIBLE);
                SharedPreferences sharedPreference =context.getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
                String device = sharedPreference.getString("mobile_id","");

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddCartResponse> cartResponseCall = apiInterface.AddCart(user_id, v_id, quantity, start_limit, end_limit, operator,device);
                cartResponseCall.enqueue(new Callback<AddCartResponse>() {
                    @Override
                    public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                        if (response.isSuccessful()) ;
                        AddCartResponse addCartResponse = response.body();
                        if (addCartResponse.status.equals("1")) {
                            int Dec_qun = addCartResponse.noOfItems;
                            holder.show_value.setText(Integer.toString(Dec_qun));
                            holder.progressbar_add_increase.setVisibility(View.GONE);
//                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();

                        }
                        else if (addCartResponse.status.equals("0")) {
                            holder.progressbar_add_increase.setVisibility(View.GONE);
                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddCartResponse> call, Throwable t) {
                        holder.progressbar_add_increase.setVisibility(View.GONE);
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });


        holder.product_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popularProductResponse == null){
                    Toast.makeText(context, "No Variations", Toast.LENGTH_SHORT).show();
                }
                else {
                    String vid = popularProductResponse.get(position).variationId;
                    SharedPreferences sharedPreference = context.getSharedPreferences("product", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor1 = sharedPreference.edit();
                    editor1.commit();
                    SharedPreferences sharedPreferences = context.getSharedPreferences("Variation", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("v_id", vid);
                    editor.commit();

                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    intent.putExtra("variation", "1234");
                    context.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return popularProductResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView product_image;
        TextView product_title, product_name;
        TextView text_offer;
        TextView price, text_price;
        TextView text_ml;
        TextView dprice;
        TextView show_value;
        ImageView increas, decreas;
        LinearLayout linear_cart;
        RelativeLayout linear_add, Relative_increase;
        ProgressBar progressbar_add, progressbar_add_increase;

        public Holder(View itemView) {
            super(itemView);

            product_image = (ImageView) itemView.findViewById(R.id.product_image);
            product_title = (TextView) itemView.findViewById(R.id.product_title);
            product_name = itemView.findViewById(R.id.product_name);
            price = (TextView) itemView.findViewById(R.id.price);
            dprice = (TextView) itemView.findViewById(R.id.dprice);
            text_offer = itemView.findViewById(R.id.text_offer);
            text_ml = itemView.findViewById(R.id.text_ml);
            text_price = itemView.findViewById(R.id.text_price);

            show_value = itemView.findViewById(R.id.show_value);
            decreas = itemView.findViewById(R.id.decreas);
            increas = itemView.findViewById(R.id.increas);
            linear_add = itemView.findViewById(R.id.linear_add);
            linear_cart = itemView.findViewById(R.id.linear_cart);
            Relative_increase = itemView.findViewById(R.id.Relative_increase);

            progressbar_add_increase = itemView.findViewById(R.id.progressbar_add_increase);
            progressbar_add = itemView.findViewById(R.id.progressbar_add);

        }
    }

    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}