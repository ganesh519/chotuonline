package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.ForgotpassResendResponse;
import igrand.info.tech.Chotuonline.model.MobilenumberOtpResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MobilenumberOtp extends AppCompatActivity {
    EditText signup_otp;
    String otp, code;
    ApiInterface apiInterface, apiInterface1;
    Button proceed;
    TextView resend,countdownText;
   private SmsVerifyCatcher smsVerifyCatcher;
    LinearLayout otpprocess;
    private static CountDownTimer countDownTimer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text);
        signup_otp = findViewById(R.id.signup_otp);
        proceed = findViewById(R.id.proceed);
        resend = findViewById(R.id.resend);
//        TimeCount();
//        smsVerifyCatcher.onStart();
        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                code = parseCode(message);
                signup_otp.setText(code);

            }
        });


        final String mobile_no=getIntent().getStringExtra("MobileNumber");


//        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("mobile", Context.MODE_PRIVATE);
//        String mobile_no = sharedPreferences.getString("MobileNumber", "");

        SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        final String user_id = sharedPreferences1.getString("user_id", "");

        final ProgressDialog progressDialog1 = new ProgressDialog(MobilenumberOtp.this);
        progressDialog1.setMessage("wait...");
        progressDialog1.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ForgotpassResendResponse> call1 = apiInterface.ForgotpassResend(user_id, mobile_no);
        call1.enqueue(new Callback<ForgotpassResendResponse>() {
            @Override
            public void onResponse(Call<ForgotpassResendResponse> call, Response<ForgotpassResendResponse> response) {
                if (response.isSuccessful()) ;
                ForgotpassResendResponse forgotpassResendResponse = response.body();
                if (forgotpassResendResponse.status.equals("1")) {
                    progressDialog1.dismiss();
                    Toast.makeText(MobilenumberOtp.this, forgotpassResendResponse.message, Toast.LENGTH_SHORT).show();

                } else if (forgotpassResendResponse.status.equals("0")) {
                    progressDialog1.dismiss();
                    Toast.makeText(MobilenumberOtp.this, forgotpassResendResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ForgotpassResendResponse> call, Throwable t) {
                progressDialog1.dismiss();
                Toast.makeText(MobilenumberOtp.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                otp = signup_otp.getText().toString();
                smsVerifyCatcher.onStart();
//                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("mobile", Context.MODE_PRIVATE);
//                String mobile_no = sharedPreferences.getString("MobileNumber", "");

//                SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
//                final String user_id = sharedPreferences1.getString("user_id", "");
                final ProgressDialog progressDialog = new ProgressDialog(MobilenumberOtp.this);
                progressDialog.setMessage("wait...");
                progressDialog.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<MobilenumberOtpResponse> call = apiInterface.MobilenumberOtp(user_id, mobile_no, otp);
                call.enqueue(new Callback<MobilenumberOtpResponse>() {
                    @Override
                    public void onResponse(Call<MobilenumberOtpResponse> call, Response<MobilenumberOtpResponse> response) {
                        if (response.isSuccessful()) ;
                        MobilenumberOtpResponse mobilenumberOtpResponse = response.body();
                        if (mobilenumberOtpResponse.status.equals("1")) {
                            progressDialog.dismiss();
                            Toast.makeText(MobilenumberOtp.this, mobilenumberOtpResponse.message, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(MobilenumberOtp.this, MyAccountActivity.class));
                            finish();
                        } else if (mobilenumberOtpResponse.status.equals("0")) {
                            progressDialog.dismiss();
                            Toast.makeText(MobilenumberOtp.this, mobilenumberOtpResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<MobilenumberOtpResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(MobilenumberOtp.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smsVerifyCatcher.onStart();
//                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("mobile", Context.MODE_PRIVATE);
//                String mobile_no = sharedPreferences.getString("MobileNumber", "");

//                SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
//                final String user_id = sharedPreferences1.getString("user_id", "");

                final ProgressDialog progressDialog1 = new ProgressDialog(MobilenumberOtp.this);
                progressDialog1.setMessage("wait...");
                progressDialog1.show();


                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<ForgotpassResendResponse> call1 = apiInterface.ForgotpassResend(user_id, mobile_no);
                call1.enqueue(new Callback<ForgotpassResendResponse>() {
                    @Override
                    public void onResponse(Call<ForgotpassResendResponse> call, Response<ForgotpassResendResponse> response) {
                        if (response.isSuccessful()) ;
                        ForgotpassResendResponse forgotpassResendResponse = response.body();
                        if (forgotpassResendResponse.status.equals("1")) {
                            progressDialog1.dismiss();
                            Toast.makeText(MobilenumberOtp.this, forgotpassResendResponse.message, Toast.LENGTH_SHORT).show();

                        } else if (forgotpassResendResponse.status.equals("0")) {
                            progressDialog1.dismiss();
                            Toast.makeText(MobilenumberOtp.this, forgotpassResendResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ForgotpassResendResponse> call, Throwable t) {
                        progressDialog1.dismiss();
                        Toast.makeText(MobilenumberOtp.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


//    private void TimeCount(){
//
//        resend.setVisibility(View.GONE);
//        otpprocess.setVisibility(View.VISIBLE);
//
//        countDownTimer = new CountDownTimer(30000, 1000) {
//            public void onTick(long millisUntilFinished) {
//                long millis = millisUntilFinished;
//                //Convert milliseconds into hour,minute and seconds
//                String hms = String.format("%02d", TimeUnit.MILLISECONDS.toSeconds(millis));
//                countdownText.setText(hms);//set text
//            }
//
//            public void onFinish() {
//                otpprocess.setVisibility(View.GONE);
//                resend.setVisibility(View.VISIBLE);
//            }
//        }.start();
//
//    }
}
