package igrand.info.tech.Chotuonline.model;

public class Diaper {

    private int product_image;
    private String product_title;
    private String product_name;
    private String product_price;
    private String product_dprice;
    private String off;

    public Diaper(int product_image, String product_title, String product_name, String product_price, String product_dprice, String off) {
        this.product_image = product_image;
        this.product_title = product_title;
        this.product_name = product_name;
        this.product_price = product_price;
        this.product_dprice = product_dprice;
        this.off = off;
    }

    public int getProduct_image() {
        return product_image;
    }

    public void setProduct_image(int product_image) {
        this.product_image = product_image;
    }

    public String getProduct_title() {
        return product_title;
    }

    public void setProduct_title(String product_title) {
        this.product_title = product_title;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_dprice() {
        return product_dprice;
    }

    public void setProduct_dprice(String product_dprice) {
        this.product_dprice = product_dprice;
    }

    public String getOff() {
        return off;
    }

    public void setOff(String off) {
        this.off = off;
    }
}
