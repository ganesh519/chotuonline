package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.PrivacyDataResponse;
import igrand.info.tech.Chotuonline.model.PrivacyResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrivacyActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView back;
    TextView privacy;
    ApiInterface apiInterface;
    String textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        back = findViewById(R.id.back);
        privacy = findViewById(R.id.privacy);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.privacy:
                getData();
                break;
        }

    }

    private void getData() {
        final ProgressDialog progressDialog = new ProgressDialog(PrivacyActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PrivacyResponse> call = apiInterface.Privacy();
        call.enqueue(new Callback<PrivacyResponse>() {
            @Override
            public void onResponse(Call<PrivacyResponse> call, Response<PrivacyResponse> response) {
                if (response.isSuccessful()) ;
                PrivacyResponse privacyResponse = response.body();
                if (privacyResponse.status == 1) {
                    PrivacyDataResponse privacyDataResponse = privacyResponse.data;
                    textView = privacyDataResponse.description;
                    privacy.setText(textView);
                    progressDialog.dismiss();
                }
            }
            @Override
            public void onFailure(Call<PrivacyResponse> call, Throwable t) {
                Toast.makeText(PrivacyActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
