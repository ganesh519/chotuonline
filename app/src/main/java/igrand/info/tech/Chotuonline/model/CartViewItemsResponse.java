package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CartViewItemsResponse {

    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("brand")
    @Expose
    public String brand;
    @SerializedName("display_name")
    @Expose
    public String displayName;
    @SerializedName("quantity")
    @Expose
    public String quantity;
    @SerializedName("variation_id")
    @Expose
    public String variationId;
    @SerializedName("variation_image")
    @Expose
    public String variationImage;
    @SerializedName("stock_left")
    @Expose
    public String stockLeft;
    @SerializedName("unit_price")
    @Expose
    public String unitPrice;
    @SerializedName("total_unit_price")
    @Expose
    public String totalUnitPrice;
    @SerializedName("actual_price")
    @Expose
    public String actualPrice;
    @SerializedName("discount")
    @Expose
    public Float discount;
    @SerializedName("start_limit")
    @Expose
    public String startLimit;
    @SerializedName("end_limit")
    @Expose
    public String endLimit;
    @SerializedName("no_of_items")
    @Expose
    public String noOfItems;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productId", productId).append("productName", productName).append("brand", brand).append("displayName", displayName).append("quantity", quantity).append("variationId", variationId).append("variationImage", variationImage).append("stockLeft", stockLeft).append("unitPrice", unitPrice).append("totalUnitPrice", totalUnitPrice).append("actualPrice", actualPrice).append("discount", discount).append("startLimit", startLimit).append("endLimit", endLimit).append("noOfItems", noOfItems).toString();
    }
}
