package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.FaqRecyclerAdapter;
import igrand.info.tech.Chotuonline.model.FaqDataResponse;
import igrand.info.tech.Chotuonline.model.FaqResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FAQsActivity extends AppCompatActivity {

    Context ctx;
    ImageView back;
    RecyclerView faqrecycler;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        final ProgressDialog progressDialog = new ProgressDialog(FAQsActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        faqrecycler = findViewById(R.id.faqrecycler);
        faqrecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        faqrecycler.setHasFixedSize(true);
        faqrecycler.setNestedScrollingEnabled(false);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<FaqResponse> call = apiInterface.Faq();
        call.enqueue(new Callback<FaqResponse>() {
            @Override
            public void onResponse(Call<FaqResponse> call, Response<FaqResponse> response) {
                if (response.isSuccessful()) ;
                FaqResponse faqResponse = response.body();
                if (faqResponse.status.equals("1")) {
                    progressDialog.dismiss();
                    List<FaqDataResponse> faqDataResponse = faqResponse.data;
                    faqrecycler.setAdapter(new FaqRecyclerAdapter(faqDataResponse, ctx));
                }
                else if (faqResponse.status.equals("0")) {
                    Toast.makeText(FAQsActivity.this, faqResponse.message, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<FaqResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(FAQsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
