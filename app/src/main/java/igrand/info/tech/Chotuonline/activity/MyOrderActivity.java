package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.MyOrderRecyclerAdapter;
import igrand.info.tech.Chotuonline.model.MyOrdersDataResponse;
import igrand.info.tech.Chotuonline.model.MyOrdersResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrderActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ImageView back;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myorder_activity);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.recycler_order);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences.getString("user_id", "");

        final ProgressDialog progressDialog = new ProgressDialog(MyOrderActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MyOrdersResponse> call = apiInterface.MyOrders(user_id);
        call.enqueue(new Callback<MyOrdersResponse>() {
            @Override
            public void onResponse(Call<MyOrdersResponse> call, Response<MyOrdersResponse> response) {
                if (response.isSuccessful()) ;
                MyOrdersResponse myOrdersResponse = response.body();
                if (myOrdersResponse.status.equals("1")) {
                    progressDialog.dismiss();
                    List<MyOrdersDataResponse> myOrdersDataResponse = myOrdersResponse.data;
                    if (myOrdersDataResponse != null) {
                        recyclerView.setAdapter(new MyOrderRecyclerAdapter(myOrdersDataResponse, getApplicationContext()));
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setNestedScrollingEnabled(false);
                    }
                } else if (myOrdersResponse.status.equals("0")) {
                    progressDialog.dismiss();
                    Toast.makeText(MyOrderActivity.this, myOrdersResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MyOrdersResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MyOrderActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
