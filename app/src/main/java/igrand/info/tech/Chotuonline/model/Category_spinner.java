package igrand.info.tech.Chotuonline.model;

public class Category_spinner {
    private String quantiti;
    private String id;
    private String pricee;
    private String image;
    private String image1;
    private String image2;
    private String image3;
    private String dprice;
    private String off;
    private String startlimit;
    private String endlimit;
    private String pname;
    private String save;

    public String getSave() {
        return save;
    }

    public void setSave(String save) {
        this.save = save;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getQuantiti() {
        return quantiti;
    }

    public void setQuantiti(String quantiti) {
        this.quantiti = quantiti;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPricee() {
        return pricee;
    }

    public void setPricee(String pricee) {
        this.pricee = pricee;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getDprice() {
        return dprice;
    }

    public void setDprice(String dprice) {
        this.dprice = dprice;
    }

    public String getOff() {
        return off;
    }

    public void setOff(String off) {
        this.off = off;
    }

    public String getStartlimit() {
        return startlimit;
    }

    public void setStartlimit(String startlimit) {
        this.startlimit = startlimit;
    }

    public String getEndlimit() {
        return endlimit;
    }

    public void setEndlimit(String endlimit) {
        this.endlimit = endlimit;
    }
}
