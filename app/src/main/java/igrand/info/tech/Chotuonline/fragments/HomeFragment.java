package igrand.info.tech.Chotuonline.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.MainActivity;
import igrand.info.tech.Chotuonline.activity.SearchActivity;
import igrand.info.tech.Chotuonline.adapter.CategoryRecyclerAdapter;
import igrand.info.tech.Chotuonline.adapter.DownSliderViewPagerAdapter;
import igrand.info.tech.Chotuonline.adapter.HorizontalRecyclerAdapter;
import igrand.info.tech.Chotuonline.adapter.UpSliderViewPagerAdapter;
import igrand.info.tech.Chotuonline.localdatabase.BACRecord;
import igrand.info.tech.Chotuonline.localdatabase.ChotuDB;
import igrand.info.tech.Chotuonline.model.AllCategoryResponse;
import igrand.info.tech.Chotuonline.model.Category;
import igrand.info.tech.Chotuonline.model.DownSliderResponse;
import igrand.info.tech.Chotuonline.model.HomeDataResponse;
import igrand.info.tech.Chotuonline.model.HomeResponse;
import igrand.info.tech.Chotuonline.model.TodayDealResponse;
import igrand.info.tech.Chotuonline.model.UpSliderResponse;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class HomeFragment extends android.support.v4.app.Fragment {
    TextView et_double_search;
    View view;
    private RecyclerView recyclerView, recycler_slider, recycler_downslider;
    private View.OnClickListener listener;
    ChotuDB chotuDB;
    List<BACRecord> chotuCartArraylist;
    ViewPager mPager1;
    AutoScrollViewPager mPager;
    private static int currentPage = 0;

    RecyclerView recyclerView1;
    RecyclerView.Adapter adapter, adapter1;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Category> arrayList1 = new ArrayList<>();

    ApiInterface apiInterface;
    List<UpSliderResponse> upSliderResponse;
    List<DownSliderResponse> downSliderResponse;
    private HomeDataResponse homeDataResponse;
    Context ctx;
    NestedScrollView home_scroll;
    AutoScrollViewPager viewPager,down_pager;
    CircleIndicator indicator,down_indicator;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, null);
        initView(view);

        MainActivity.CartCount();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("wait...");
        progressDialog.show();

        recyclerView = (RecyclerView) view.findViewById(R.id.home_recycler);
        recyclerView1 = (RecyclerView) view.findViewById(R.id.recycler_category);
//        recycler_slider = (RecyclerView) view.findViewById(R.id.recycler_slider);
//        recycler_downslider = (RecyclerView) view.findViewById(R.id.recycler_downslider);
        home_scroll = view.findViewById(R.id.home_scroll);

        viewPager=view.findViewById(R.id.pager);
        indicator=view.findViewById(R.id.indicator);

        down_indicator=view.findViewById(R.id.down_indicator);
        down_pager=view.findViewById(R.id.down_pager);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences.getString("user_id", "");

        SharedPreferences sharedPreference =getActivity().getSharedPreferences("MobileDeviceId", Context.MODE_PRIVATE);
        String device = sharedPreference.getString("mobile_id","");

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<HomeResponse> call = apiInterface.PostHome(user_id,device);
        call.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                if (response.isSuccessful()) ;
                HomeResponse homeResponse = response.body();
                if (homeResponse.status == 1) {
                    progressDialog.dismiss();
                    home_scroll.setVisibility(View.VISIBLE);
                    HomeDataResponse homeDataResponse = homeResponse.data;
                    upSliderResponse = homeDataResponse.upSlider;
                    UpSliderViewPagerAdapter upSliderViewPagerAdapter=new UpSliderViewPagerAdapter(upSliderResponse,getActivity());
                    viewPager.setAdapter(upSliderViewPagerAdapter);
                    viewPager.startAutoScroll(1000);
                    viewPager.setInterval(2500);
                    viewPager.setDirection(AutoScrollViewPager.RIGHT);
                    indicator.setViewPager(viewPager);
//                    HomeAdapter2 adapter = new HomeAdapter2(getActivity(), homeDataResponse);
//                    recycler_slider.setAdapter(adapter);
//                    recycler_slider.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));

                    List<TodayDealResponse> todayDealResponse = homeDataResponse.todayDeals;
                    if (todayDealResponse != null) {
                        recyclerView.setAdapter(new HorizontalRecyclerAdapter(todayDealResponse, getContext()));
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setNestedScrollingEnabled(false);
                    }
                    List<AllCategoryResponse> categoryResponse = homeDataResponse.allCategories;
                    if (categoryResponse != null) {
                        recyclerView1.setAdapter(new CategoryRecyclerAdapter(categoryResponse, getContext()));
                        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
                        recyclerView1.setLayoutManager(gridLayoutManager);
                        recyclerView1.setHasFixedSize(true);
                        recyclerView1.setNestedScrollingEnabled(false);
//                                        int padding = (int) getActivity().getResources().getDimension(R.dimen._3sdp);
//                                        recyclerView1.setPadding(padding, padding, padding, padding);
                    }
                    downSliderResponse = homeDataResponse.downSlider;
                    DownSliderViewPagerAdapter downSliderViewPagerAdapter=new DownSliderViewPagerAdapter(downSliderResponse,getActivity());
                    down_pager.setAdapter(downSliderViewPagerAdapter);
                    down_pager.startAutoScroll(1000);
                    down_pager.setInterval(2500);
                    down_pager.setDirection(AutoScrollViewPager.RIGHT);
                    down_indicator.setViewPager(down_pager);

//                    LinearLayoutManager manager1 = new LinearLayoutManager(getActivity());
//                    recycler_downslider.setLayoutManager(manager1);
//                    HomeAdapter1 adapter1 = new HomeAdapter1(getActivity(), homeDataResponse);
//                    recycler_downslider.setAdapter(adapter1);
                }
                else if (homeResponse.status == 0) {
                    home_scroll.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), homeResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                home_scroll.setVisibility(View.GONE);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });

        return view;
    }

    private void initView(View view) {

        MainActivity.user_iv.setVisibility(View.VISIBLE);
        MainActivity.logo.setVisibility(View.VISIBLE);
        MainActivity.main_search.setVisibility(View.GONE);
        MainActivity.category_text.setVisibility(View.GONE);

        et_double_search = view.findViewById(R.id.et_double_search);
        recyclerView = view.findViewById(R.id.home_recycler);
        et_double_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), SearchActivity.class));
            }
        });
    }

    private void setFragment(android.support.v4.app.Fragment fragment) {
        android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, fragment);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }
}

