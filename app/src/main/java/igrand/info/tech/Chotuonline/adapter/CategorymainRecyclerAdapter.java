package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.fragments.CategoryMainListFragment;
import igrand.info.tech.Chotuonline.model.AllCategorySubCategoryDataREsponse;

public class CategorymainRecyclerAdapter extends RecyclerView.Adapter<CategorymainRecyclerAdapter.Holder> {
    List<AllCategorySubCategoryDataREsponse> allCategorySubCategoryDataREsponses;
    CategoryMainListFragment fragment;
    Context context;
    private int lastPosition = -1;

    public CategorymainRecyclerAdapter(List<AllCategorySubCategoryDataREsponse> allCategorySubCategoryDataREsponse, Context context, CategoryMainListFragment fragment) {
        this.allCategorySubCategoryDataREsponses = allCategorySubCategoryDataREsponse;
        this.context = context;
        this.fragment = fragment;

    }

    @NonNull
    @Override
    public CategorymainRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_category, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategorymainRecyclerAdapter.Holder holder, final int position) {

        setAnimation(holder.itemView,position);
        holder.text_name.setText(allCategorySubCategoryDataREsponses.get(position).subcategoryName);
//        holder.tabs.setBackgroundResource(R.drawable.button_shape1);

        holder.tabs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //allCategorySubCategoryDataREsponses.get(position).isSelected=true;

                for (int i = 0; i < allCategorySubCategoryDataREsponses.size(); i++) {
                    if (i == position) {
                        allCategorySubCategoryDataREsponses.get(i).isSelected = true;

                        //holder.text_name.setTextColor(Color.parseColor("#ff0000"));
                    }
                    else {
                        allCategorySubCategoryDataREsponses.get(i).isSelected = false;
                      //  holder.text_name.setTextColor(Color.parseColor("#000000"));

                    }
                }
               // holder.text_name.setTextColor(Color.parseColor("#ff0000"));

                String subCatId = allCategorySubCategoryDataREsponses.get(position).subcategoryId;
                fragment.cat(subCatId, context);

                notifyDataSetChanged();

            }
        });

        if (allCategorySubCategoryDataREsponses.get(position).isSelected) {
//            holder.tabs.setBackgroundColor(Color.parseColor("#ff0000"));
            holder.tabs.setBackgroundResource(R.drawable.button_shape1);
            holder.text_name.setTextColor(Color.parseColor("#ffffff"));

        } else {
//            holder.tabs.setBackgroundColor(Color.parseColor("#FFFFFF"));
            holder.tabs.setBackgroundResource(R.drawable.button_shape);
            holder.text_name.setTextColor(Color.parseColor("#1b1b1b"));

        }
    }

    @Override
    public int getItemCount() {
        return allCategorySubCategoryDataREsponses.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView text_name;
        RelativeLayout tabs;

        public Holder(View itemView) {
            super(itemView);
            text_name = itemView.findViewById(R.id.text_name);
            tabs = itemView.findViewById(R.id.tabs);

        }
    }

    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
