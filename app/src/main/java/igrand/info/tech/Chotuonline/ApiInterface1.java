package igrand.info.tech.Chotuonline;

import igrand.info.tech.Chotuonline.model.JSignupResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface1 {

    @FormUrlEncoded
    @POST("sign-up.php")
    Call<JSignupResponse> JSignup(@Field("username") String username, @Field("mobile_number") String mobile_number);

}
