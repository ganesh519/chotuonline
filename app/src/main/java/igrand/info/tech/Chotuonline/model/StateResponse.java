package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class StateResponse {


    @SerializedName("state_id")
    @Expose
    public String stateId;
    @SerializedName("state_name")
    @Expose
    public String stateName;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("stateId", stateId).append("stateName", stateName).toString();
    }
}
