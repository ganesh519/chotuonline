package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.adapter.AboutUsRecyclerAdapter;
import igrand.info.tech.Chotuonline.model.AboutDataResponse;
import igrand.info.tech.Chotuonline.model.AboutResponse;
import retrofit2.Call;
import retrofit2.Callback;

public class AboutUsActivity extends AppCompatActivity {
    ImageView back;
    ApiInterface apiInterface;
    RecyclerView recycler_about;
    Context ctx;
    TextView terms,page,discount_text,like_text,send_text,time_text,lock_text,money_text,right_text,user_text;
    LinearLayout linear;
    SwipeRefreshLayout swiperefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        recycler_about=findViewById(R.id.recycler_about);
        swiperefresh=findViewById(R.id.swiperefresh);
        getData();
        discount_text=findViewById(R.id.discount_text);
        discount_text.setTypeface(discount_text.getTypeface(), Typeface.BOLD);

        like_text=findViewById(R.id.like_text);
        like_text.setTypeface(like_text.getTypeface(), Typeface.BOLD);

        send_text=findViewById(R.id.send_text);
        send_text.setTypeface(send_text.getTypeface(), Typeface.BOLD);

        time_text=findViewById(R.id.time_text);
        time_text.setTypeface(time_text.getTypeface(), Typeface.BOLD);

        lock_text=findViewById(R.id.lock_text);
        lock_text.setTypeface(lock_text.getTypeface(), Typeface.BOLD);

        money_text=findViewById(R.id.money_text);
        money_text.setTypeface(money_text.getTypeface(), Typeface.BOLD);

        right_text=findViewById(R.id.right_text);
        right_text.setTypeface(right_text.getTypeface(), Typeface.BOLD);

        user_text=findViewById(R.id.user_text);
        user_text.setTypeface(user_text.getTypeface(), Typeface.BOLD);

        linear=findViewById(R.id.linear);


        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//
//        final ProgressDialog progressDialog = new ProgressDialog(AboutUsActivity.this);
//        progressDialog.setMessage("wait...");
//        progressDialog.show();
//
//        apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        Call<AboutResponse> callabout = apiInterface.Aboutus();
//        callabout.enqueue(new Callback<AboutResponse>() {
//            @Override
//            public void onResponse(Call<AboutResponse> call, retrofit2.Response<AboutResponse> response) {
//                if (response.isSuccessful()) ;
//                AboutResponse aboutResponse = response.body();
//                if (aboutResponse.status.equals("1")) {
//                    linear.setVisibility(View.VISIBLE);
//                    List<AboutDataResponse> aboutDataResponse=aboutResponse.data;
//                    for (int i=0;i<aboutDataResponse.size();i++){
//                        recycler_about.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
//                        recycler_about.setAdapter(new AboutUsRecyclerAdapter(aboutDataResponse, ctx));
//                        recycler_about.setHasFixedSize(true);
//                        recycler_about.setNestedScrollingEnabled(false);
//                        progressDialog.dismiss();
//                    }
//
//                } else if (aboutResponse.status.equals("0")) {
//
//                    linear.setVisibility(View.GONE);
//                    Toast.makeText(AboutUsActivity.this, aboutResponse.message, Toast.LENGTH_SHORT).show();
//                    progressDialog.dismiss();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<AboutResponse> call, Throwable t) {
//                linear.setVisibility(View.GONE);
//                progressDialog.dismiss();
//                Toast.makeText(AboutUsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefresh.setRefreshing(true);
                getData();
            }
        });
    }

    private void getData() {

//        final ProgressDialog progressDialog = new ProgressDialog(AboutUsActivity.this);
//        progressDialog.setMessage("wait...");
//        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AboutResponse> callabout = apiInterface.Aboutus();
        callabout.enqueue(new Callback<AboutResponse>() {
            @Override
            public void onResponse(Call<AboutResponse> call, retrofit2.Response<AboutResponse> response) {
                if (response.isSuccessful()) ;
                AboutResponse aboutResponse = response.body();
                if (aboutResponse.status.equals("1")) {
                    swiperefresh.setRefreshing(false);
                    linear.setVisibility(View.VISIBLE);
                    List<AboutDataResponse> aboutDataResponse=aboutResponse.data;
                    for (int i=0;i<aboutDataResponse.size();i++){
                        recycler_about.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                        recycler_about.setAdapter(new AboutUsRecyclerAdapter(aboutDataResponse, ctx));
                        recycler_about.setHasFixedSize(true);
                        recycler_about.setNestedScrollingEnabled(false);
//                        progressDialog.dismiss();
                    }

                } else if (aboutResponse.status.equals("0")) {
                    swiperefresh.setRefreshing(false);
                    linear.setVisibility(View.GONE);
                    Toast.makeText(AboutUsActivity.this, aboutResponse.message, Toast.LENGTH_SHORT).show();
//                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<AboutResponse> call, Throwable t) {
                swiperefresh.setRefreshing(false);
                linear.setVisibility(View.GONE);
//                progressDialog.dismiss();
                Toast.makeText(AboutUsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

}
