package igrand.info.tech.Chotuonline.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.HomeProductimageResponse;
import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase;

public class Zoom_Image extends AppCompatActivity {
    List<HomeProductimageResponse> images;
    String name;
    String Position;
    TextView productname;
    RecyclerView rercycler_zoom;
    ImageViewTouch mp3Image;
    ImageView back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zoom_image);
        mp3Image=findViewById(R.id.mp3Image);
        productname=findViewById(R.id.productname);
        rercycler_zoom=findViewById(R.id.rercycler_zoom);
        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Bundle bundle=getIntent().getExtras();
        if (bundle!=null){
             name=bundle.getString("pname");
             Position=bundle.getString("pos");
        }
        images= (List<HomeProductimageResponse>) getIntent().getSerializableExtra("Sliderimage");

        Picasso.with(getApplicationContext()).load(images.get(Integer.parseInt(Position)).image).into(mp3Image);

//        Picasso.get().load(get).into(mp3Image);
        productname.setText(name);
        mp3Image.setDisplayType(ImageViewTouchBase.DisplayType.FIT_TO_SCREEN);
        rercycler_zoom.setAdapter(new RecyclerZommAdapter(images));
        rercycler_zoom.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        rercycler_zoom.setHasFixedSize(true);
        rercycler_zoom.setNestedScrollingEnabled(false);
    }

    public class RecyclerZommAdapter extends RecyclerView.Adapter<RecyclerZommAdapter.Holder>{
        List<HomeProductimageResponse> images;

        public RecyclerZommAdapter(List<HomeProductimageResponse> images) {
            this.images=images;

        }

        @NonNull
        @Override
        public RecyclerZommAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_zoom, parent, false);
            return new Holder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final RecyclerZommAdapter.Holder holder, final int pp) {

            Picasso.with(getApplicationContext()).load(images.get(pp).image).into(holder.image_zoom);

            holder.image_zoom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Picasso.with(getApplicationContext()).load(images.get(pp).image).into(mp3Image);
                }
            });
        }

        @Override
        public int getItemCount() {
            return images.size();
        }
        class Holder extends RecyclerView.ViewHolder{
            ImageView image_zoom;

            public Holder(View itemView) {
                super(itemView);
                image_zoom=itemView.findViewById(R.id.image_zoom);
            }
        }
    }
}
