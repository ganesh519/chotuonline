package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class ProductVariationDetailsDataResponse {
    @SerializedName("product_variation_id")
    @Expose
    public String productVariationId;
    @SerializedName("variation_code")
    @Expose
    public String variationCode;
    @SerializedName("quantity")
    @Expose
    public String quantity;
    @SerializedName("actual_price")
    @Expose
    public String actualPrice;
    @SerializedName("you_saved")
    @Expose
    public Float youSaved;
    @SerializedName("unit_price")
    @Expose
    public String unitPrice;
    @SerializedName("stock_left")
    @Expose
    public String stockLeft;
    @SerializedName("brand")
    @Expose
    public String brand;
    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("discount")
    @Expose
    public Integer discount;
    @SerializedName("start_limit")
    @Expose
    public String startLimit;
    @SerializedName("end_limit")
    @Expose
    public String endLimit;
    @SerializedName("no_of_items")
    @Expose
    public String noOfItems;
    @SerializedName("cart_type")
    @Expose
    public Integer cartType;
    @SerializedName("variation_image")
    @Expose
    public List<HomeProductimageResponse> variationImage = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productVariationId", productVariationId).append("variationCode", variationCode).append("quantity", quantity).append("actualPrice", actualPrice).append("youSaved", youSaved).append("unitPrice", unitPrice).append("stockLeft", stockLeft).append("brand", brand).append("productId", productId).append("productName", productName).append("description", description).append("discount", discount).append("startLimit", startLimit).append("endLimit", endLimit).append("noOfItems", noOfItems).append("cartType", cartType).append("variationImage", variationImage).toString();
    }


}
