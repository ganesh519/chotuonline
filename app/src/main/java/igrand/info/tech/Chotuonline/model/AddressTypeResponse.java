package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AddressTypeResponse {

    @SerializedName("address_type_id")
    @Expose
    public String addressTypeId;
    @SerializedName("name")
    @Expose
    public String name;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addressTypeId", addressTypeId).append("name", name).toString();
    }

}
