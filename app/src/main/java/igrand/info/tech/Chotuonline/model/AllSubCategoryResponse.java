package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class AllSubCategoryResponse {



    @SerializedName("category_id")
    @Expose
    public String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    public String subcategoryId;
    @SerializedName("subcategory_name")
    @Expose
    public String subcategoryName;
    @SerializedName("subsubcategoryinfo")
    @Expose
    public List<AllSubSubCategoryDataResponse> subsubcategoryinfo = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("categoryId", categoryId).append("subcategoryId", subcategoryId).append("subcategoryName", subcategoryName).append("subsubcategoryinfo", subsubcategoryinfo).toString();
    }

}
