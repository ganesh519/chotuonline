package igrand.info.tech.Chotuonline.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class Product_DetailsDataResponse {

    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("brand")
    @Expose
    public String brand;
    @SerializedName("display_name")
    @Expose
    public String displayName;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("vat")
    @Expose
    public String vat;
    @SerializedName("product_variation")
    @Expose
    public List<ProductVariationResponse> productVariation = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productName", productName).append("brand", brand).append("displayName", displayName).append("description", description).append("vat", vat).append("productVariation", productVariation).toString();
    }

}
