package igrand.info.tech.Chotuonline.adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.activity.Addressbook;
import igrand.info.tech.Chotuonline.activity.CheckOutActivity;
import igrand.info.tech.Chotuonline.activity.EditAddressActivity;
import igrand.info.tech.Chotuonline.activity.MainActivity;
import igrand.info.tech.Chotuonline.model.AddressBookDataResoponse;
import igrand.info.tech.Chotuonline.model.DeleteAddressResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddAddressRecyclerAdapter1 extends RecyclerView.Adapter<AddAddressRecyclerAdapter1.Holder> {
    Context context;
    List<AddressBookDataResoponse> addressBookDataResoponse;
    ApiInterface apiInterface, apiInterface1;
    String check;
    Activity activity;
    public static String address_id, add_id, add_type, pin, city, area, landmark, resdent, house, state, type;
    String Address, Address_id, address_type;
    String checking;

    public AddAddressRecyclerAdapter1(Context context, List<AddressBookDataResoponse> addressBookDataResoponse, String check) {
        this.context = context;
        this.addressBookDataResoponse = addressBookDataResoponse;
        this.check = check;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_ddress, parent, false);

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {

//        holder.home.setText(addressBookDataResoponse.get(position).addressType);
        holder.address.setText(addressBookDataResoponse.get(position).house + "," + addressBookDataResoponse.get(position).residential + "," + addressBookDataResoponse.get(position).landmark + ","
                + addressBookDataResoponse.get(position).area + "," + addressBookDataResoponse.get(position).city
                + "," + addressBookDataResoponse.get(position).state + "-" + addressBookDataResoponse.get(position).pincode + ".");
        holder.c_name.setText(addressBookDataResoponse.get(position).customerName);
        holder.mobile.setText(addressBookDataResoponse.get(position).customerPhoneNumber);

        if (addressBookDataResoponse.get(position).defaultAddress.equals("1")) {
            holder.home.setText(addressBookDataResoponse.get(position).addressType + " (Default)");
            holder.home.setTextColor(Color.parseColor("#018bc8"));
            holder.address.setTextColor(Color.parseColor("#018bc8"));
            holder.c_name.setText(addressBookDataResoponse.get(position).customerName);
            holder.c_name.setTextColor(Color.parseColor("#018bc8"));
            holder.mobile.setText(addressBookDataResoponse.get(position).customerPhoneNumber);
            holder.mobile.setTextColor(Color.parseColor("#018bc8"));

            String addr = addressBookDataResoponse.get(position).house + "," + addressBookDataResoponse.get(position).residential + "," + addressBookDataResoponse.get(position).landmark +
                    "," + addressBookDataResoponse.get(position).area + "," + addressBookDataResoponse.get(position).city + "," +
                    addressBookDataResoponse.get(position).state + "," + addressBookDataResoponse.get(position).country + "," + addressBookDataResoponse.get(position).pincode + ".";
            String add_id = addressBookDataResoponse.get(position).addressbookId;
            String add_type = addressBookDataResoponse.get(position).addressType;

            SharedPreferences sharedPreference = context.getSharedPreferences("AddressData", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.putString("ADDRESS", addr);
            editor.putString("ADDRESS_ID", add_id);
            editor.putString("ADDRESS_TYPE", add_type);
            editor.commit();

        } else {
            holder.home.setText(addressBookDataResoponse.get(position).addressType);
        }

        holder.Edit_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address_id = addressBookDataResoponse.get(position).addressbookId;
                String stateid = addressBookDataResoponse.get(position).stateId;
                String cityid = addressBookDataResoponse.get(position).cityId;
                String areaid = addressBookDataResoponse.get(position).areaId;
                String house = addressBookDataResoponse.get(position).house;
                String resedent = addressBookDataResoponse.get(position).residential;
                String land = addressBookDataResoponse.get(position).landmark;
                String addres_type = addressBookDataResoponse.get(position).addressType;
                String type = addressBookDataResoponse.get(position).defaultAddress;

                Intent intent = new Intent(context, EditAddressActivity.class);
                intent.putExtra("AddressBook_id", address_id);
                intent.putExtra("state_id", stateid);
                intent.putExtra("city_id", cityid);
                intent.putExtra("area_id", areaid);
                intent.putExtra("houneno", house);
                intent.putExtra("resdential", resedent);
                intent.putExtra("landmark", land);
                intent.putExtra("addressType", addres_type);
                intent.putExtra("defaultadd", type);

                intent.putExtra("key", check);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.startActivity(intent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (checking.equals("myAccount")) {
                    Address = addressBookDataResoponse.get(position).house + "," + addressBookDataResoponse.get(position).residential + "," + addressBookDataResoponse.get(position).landmark +
                            "," + addressBookDataResoponse.get(position).area + "," + addressBookDataResoponse.get(position).city + "," +
                            addressBookDataResoponse.get(position).state + "," + addressBookDataResoponse.get(position).country + "," + addressBookDataResoponse.get(position).pincode + ".";

                    Address_id = addressBookDataResoponse.get(position).addressbookId;
                    address_type = addressBookDataResoponse.get(position).addressType;

                    SharedPreferences sharedPreferences = context.getSharedPreferences("AddressData", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("ADDRESS", Address);
                    editor.putString("ADDRESS_ID", Address_id);
                    editor.putString("ADDRESS_TYPE", address_type);
                    editor.commit();

                } else if (checking.equals("MainActivity")) {

                    Address = addressBookDataResoponse.get(position).house + "," + addressBookDataResoponse.get(position).residential + "," + addressBookDataResoponse.get(position).landmark +
                            "," + addressBookDataResoponse.get(position).area + "," + addressBookDataResoponse.get(position).city + "," +
                            addressBookDataResoponse.get(position).state + "," + addressBookDataResoponse.get(position).country + "," + addressBookDataResoponse.get(position).pincode + ".";

                    Address_id = addressBookDataResoponse.get(position).addressbookId;
                    address_type = addressBookDataResoponse.get(position).addressType;

                    SharedPreferences sharedPreferences = context.getSharedPreferences("AddressData", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("ADDRESS", Address);
                    editor.putString("ADDRESS_ID", Address_id);
                    editor.putString("ADDRESS_TYPE", address_type);

                    editor.commit();


                } else if (checking.equals("AddAddress")) {

                    Address = addressBookDataResoponse.get(position).house + "," + addressBookDataResoponse.get(position).residential + "," + addressBookDataResoponse.get(position).landmark +
                            "," + addressBookDataResoponse.get(position).area + "," + addressBookDataResoponse.get(position).city + "," +
                            addressBookDataResoponse.get(position).state + "," + addressBookDataResoponse.get(position).country + "," + addressBookDataResoponse.get(position).pincode + ".";

                    Address_id = addressBookDataResoponse.get(position).addressbookId;
                    address_type = addressBookDataResoponse.get(position).addressType;

                    SharedPreferences sharedPreferences = context.getSharedPreferences("AddressData", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("ADDRESS", Address);
                    editor.putString("ADDRESS_ID", Address_id);
                    editor.putString("ADDRESS_TYPE", address_type);

                    editor.commit();


                } else {

                    Address = addressBookDataResoponse.get(position).house + "," + addressBookDataResoponse.get(position).residential + "," + addressBookDataResoponse.get(position).landmark +
                            "," + addressBookDataResoponse.get(position).area + "," + addressBookDataResoponse.get(position).city + "," +
                            addressBookDataResoponse.get(position).state + "," + addressBookDataResoponse.get(position).country + "," + addressBookDataResoponse.get(position).pincode + ".";

                    Address_id = addressBookDataResoponse.get(position).addressbookId;
                    address_type = addressBookDataResoponse.get(position).addressType;

                    SharedPreferences sharedPreferences = context.getSharedPreferences("AddressData", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("ADDRESS", Address);
                    editor.putString("ADDRESS_ID", Address_id);
                    editor.putString("ADDRESS_TYPE", address_type);

                    editor.commit();

                    Intent intent = new Intent(context, CheckOutActivity.class);
                    AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    appCompatActivity.finish();
                    appCompatActivity.startActivity(intent);

                }


            }
        });

        holder.address_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                SharedPreferences sharedPreferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                String user_id = sharedPreferences.getString("user_id", "");
                String addres_id = addressBookDataResoponse.get(position).addressbookId;

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<DeleteAddressResponse> call = apiInterface.AddressDelete(user_id, addres_id);
                call.enqueue(new Callback<DeleteAddressResponse>() {
                    @Override
                    public void onResponse(Call<DeleteAddressResponse> call, Response<DeleteAddressResponse> response) {
                        if (response.isSuccessful()) ;
                        DeleteAddressResponse deleteAddressResponse = response.body();
                        if (deleteAddressResponse.status.equals("1")) {
                            Toast.makeText(context, deleteAddressResponse.message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, Addressbook.class);
                            AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            appCompatActivity.startActivity(intent);

                            SharedPreferences sharedPreference1 = context.getSharedPreferences("AddressData", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor1 = sharedPreference1.edit();
                            editor1.clear();
                            editor1.commit();

                        } else if (deleteAddressResponse.status.equals("0")) {
                            Toast.makeText(context, deleteAddressResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteAddressResponse> call, Throwable t) {
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressBookDataResoponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView home, address, c_name, mobile;
        RelativeLayout Edit_address;
        ImageView address_delete;
        LinearLayout linear;
        RadioButton radio_select;

        public Holder(final View itemView) {
            super(itemView);
            home = itemView.findViewById(R.id.home);
            address = itemView.findViewById(R.id.address);
            Edit_address = itemView.findViewById(R.id.Edit_address);
            address_delete = itemView.findViewById(R.id.address_delete);
            linear = itemView.findViewById(R.id.linear);
            mobile = itemView.findViewById(R.id.mobile);
            c_name = itemView.findViewById(R.id.c_name);

            checking = ((AppCompatActivity) itemView.getContext()).getIntent().getStringExtra("Addressbook");


        }
    }
}
