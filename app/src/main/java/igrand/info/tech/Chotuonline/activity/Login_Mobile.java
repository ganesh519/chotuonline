package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.LoginMobileResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login_Mobile extends AppCompatActivity {
    Button mobile_login;
    EditText mobile_no;
    ApiInterface apiInterface;
    String Mobile;
    ImageView back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_mobile);
        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mobile_no=findViewById(R.id.mobile_no);
        mobile_login=findViewById(R.id.mobile_login);
        mobile_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mobile=mobile_no.getText().toString().trim();
                final ProgressDialog pd = new ProgressDialog(Login_Mobile.this);
                pd.setMessage("Loading....");
                pd.show();
                apiInterface= ApiClient.getClient().create(ApiInterface.class);
                Call<LoginMobileResponse> call=apiInterface.LoginMobile(Mobile);
                call.enqueue(new Callback<LoginMobileResponse>() {
                    @Override
                    public void onResponse(Call<LoginMobileResponse> call, Response<LoginMobileResponse> response) {
                        if (response.isSuccessful());
                        LoginMobileResponse loginMobileResponse=response.body();
                        if (loginMobileResponse.status.equals("1")){
                            pd.dismiss();
//                            Toast.makeText(Login_Mobile.this, loginMobileResponse.message, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(Login_Mobile.this, Mobile_OtpVerify.class)
                                    .putExtra("phone", Mobile));
                        }
                        else if (loginMobileResponse.status.equals("0")){

                            pd.dismiss();
                            Toast.makeText(Login_Mobile.this, loginMobileResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<LoginMobileResponse> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(Login_Mobile.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
