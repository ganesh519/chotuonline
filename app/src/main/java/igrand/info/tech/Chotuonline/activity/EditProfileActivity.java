package igrand.info.tech.Chotuonline.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.EditProfileDataResponse;
import igrand.info.tech.Chotuonline.model.EditprofileResponse;
import igrand.info.tech.Chotuonline.util.Utility;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by kamlesh on 4/12/2018.
 */

public class EditProfileActivity extends AppCompatActivity {
    ImageView back;
    CircleImageView profile_image;
    public static final int PICK_IMAGE_REQUEST = 2;
    ApiInterface apiInterface;
    EditText et_fname, et_lname, et_email, et_mobile, et_altnumber;
    TextView save, et_dateofbirth;
    File file = null;
    String MobilePattern = "[0-9]{10}";
    MultipartBody.Part body = null;
    Uri uri;
    private static final int GALLERY_REQUEST = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ = 1;
    private Uri mCropImageUri;
    String user_id;
    String photo;
    EditProfileDataResponse editProfileDataResponse;

    static final int DATE_DIALOG_ID = 0;
    private int mYear, mMonth, mDay;

    private ArrayList<Image> images;

    @SuppressWarnings("deprecation")
    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);
        et_fname = findViewById(R.id.et_fname);
        et_lname = findViewById(R.id.et_lname);

        et_email = findViewById(R.id.et_email);
        et_mobile = findViewById(R.id.et_mobile);
//        et_mobile.setEnabled(false);
        et_altnumber = findViewById(R.id.et_altnumber);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        profile_image = findViewById(R.id.profile_image);
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.with(EditProfileActivity.this)                         //  Initialize ImagePicker with activity or fragment context
                        .setToolbarColor("#212121")         //  Toolbar color
                        .setStatusBarColor("#000000")       //  StatusBar color (works with SDK >= 21  )
                        .setToolbarTextColor("#FFFFFF")     //  Toolbar text color (Title and Done button)
                        .setToolbarIconColor("#FFFFFF")     //  Toolbar icon color (Back and Camera button)
                        .setProgressBarColor("#4CAF50")     //  ProgressBar color
                        .setBackgroundColor("#212121")      //  Background color
                        .setCameraOnly(false)               //  Camera mode
                        .setMultipleMode(false)              //  Select multiple images or single image
                        .setFolderMode(true)                //  Folder mode
                        .setShowCamera(true)                //  Show camera button
                        .setFolderTitle("Albums")           //  Folder title (works with FolderMode = true)
                        .setImageTitle("Galleries")         //  Image title (works with FolderMode = false)
                        .setDoneTitle("Done")               //  Done button title
                        .setLimitMessage("You have reached selection limit")    // Selection limit message
                        .setMaxSize(10)                     //  Max images can be selected
                        .setSavePath("ImagePicker")         //  Image capture folder name
                        .setSelectedImages(images)          //  Selected images
                        .setAlwaysShowDoneButton(true)      //  Set always show done button in multiple mode
                        .setKeepScreenOn(true)              //  Keep screen on when selecting images
                        .start();                           //  Start ImagePicker
                }
        });

        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        et_dateofbirth = findViewById(R.id.et_dateofbirth);
        c.setTimeInMillis(System.currentTimeMillis());
        et_dateofbirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        save = findViewById(R.id.save);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreferences.getString("user_id", "");

        SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("Profile", Context.MODE_PRIVATE);
        String fname = sharedPreferences1.getString("firstname", "");
        String lname = sharedPreferences1.getString("lastname", "");
        String email = sharedPreferences1.getString("e_mail", "");
        String phone = sharedPreferences1.getString("mobile", "");
        String aphone1 = sharedPreferences1.getString("altermobile", "");
        String date1 = sharedPreferences1.getString("date_of_birth", "");
        photo = sharedPreferences1.getString("image", "");

        et_fname.setText(fname);
        et_lname.setText(lname);
        et_email.setText(email);
        et_mobile.setText(phone);
        et_dateofbirth.setText(date1);
        et_altnumber.setText(aphone1);

        if (photo.equals("")) {
            profile_image.setImageResource(R.drawable.no_image);
        }
        else {
            Picasso.with(getApplicationContext()).load(photo).resize(160, 160).into(profile_image);
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getProfile();
            }
        });
    }

    private void getProfile() {

        String f_name = et_fname.getText().toString();
        String l_name = et_lname.getText().toString();
        String e_mail = et_email.getText().toString();
        String mobile = et_mobile.getText().toString();
        String amobile = et_altnumber.getText().toString();
        String date = et_dateofbirth.getText().toString();

        if (f_name.equals("")) {

            Toast.makeText(this, "Enter First name", Toast.LENGTH_SHORT).show();
        }
        else if (l_name.equals("")) {
            Toast.makeText(this, "Enter Last name", Toast.LENGTH_SHORT).show();

        }
        else if (!amobile.equals("")&& amobile.length()<10){
            Toast.makeText(getApplicationContext(), "Please enter valid 10 digit mobile number", Toast.LENGTH_SHORT).show();

        }
//        else if(!amobile.matches(MobilePattern)) {
//            Toast.makeText(getApplicationContext(), "Please enter valid 10 digit mobile number", Toast.LENGTH_SHORT).show();
//        }
        else if (!Utility.checkMailPatteren(e_mail)) {
            Utility.toastView(getApplicationContext(), "Invalid email");
        }
        else {

            getPProfile(f_name, l_name, e_mail, mobile, amobile, date);

        }
    }

    private void getPProfile(String f_name, String l_name, String e_mail, String mobile, String amobile, String date) {

        final ProgressDialog progressDialog = new ProgressDialog(EditProfileActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        RequestBody cust_id = RequestBody.create(MediaType.parse("text/plain"), user_id);
        RequestBody firstnam = RequestBody.create(MediaType.parse("text/plain"), f_name);
        RequestBody lastnam = RequestBody.create(MediaType.parse("text/plain"), l_name);
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), e_mail);
        RequestBody Mobile = RequestBody.create(MediaType.parse("text/plain"), mobile);
        RequestBody Amobile = RequestBody.create(MediaType.parse("text/plain"), amobile);
        RequestBody dateof = RequestBody.create(MediaType.parse("text/plain"), date);

        MultipartBody.Part body = null;
        if (file != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            body = MultipartBody.Part.createFormData("photo", file.getName(), requestFile);

        }
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<EditprofileResponse> call = apiInterface.EditProfile(body, cust_id, firstnam, lastnam, email, Mobile, dateof, Amobile);
        call.enqueue(new Callback<EditprofileResponse>() {
            @Override
            public void onResponse(Call<EditprofileResponse> call, Response<EditprofileResponse> response) {
                if (response.isSuccessful()) ;
                EditprofileResponse editprofileResponse = response.body();
                if (editprofileResponse.status.equals("1")) {
                    editProfileDataResponse = editprofileResponse.data;
                    String image = editProfileDataResponse.userPhoto;
                    String name = editProfileDataResponse.userFirstName;
                    String mobile = editProfileDataResponse.userPhoneNumber;

                    SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor1 = sharedPreferences1.edit();
                    editor1.putString("profileimage", image);
                    editor1.putString("Name", name);
                    editor1.putString("Mobile", mobile);
                    editor1.commit();

                    progressDialog.dismiss();
                    Toast.makeText(EditProfileActivity.this, editprofileResponse.message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(EditProfileActivity.this, MainActivity.class));
                    finish();
                }
                else if (editprofileResponse.status.equals("0")) {
                    progressDialog.dismiss();
                    Toast.makeText(EditProfileActivity.this, editprofileResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EditprofileResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(EditProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                DatePickerDialog datePickerDialog=new  DatePickerDialog(this,
                        mDateSetListener,
                        mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                return datePickerDialog;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            et_dateofbirth.setText(new StringBuilder().append(mDay).append("/").append(mMonth + 1).append("/").append(mYear));
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);

            try {
                if (images != null) {
                    for (int i = 0; i < images.size(); i++) {
                        Uri uri = Uri.fromFile(new File(images.get(0).getPath()));
                        Picasso.with(getApplicationContext()).load(uri).into(profile_image);
                        file = null;
                        String filepath = images.get(0).getPath();
                        if (filepath != null && !filepath.equals("")) {
                            file = new File(filepath);
                        }
                    }
                }
            }
            catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
