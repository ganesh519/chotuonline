package igrand.info.tech.Chotuonline.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;

import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.PaymentMethodsDataResponse;

public class PaymentmethodsRecyclerAdapter extends RecyclerView.Adapter<PaymentmethodsRecyclerAdapter.Holder> {

  private  List<PaymentMethodsDataResponse> paymentMethodsDataResponse;
    Context context;
    public int lastSelectedPosition=-1 ;
    private String name,payname;
    public static String cash="";

    public PaymentmethodsRecyclerAdapter(List<PaymentMethodsDataResponse> paymentMethodsDataResponse, Context context) {
        this.paymentMethodsDataResponse=paymentMethodsDataResponse;
        this.context=context;
    }

    @NonNull
    @Override
    public PaymentmethodsRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_paymentmethods, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentmethodsRecyclerAdapter.Holder holder, int position) {

        holder.offer_name.setText(paymentMethodsDataResponse.get(position).name);
        holder.offer_select.setChecked(lastSelectedPosition == position);

    }
    @Override
    public int getItemCount() {
        return paymentMethodsDataResponse.size();
    }

   public class Holder extends RecyclerView.ViewHolder {
        TextView offer_name;
        RadioButton offer_select;
        RadioGroup radio_group;


        public Holder(View itemView) {
            super(itemView);
            offer_select=itemView.findViewById(R.id.offer_select);
            offer_name=itemView.findViewById(R.id.offer_name);
//            radio_group=itemView.findViewById(R.id.radio_group);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                     cash=paymentMethodsDataResponse.get(lastSelectedPosition).paymentId;

//                    SharedPreferences sharedPreferences=context.getSharedPreferences("Cash_id",Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor=sharedPreferences.edit();
//                    editor.putString("ID",cash);
//                    editor.commit();
                }
            };
            itemView.setOnClickListener(clickListener);
            offer_select.setOnClickListener(clickListener);
        }
    }
}
