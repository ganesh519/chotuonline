package igrand.info.tech.Chotuonline.model;

public class Category {

    private int product_image;
    private String product_title;

    public Category(int product_image, String product_title) {
        this.product_image = product_image;
        this.product_title = product_title;
    }

    public int getProduct_image() {
        return product_image;
    }

    public void setProduct_image(int product_image) {
        this.product_image = product_image;
    }

    public String getProduct_title() {
        return product_title;
    }

    public void setProduct_title(String product_title) {
        this.product_title = product_title;
    }
}
