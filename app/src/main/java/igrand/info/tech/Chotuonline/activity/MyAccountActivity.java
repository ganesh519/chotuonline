package igrand.info.tech.Chotuonline.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import igrand.info.tech.Chotuonline.ApiClient;
import igrand.info.tech.Chotuonline.ApiInterface;
import igrand.info.tech.Chotuonline.R;
import igrand.info.tech.Chotuonline.model.ProfileDataResponse;
import igrand.info.tech.Chotuonline.model.ProfileResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamlesh on 4/12/2018.
 */

public class MyAccountActivity extends AppCompatActivity {

    TextView new_address;
    ImageView back, edit;
    ImageView home_edit, edit_office;
    CircleImageView profile_image;
    public static final int PICK_IMAGE_REQUEST = 2;
    TextView name, mail, mobile, dateofbirth, alter_mobile;
    ApiInterface apiInterface;
    String cname, cl_name, cmail, cmobile, cdateofbirth, amobile, photo;
    LinearLayout edit_profile;
    RelativeLayout addressbook, changepassword, changephone;
    NestedScrollView nested_profile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_account);
        nested_profile=findViewById(R.id.nested_profile);
        name = findViewById(R.id.name);
        mail = findViewById(R.id.mail);
        mobile = findViewById(R.id.mobile);
        dateofbirth = findViewById(R.id.dateofbirth);
        profile_image = findViewById(R.id.profile_image);
        alter_mobile = findViewById(R.id.alter_mobile);
        addressbook = findViewById(R.id.addressbook);
        addressbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyAccountActivity.this, Addressbook.class).putExtra("Addressbook","myAccount"));
            }
        });
        changepassword = findViewById(R.id.changepassword);
        changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyAccountActivity.this, ChangePasswordActivity.class));
            }
        });
        changephone = findViewById(R.id.changephone);
        changephone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyAccountActivity.this, ChangeMobileNumberActivity.class));
            }
        });

        edit_profile = findViewById(R.id.edit_profile);

        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyAccountActivity.this, EditProfileActivity.class));
                finish();
            }
        });
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences.getString("user_id", "");

        final ProgressDialog progressDialog = new ProgressDialog(MyAccountActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProfileResponse> call = apiInterface.Profile(user_id);
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if (response.isSuccessful()) ;
                ProfileResponse profileResponse = response.body();
                if (profileResponse.status.equals("1")) {
                    nested_profile.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    ProfileDataResponse profileDataResponse = profileResponse.data;
                    cname = profileDataResponse.firstName;
                    cl_name = profileDataResponse.lastName;
                    cmail = profileDataResponse.emailId;
                    cmobile = profileDataResponse.phoneNumber;
                    cdateofbirth = profileDataResponse.dateOfBirth;
                    amobile = profileDataResponse.alternateNumber;
                    photo = profileDataResponse.image;
                    if (photo.equals("")){
                        profile_image.setImageResource(R.drawable.no_image);
                    }
                    else {
                        Picasso.with(getApplicationContext()).load(photo).error(R.drawable.no_image).into(profile_image);
                    }

                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("Profile", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("firstname", cname);
                    editor.putString("lastname", cl_name);
                    editor.putString("e_mail", cmail);
                    editor.putString("mobile", cmobile);
                    editor.putString("image", photo);
                    editor.putString("date_of_birth", cdateofbirth);
                    editor.putString("altermobile", amobile);
                    editor.commit();

                    name.setText(cname);
                    mail.setText(cmail);
                    mobile.setText(cmobile);
                    dateofbirth.setText(cdateofbirth);
                    alter_mobile.setText(amobile);
                }
                else if (profileResponse.status.equals("0")) {
                    nested_profile.setVisibility(View.GONE);
                    Toast.makeText(MyAccountActivity.this, profileResponse.message, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                nested_profile.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(MyAccountActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
