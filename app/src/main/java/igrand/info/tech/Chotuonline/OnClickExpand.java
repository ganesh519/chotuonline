package igrand.info.tech.Chotuonline;

import igrand.info.tech.Chotuonline.model.AllSubCategoryResponse;
import igrand.info.tech.Chotuonline.model.AllSubSubCategoryDataResponse;

public interface OnClickExpand {
    void subItemSelected(AllSubCategoryResponse all_categoryDataResponse);
    void subsubItemSelected(AllSubCategoryResponse all_categoryDataResponse,AllSubSubCategoryDataResponse rowItem);
}
